#!/usr/bin/env bash

mvn clean package -Ptest

echo 'Remove existing build'

ssh root@161.35.24.193 << EOF

sudo ufw deny 12369
systemctl stop LexitCabinetApi-test
rm /etc/systemd/system/LexitCabinetApi-test.service
rm -r /applications/LexitCabinetApi/test
mkdir /applications
mkdir /applications/LexitCabinetApi
mkdir /applications/LexitCabinetApi/test
mkdir /applications/LexitCabinetApi/test/build

EOF

echo 'Copy files...'

scp \
    ./target/LexitCabinetApi-0.0.1-SNAPSHOT.jar \
    root@161.35.24.193:/applications/LexitCabinetApi/test

echo 'Load service file...'

scp \
    ./scripts/files/LexitCabinetApi-test.service \
    root@161.35.24.193:/etc/systemd/system

ssh root@161.35.24.193 << EOF

sudo systemctl daemon-reload
sudo systemctl start LexitCabinetApi-test
sudo systemctl status LexitCabinetApi-test
sudo ufw allow 12369

EOF

echo 'Bye...'