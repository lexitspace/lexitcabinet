package ua.lexit.cabinet;

import org.springframework.jdbc.core.JdbcTemplate;
import ua.lexit.cabinet.config.DataSourceConfiguration;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.entities.users.Collaborator;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.exceptions.ServiceException;
import ua.lexit.cabinet.repository.user.ClientDAO;
import ua.lexit.cabinet.repository.user.ClientDAOImpl;
import ua.lexit.cabinet.repository.user.CollaboratorDAO;
import ua.lexit.cabinet.repository.user.CollaboratorDAOImpl;


public class Start {
    public static void main(String[] args) throws RepositoryException, ServiceException {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(new DataSourceConfiguration().devDataSource());
        ClientDAO clientDAO = new ClientDAOImpl(jdbcTemplate);
        CollaboratorDAO collaboratorDAO = new CollaboratorDAOImpl(jdbcTemplate);
//        AddressDAO addressDAO = new AddressDAOImpl(jdbcTemplate);
////        System.out.println(subjectDAO.get("eb851553-79ea-4e2e-8996-3e75d21c3e92"));
////        List<Subject> list = subjectDAO.getList();
//        List<Address> list = addressDAO.getList();
//
//
//        for (Address s : list) {
//            System.out.println(s);
//        }

        Client client = new Client();
        client.setFirstName("test");
        clientDAO.create(client);

        Collaborator collaborator = new Collaborator();
        collaborator.setFirstName("testCollaborator");
        collaboratorDAO.create(collaborator);




//        System.out.println(GISStreetType.AVENUE.name());
//        System.out.println(GISStreetType.AVENUE);
//        System.out.println(GISStreetType.valueOf("AVENUE"));
    }
}
