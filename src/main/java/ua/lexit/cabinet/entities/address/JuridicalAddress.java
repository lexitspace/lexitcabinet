package ua.lexit.cabinet.entities.address;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class JuridicalAddress extends Address {

    private String title;
    private Integer limitation;

}
