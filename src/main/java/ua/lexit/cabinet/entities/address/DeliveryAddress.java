package ua.lexit.cabinet.entities.address;

import lombok.*;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.address.delivery.AddressDeliveryService;
import ua.lexit.cabinet.enums.address.delivery.AddressDeliveryType;
import ua.lexit.cabinet.enums.address.GISFlatType;
import ua.lexit.cabinet.enums.address.GISStreetType;
import ua.lexit.cabinet.enums.statuses.AddressStatus;
import ua.lexit.cabinet.enums.types.AddressType;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class DeliveryAddress extends Address {

    private Client client;
    private AddressDeliveryService deliveryService;
    private AddressDeliveryType deliveryType;
    private String recipient;
    private String phoneNumber;
    private String city;
    private GISStreetType streetType;
    private String street;
    private String house;
    private String corpus;
    private String entrance;
    private String floor;
    private GISFlatType flatType;
    private String flat;
    private String mailbox;
    private String zipCode;
    private String warehouse;

}
