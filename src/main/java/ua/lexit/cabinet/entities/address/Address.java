package ua.lexit.cabinet.entities.address;

import lombok.*;
import ua.lexit.cabinet.enums.statuses.AddressStatus;
import ua.lexit.cabinet.enums.types.AddressType;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Address {

    private String uuid;
    private Instant createDate;
    private AddressType type;
    private AddressStatus status;

}
