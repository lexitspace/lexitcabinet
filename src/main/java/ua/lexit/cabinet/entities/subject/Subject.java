package ua.lexit.cabinet.entities.subject;

import lombok.*;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.statuses.SubjectStatus;
import ua.lexit.cabinet.enums.types.SubjectType;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Subject {

    private String uuid;
    private Instant createDate;
    private SubjectType type;
    private SubjectStatus status;
    private Client client;
    private String title;
    private String legalCode;
    private JuridicalAddress juridicalAddress;
}
