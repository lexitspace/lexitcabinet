package ua.lexit.cabinet.entities.token;

import lombok.*;
import ua.lexit.cabinet.entities.users.User;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Token {

    private String uuid;
    private User user;
    private String token;
    private Instant createDate;
    private Instant expiryDate;

}
