package ua.lexit.cabinet.entities.users;


import lombok.*;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.UserType;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class User {

    private String uuid;
    private Instant createDate;
    private UserType type;
    private UserStatus status;
    private String login;
    private String email;
    private String phone;
    private String password;
    private String firstName;
    private String secondName;
    private String lastName;
    private String shortName;
    private String fullName;

}
