package ua.lexit.cabinet.services.user;

import ua.lexit.cabinet.entities.users.Collaborator;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.util.List;

public interface CollaboratorService {
    /**
     * Renew current {@link Collaborator} record from the database.
     *
     * @param collaborator example of class that we want to renew in the database.
     * @return {@link Collaborator} not null.
     * @throws RepositoryException
     */
    Collaborator renew(
            Collaborator collaborator
    ) throws RepositoryException;

    /**
     * Get existing {@link Collaborator} record from the database.
     *
     * @param identifier - identifier of the getting record.
     * @return {@link Collaborator} not null.
     * @throws RepositoryException
     */
    Collaborator get(
            String identifier
    ) throws RepositoryException;

    /**
     * Get existing {@link Collaborator} records from the database.
     *
     * @return {@link List <Collaborator>} not null.
     * @throws RepositoryException
     */
    List<Collaborator> getList() throws RepositoryException;

    /**
     * Get existing {@link Collaborator} records from the database.
     *
     * @param collaboratorUuids -
     * @param statusCodes       -
     * @param limit             -
     * @param offset            -
     * @return {@link List <Collaborator>} not null.
     * @throws RepositoryException
     */
    List<Collaborator> getList(
            List<String> collaboratorUuids,
            List<String> statusCodes,
            Integer limit,
            Integer offset
    ) throws RepositoryException;

    /**
     * Get size {@link Collaborator} records from the database.
     *
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize() throws RepositoryException;

    /**
     * Get size {@link Collaborator} records from the database.
     *
     * @param collaboratorUuids -
     * @param statusCodes       -
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize(
            List<String> collaboratorUuids,
            List<String> statusCodes
    ) throws RepositoryException;

    /**
     * Create {@link Collaborator} record to the database.
     *
     * @param collaborator example of class that we want store in the database.
     * @return {@link Collaborator} not null.
     * @throws RepositoryException
     */
    Collaborator create(
            Collaborator collaborator
    ) throws RepositoryException;

    /**
     * Update current {@link Collaborator} record in the database.
     *
     * @param collaborator example of class that we want to update in the database.
     * @return {@link Collaborator} not null.
     * @throws RepositoryException
     */
    Collaborator update(
            Collaborator collaborator
    ) throws RepositoryException;

    /**
     * Delete current {@link Collaborator} record from the database.
     *
     * @param collaborator example of class that we want to delete in the database.
     * @throws RepositoryException
     */
    void delete(
            Collaborator collaborator
    ) throws RepositoryException;

    /**
     * Delete existing {@link Collaborator} record from the database by {@code uuid}.
     *
     * @param identifier identifier of the deleting record.
     * @throws RepositoryException
     */
    void delete(
            String identifier
    ) throws RepositoryException;
}
