package ua.lexit.cabinet.services.user;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.entities.users.Collaborator;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.repository.user.CollaboratorDAO;

import java.util.List;

@Service
public class CollaboratorServiceImpl implements CollaboratorService {

    final private CollaboratorDAO collaboratorDAO;

    public CollaboratorServiceImpl(
            CollaboratorDAO collaboratorDAO
    ) {
        this.collaboratorDAO = collaboratorDAO;
    }

    @Override
    public Collaborator renew(
            Collaborator collaborator
    ) throws RepositoryException {
        return collaboratorDAO.renew(collaborator);
    }

    @Override
    public Collaborator get(
            String identifier
    ) throws RepositoryException {
        return collaboratorDAO.get(identifier);
    }

    @Override
    public List<Collaborator> getList() throws RepositoryException {
        return collaboratorDAO.getList();
    }

    @Override
    public List<Collaborator> getList(
            List<String> collaboratorUuids,
            List<String> statusCodes,
            Integer limit,
            Integer offset
    ) throws RepositoryException {
        return collaboratorDAO.getList(
                collaboratorUuids,
                statusCodes,
                limit,
                offset
        );
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        return collaboratorDAO.getListSize();
    }

    @Override
    public Integer getListSize(
            List<String> collaboratorUuids,
            List<String> statusCodes
    ) throws RepositoryException {
        return collaboratorDAO.getListSize(
                collaboratorUuids,
                statusCodes
        );
    }

    @Override
    public Collaborator create(
            Collaborator collaborator
    ) throws RepositoryException {
        return collaboratorDAO.create(collaborator);
    }

    @Override
    public Collaborator update(
            Collaborator collaborator
    ) throws RepositoryException {
        return collaboratorDAO.update(collaborator);
    }

    @Override
    public void delete(
            Collaborator collaborator
    ) throws RepositoryException {
        collaboratorDAO.delete(collaborator);
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        collaboratorDAO.delete(identifier);
    }
}
