package ua.lexit.cabinet.services.user;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.repository.user.ClientDAO;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    final private ClientDAO clientDAO;

    public ClientServiceImpl(
            ClientDAO clientDAO
    ) {
        this.clientDAO = clientDAO;
    }

    @Override
    public Client renew(
            Client client
    ) throws RepositoryException {
        return clientDAO.renew(client);
    }

    @Override
    public Client get(
            String identifier
    ) throws RepositoryException {
        return clientDAO.get(identifier);
    }

    @Override
    public List<Client> getList() throws RepositoryException {
        return clientDAO.getList();
    }

    @Override
    public List<Client> getList(
            List<String> clientUuids,
            List<String> statusCodes,
            Integer limit,
            Integer offset
    ) throws RepositoryException {
        return clientDAO.getList(
                clientUuids,
                statusCodes,
                limit,
                offset
        );
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        return clientDAO.getListSize();
    }

    @Override
    public Integer getListSize(
            List<String> clientUuids,
            List<String> statusCodes
    ) throws RepositoryException {
        return clientDAO.getListSize(
                clientUuids,
                statusCodes
        );
    }

    @Override
    public Client create(
            Client client
    ) throws RepositoryException {
        return clientDAO.create(client);
    }

    @Override
    public Client update(
            Client client
    ) throws RepositoryException {
        return clientDAO.update(client);
    }

    @Override
    public void delete(
            Client client
    ) throws RepositoryException {
        clientDAO.delete(client);
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        clientDAO.delete(identifier);
    }
}
