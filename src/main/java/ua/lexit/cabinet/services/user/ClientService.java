package ua.lexit.cabinet.services.user;

import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.util.List;

public interface ClientService {

    /**
     * Renew current {@link Client} record from the database.
     * @param client example of class that we want to renew in the database.
     * @return {@link Client} not null.
     * @throws RepositoryException
     */
    Client renew(
            Client client
    ) throws RepositoryException;

    /**
     * Get existing {@link Client} record from the database.
     * @param identifier - identifier of the getting record.
     * @return {@link Client} not null.
     * @throws RepositoryException
     */
    Client get(
            String identifier
    ) throws RepositoryException;

    /**
     * Get existing {@link Client} records from the database.
     * @return {@link List <Client>} not null.
     * @throws RepositoryException
     */
    List<Client> getList() throws RepositoryException;

    /**
     * Get existing {@link Client} records from the database.
     * @param clientUuids  -
     * @param statusCodes  -
     * @param limit  -
     * @param offset -
     * @return {@link List <Client>} not null.
     * @throws RepositoryException
     */
    List<Client> getList(
            List<String> clientUuids,
            List<String> statusCodes,
            Integer limit,
            Integer offset
    ) throws RepositoryException;

    /**
     * Get size {@link Client} records from the database.
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize() throws RepositoryException;

    /**
     * Get size {@link Client} records from the database.
     * @param clientUuids  -
     * @param statusCodes  -
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize(
            List<String> clientUuids,
            List<String> statusCodes
    ) throws RepositoryException;

    /**
     * Create {@link Client} record to the database.
     * @param client example of class that we want store in the database.
     * @return {@link Client} not null.
     * @throws RepositoryException
     */
    Client create(
            Client client
    ) throws RepositoryException;

    /**
     * Update current {@link Client} record in the database.
     * @param client example of class that we want to update in the database.
     * @return {@link Client} not null.
     * @throws RepositoryException
     */
    Client update(
            Client client
    ) throws RepositoryException;

    /**
     * Delete current {@link Client} record from the database.
     * @param client example of class that we want to delete in the database.
     * @throws RepositoryException
     */
    void delete(
            Client client
    ) throws RepositoryException;

    /**
     * Delete existing {@link Client} record from the database by {@code uuid}.
     * @param identifier identifier of the deleting record.
     * @throws RepositoryException
     */
    void delete(
            String identifier
    ) throws RepositoryException;
}
