package ua.lexit.cabinet.services.address;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.entities.address.DeliveryAddress;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.repository.address.DeliveryAddressDAO;

import java.util.List;

@Service
public class DeliveryAddressServiceImpl implements DeliveryAddressService {

    final private DeliveryAddressDAO deliveryAddressDAO;

    public DeliveryAddressServiceImpl(
            DeliveryAddressDAO deliveryAddressDAO
    ) {
        this.deliveryAddressDAO = deliveryAddressDAO;
    }

    @Override
    public DeliveryAddress renew(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException {
        return deliveryAddressDAO.renew(deliveryAddress);
    }

    @Override
    public DeliveryAddress get(
            String identifier
    ) throws RepositoryException {
        return deliveryAddressDAO.get(identifier);
    }

    @Override
    public List<DeliveryAddress> getList() throws RepositoryException {
        return deliveryAddressDAO.getList();
    }

    @Override
    public List<DeliveryAddress> getList(
            List<String> deliveryAddressUuids,
            List<String> statusCodes,
            List<String> userUuids,
            Integer limit,
            Integer offset
    ) throws RepositoryException {

        return deliveryAddressDAO.getList(
                deliveryAddressUuids,
                statusCodes,
                userUuids,
                limit,
                offset
        );
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        return deliveryAddressDAO.getListSize();
    }

    @Override
    public Integer getListSize(
            List<String> deliveryAddressUuids,
            List<String> statusCodes,
            List<String> userUuids
    ) throws RepositoryException {
        return deliveryAddressDAO.getListSize(
                deliveryAddressUuids,
                statusCodes,
                userUuids
        );
    }

    @Override
    public DeliveryAddress create(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException {
        return deliveryAddressDAO.create(deliveryAddress);
    }

    @Override
    public DeliveryAddress update(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException {
        return deliveryAddressDAO.update(deliveryAddress);
    }

    @Override
    public void delete(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException {
        deliveryAddressDAO.delete(deliveryAddress);
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        deliveryAddressDAO.delete(identifier);
    }
}
