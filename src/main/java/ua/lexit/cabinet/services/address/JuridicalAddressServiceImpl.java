package ua.lexit.cabinet.services.address;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.repository.address.JuridicalAddressDAO;

import java.util.List;

@Service
public class JuridicalAddressServiceImpl implements JuridicalAddressService {

    final private JuridicalAddressDAO juridicalAddressDAO;

    public JuridicalAddressServiceImpl(
            JuridicalAddressDAO juridicalAddressDAO
    ) {
        this.juridicalAddressDAO = juridicalAddressDAO;
    }

    @Override
    public JuridicalAddress renew(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException {
        return juridicalAddressDAO.renew(juridicalAddress);
    }

    @Override
    public JuridicalAddress get(
            String identifier
    ) throws RepositoryException {
        return juridicalAddressDAO.get(identifier);
    }

    @Override
    public List<JuridicalAddress> getList() throws RepositoryException {
        return juridicalAddressDAO.getList();
    }

    @Override
    public List<JuridicalAddress> getList(
            List<String> juridicalAddressUuids,
            List<String> statusCodes,
            Integer limit,
            Integer offset
    ) throws RepositoryException {
        return juridicalAddressDAO.getList(
                juridicalAddressUuids,
                statusCodes,
                limit,
                offset
        );
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        return juridicalAddressDAO.getListSize();
    }

    @Override
    public Integer getListSize(
            List<String> juridicalAddressUuids,
            List<String> statusCodes
    ) throws RepositoryException {
        return juridicalAddressDAO.getListSize(
                juridicalAddressUuids,
                statusCodes
        );
    }

    @Override
    public JuridicalAddress create(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException {
        return juridicalAddressDAO.create(juridicalAddress);
    }

    @Override
    public JuridicalAddress update(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException {
        return juridicalAddressDAO.update(juridicalAddress);
    }

    @Override
    public void delete(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException {
        juridicalAddressDAO.delete(juridicalAddress);
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        juridicalAddressDAO.delete(identifier);
    }
}
