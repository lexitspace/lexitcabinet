package ua.lexit.cabinet.services.token;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.entities.token.Token;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.repository.token.TokenDAO;

import java.util.List;

@Service
public class TokenServiceImpl implements TokenService {

    final private TokenDAO tokenDAO;

    public TokenServiceImpl(
            TokenDAO tokenDAO
    ) {
        this.tokenDAO = tokenDAO;
    }

    @Override
    public Token renew(
            Token token
    ) throws RepositoryException {
        return tokenDAO.renew(token);
    }

    @Override
    public Token get(
            String identifier
    ) throws RepositoryException {
        return tokenDAO.get(identifier);
    }

    @Override
    public List<Token> getList() throws RepositoryException {
        return tokenDAO.getList();
    }

    @Override
    public List<Token> getList(
            List<String> tokenUuids,
            List<String> userUuids
    ) throws RepositoryException {
        return tokenDAO.getList(
                tokenUuids,
                userUuids
        );
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        return tokenDAO.getListSize();
    }

    @Override
    public Integer getListSize(
            List<String> tokenUuids,
            List<String> userUuids
    ) throws RepositoryException {
        return tokenDAO.getListSize(
                tokenUuids,
                userUuids
        );
    }

    @Override
    public Token create(
            Token token
    ) throws RepositoryException {
        return tokenDAO.create(token);
    }

    @Override
    public Token update(
            Token token
    ) throws RepositoryException {
        return tokenDAO.update(token);
    }

    @Override
    public void delete(
            Token token
    ) throws RepositoryException {
        tokenDAO.delete(token);
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        tokenDAO.delete(identifier);
    }
}
