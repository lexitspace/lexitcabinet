package ua.lexit.cabinet.services.subject;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.entities.subject.IndividualEntrepreneur;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.repository.subject.IndividualEntrepreneurDAO;

import java.util.List;

@Service
public class IndividualEntrepreneurServiceImpl implements IndividualEntrepreneurService {

    final private IndividualEntrepreneurDAO individualEntrepreneurDAO;

    public IndividualEntrepreneurServiceImpl(
            IndividualEntrepreneurDAO individualEntrepreneurDAO
    ) {
        this.individualEntrepreneurDAO = individualEntrepreneurDAO;
    }

    @Override
    public IndividualEntrepreneur renew(
            IndividualEntrepreneur individualEntrepreneur
    ) throws RepositoryException {
        return individualEntrepreneurDAO.renew(individualEntrepreneur);
    }

    @Override
    public IndividualEntrepreneur get(
            String identifier
    ) throws RepositoryException {
        return individualEntrepreneurDAO.get(identifier);
    }

    @Override
    public List<IndividualEntrepreneur> getList() throws RepositoryException {
        return individualEntrepreneurDAO.getList();
    }

    @Override
    public List<IndividualEntrepreneur> getList(
            List<String> individualEntrepreneurUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids,
            Integer limit,
            Integer offset
    ) throws RepositoryException {
        return individualEntrepreneurDAO.getList(
                individualEntrepreneurUuids,
                statusCodes,
                userUuids,
                addressUuids,
                limit,
                offset
        );
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        return individualEntrepreneurDAO.getListSize();
    }

    @Override
    public Integer getListSize(
            List<String> individualEntrepreneurUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids
    ) throws RepositoryException {
        return individualEntrepreneurDAO.getListSize(
                individualEntrepreneurUuids,
                statusCodes,
                userUuids,
                addressUuids
        );
    }

    @Override
    public IndividualEntrepreneur create(
            IndividualEntrepreneur individualEntrepreneur
    ) throws RepositoryException {
        return individualEntrepreneurDAO.create(individualEntrepreneur);
    }

    @Override
    public IndividualEntrepreneur update(
            IndividualEntrepreneur individualEntrepreneur
    ) throws RepositoryException {
        return individualEntrepreneurDAO.create(individualEntrepreneur);
    }

    @Override
    public void delete(
            IndividualEntrepreneur individualEntrepreneur
    ) throws RepositoryException {
        individualEntrepreneurDAO.delete(individualEntrepreneur);
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        individualEntrepreneurDAO.delete(identifier);
    }
}
