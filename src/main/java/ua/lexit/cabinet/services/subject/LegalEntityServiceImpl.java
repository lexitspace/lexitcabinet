package ua.lexit.cabinet.services.subject;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.entities.subject.LegalEntity;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.repository.subject.LegalEntityDAO;

import java.util.List;

@Service
public class LegalEntityServiceImpl implements LegalEntityService {

    final private LegalEntityDAO legalEntityDAO;

    public LegalEntityServiceImpl(
            LegalEntityDAO legalEntityDAO
    ) {
        this.legalEntityDAO = legalEntityDAO;
    }

    @Override
    public LegalEntity renew(
            LegalEntity legalEntity
    ) throws RepositoryException {
        return legalEntityDAO.renew(legalEntity);
    }

    @Override
    public LegalEntity get(
            String identifier
    ) throws RepositoryException {
        return legalEntityDAO.get(identifier);
    }

    @Override
    public List<LegalEntity> getList() throws RepositoryException {
        return legalEntityDAO.getList();
    }

    @Override
    public List<LegalEntity> getList(
            List<String> legalEntityUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids,
            Integer limit,
            Integer offset
    ) throws RepositoryException {
        return legalEntityDAO.getList(
                legalEntityUuids,
                statusCodes,
                userUuids,
                addressUuids,
                limit,
                offset
        );
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        return legalEntityDAO.getListSize();
    }

    @Override
    public Integer getListSize(
            List<String> legalEntityUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids
    ) throws RepositoryException {
        return legalEntityDAO.getListSize(
                legalEntityUuids,
                statusCodes,
                userUuids,
                addressUuids
        );
    }

    @Override
    public LegalEntity create(
            LegalEntity legalEntity
    ) throws RepositoryException {
        return legalEntityDAO.create(legalEntity);
    }

    @Override
    public LegalEntity update(
            LegalEntity legalEntity
    ) throws RepositoryException {
        return legalEntityDAO.update(legalEntity);
    }

    @Override
    public void delete(
            LegalEntity legalEntity
    ) throws RepositoryException {
        legalEntityDAO.delete(legalEntity);
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        legalEntityDAO.delete(identifier);
    }
}