package ua.lexit.cabinet.Oldentities;

import lombok.*;

/**
 * Class {@link Status}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Status{

    private String uuid;
    private String code;
    private String title;

}