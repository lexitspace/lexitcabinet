package ua.lexit.cabinet.Oldentities;

import lombok.*;

import java.time.Instant;

/**
 * Class {@link Subject}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Subject {

    private String uuid;
    private Instant createDate;
    private Type type;
    private Status status;
    private User owner;
    private String title;
    private String legalCode;
    private Address address;

}
