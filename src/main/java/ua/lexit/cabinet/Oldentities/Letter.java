package ua.lexit.cabinet.Oldentities;

import lombok.*;

import java.time.Instant;

/**
 * Class {@link Letter}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Letter {

    private String uuid;
    private Instant createDate;
    private String number;
    private Type type;
    private Status status;
    private Subject subject;
    private Boolean watched;
    private Boolean notified;
    private Address deliveryAddress;
    private Attachment attachment;
    private Attachment preview;

}
