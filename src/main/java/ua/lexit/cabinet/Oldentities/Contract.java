package ua.lexit.cabinet.Oldentities;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.*;

import java.time.Instant;

/**
 * Class {@link Contract}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Contract {

    private String uuid;
    private Instant createDate;
    private String number;
    private Type type;
    private Status status;
    private Subject subjectContractor;
    private User userContractor;
    private User user;
    private Instant startDate;
    private Instant endDate;
    private ObjectNode attributes;
    private Attachment attachment;

}
