package ua.lexit.cabinet.Oldentities;

import lombok.*;

import java.time.Instant;

/**
 * Class {@link Token}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Token {

    private String uuid;
    private User user;
    private String token;
    private Instant createDate;
    private Instant expiryDate;

}
