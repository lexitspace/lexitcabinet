package ua.lexit.cabinet.Oldentities;

import lombok.*;

import java.time.Instant;

/**
 * Class {@link User}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class User {

    private String uuid;
    private Instant createDate;
    private Type type;
    private Status status;
    private String login;
    private String email;
    private String phone;
    private String password;
    private String firstName;
    private String secondName;
    private String lastName;
    private String shortName;
    private String fullName;

}
