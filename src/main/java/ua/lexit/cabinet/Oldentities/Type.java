package ua.lexit.cabinet.Oldentities;

import lombok.*;

/**
 * Class {@link Type}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Type {

    private String uuid;
    private String code;
    private String title;

}