package ua.lexit.cabinet.Oldentities;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.*;

import java.time.Instant;

/**
 * Class {@link Address}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Address {

    private String uuid;
    private Instant createDate;
    private User user;
    private Type type;
    private Status status;
    private ObjectNode attributes;

}
