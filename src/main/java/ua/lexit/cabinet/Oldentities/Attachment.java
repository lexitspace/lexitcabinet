package ua.lexit.cabinet.Oldentities;

import lombok.*;

import java.time.Instant;

/**
 * Class {@link Attachment}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Attachment {

    private String uuid;
    private Instant createDate;
    private Long size;
    private Type type;
    private byte[] data;

}
