package ua.lexit.cabinet.Oldentities;

import lombok.*;

import java.time.Instant;

/**
 * Class {@link Payment}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Payment {

    private String uuid;
    private Instant createDate;
    private Contract contract;
    private Type type;
    private Status status;
    private Instant planDate;
    private Instant payDate;
    private Double amount;
    private Attachment attachment;

}
