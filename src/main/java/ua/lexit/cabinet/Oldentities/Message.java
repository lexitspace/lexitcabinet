package ua.lexit.cabinet.Oldentities;

import lombok.*;

import java.time.Instant;

/**
 * Class {@link Message}
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Message {

    private String uuid;
    private Instant createDate;
    private Instant sendDate;
    private Type type;
    private Status status;
    private String subject;
    private String text;
    private String sender;
    private String recipient;

}
