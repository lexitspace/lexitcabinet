package ua.lexit.cabinet.enums.types;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserType {

    COLLABORATOR( "Співробітник"),
    CLIENT( "Клієнт");

    private String title;
}
