package ua.lexit.cabinet.enums.types;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AddressType {

    DELIVERY( "Адреса доставки"),
    JURIDICAL( "Юридична адреса");

    private String title;

}