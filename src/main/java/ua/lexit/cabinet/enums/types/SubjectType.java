package ua.lexit.cabinet.enums.types;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SubjectType {

    LEGAL_ENTITY( "Юридична особа"),
    INDIVIDUAL_ENTREPRENEUR( "Фізична особа підприємець");

    private String title;
}
