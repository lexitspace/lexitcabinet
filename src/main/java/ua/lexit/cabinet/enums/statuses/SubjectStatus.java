package ua.lexit.cabinet.enums.statuses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SubjectStatus {

    CREATING( "У процессі створення"),
    ACTIVE( "Обслуговується"),
    PAUSED ( "Обслуговування призупинено"),
    INACTIVE ( "Не обслуговується");

    private String title;
}
