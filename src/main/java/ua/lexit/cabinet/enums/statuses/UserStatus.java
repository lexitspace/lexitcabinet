package ua.lexit.cabinet.enums.statuses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserStatus {

    VERIFICATION( "Верифікація"),
    ACTIVE( "Активний користувач"),
    INACTIVE("Неактивний користувач");

    private String title;
}
