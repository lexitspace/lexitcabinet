package ua.lexit.cabinet.enums.statuses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AddressStatus {

    ACTIVE("Обслуговується"),
    INACTIVE( "Не обслуговується"),
    PAUSED("Ослуговування призупинено"),
    DELETED("Видалено");

    private String title;

}
