package ua.lexit.cabinet.enums.address.delivery;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AddressDeliveryService {

    LEXIT("Служба доставки Лексіт"),
    NOVA_POSHTA( "Нова пошта"),
    UKR_POSHTA("УкрПошта");

    private String title;

}
