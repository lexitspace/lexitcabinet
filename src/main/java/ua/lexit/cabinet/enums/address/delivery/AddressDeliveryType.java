package ua.lexit.cabinet.enums.address.delivery;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AddressDeliveryType {

    COURIER("Кур'єр"),
    WAREHOUSE("Доставка на відділення");

    private String title;

}
