package ua.lexit.cabinet.enums.address;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum GISFlatType {

    APARTMENT("Приміщення", "прим."),
    ROOM("Кімната", "кім."),
    OFFICE( "Офіс", "оф."),
    NON_RESIDENTIAL("Нежиле приміщення", "НП");

    private String title;
    private String abridgement;

}
