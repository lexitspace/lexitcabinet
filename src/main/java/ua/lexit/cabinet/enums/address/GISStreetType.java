package ua.lexit.cabinet.enums.address;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum GISStreetType {

    AVENUE("Проспект", "просп."),
    BOULEVARD( "Бульвар", "бульв."),
    STREET("Вулиця", "вул."),
    LANE("Провулок", "пров."),
    DESCENT("Узвіз", "узв."),
    IMPASSE("Тупик", "туп."),
    PASSAGE( "Проїзд", "проїзд"),
    SQUARE("Площа", "пл.");

    private String title;
    private String abridgement;

}