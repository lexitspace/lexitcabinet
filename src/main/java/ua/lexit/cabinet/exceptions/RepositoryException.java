package ua.lexit.cabinet.exceptions;

public class RepositoryException extends Exception{

    String message;

    RepositoryException() {
        message = "Unspecified";
    }

    public RepositoryException(String str) {
        message = str;
    }

    public String toString() {
        return ("Repository Exception Occurred: " + message);
    }

    @Override
    public String getLocalizedMessage() {
        return message;
    }

    @Override
    public String getMessage() {
        return "Repository Exception Occurred: " + message;
    }
}
