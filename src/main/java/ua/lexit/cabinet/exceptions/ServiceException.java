package ua.lexit.cabinet.exceptions;

public class ServiceException extends Exception{

    String message;

    public ServiceException() {
        message = "Unspecified";
    }

    public ServiceException(String str) {
        message = str;
    }

    public String toString() {
        return ("Service Action Exception Occurred: " + message);
    }

}
