package ua.lexit.cabinet.repository.token;

import ua.lexit.cabinet.entities.token.Token;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.util.List;

public interface TokenDAO {

    /**
     * Renew current {@link Token} record from the database.
     * @param token example of class that we want to renew in the database.
     * @return {@link Token} not null.
     * @throws RepositoryException
     */
    Token renew(
            Token token
    ) throws RepositoryException;

    /**
     * Get existing {@link Token} record from the database.
     * @param identifier - identifier of the getting record.
     * @return {@link Token} not null.
     * @throws RepositoryException
     */
    Token get(
            String identifier
    ) throws RepositoryException;

    /**
     * Get existing {@link Token} records from the database.
     * @return {@link List <Token>} not null.
     * @throws RepositoryException
     */
    List<Token> getList() throws RepositoryException;

    /**
     * Get existing {@link Token} records from the database.
     * @param tokenUuids  -
     * @param userUuids   -
     * @return {@link List <Token>} not null.
     * @throws RepositoryException
     */
    List<Token> getList(
            List<String> tokenUuids,
            List<String> userUuids
    ) throws RepositoryException;

    /**
     * Get size {@link Token} records from the database.
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize() throws RepositoryException;

    /**
     * Get size {@link Token} records from the database.
     * @param tokenUuids  -
     * @param userUuids   -           -
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize(
            List<String> tokenUuids,
            List<String> userUuids
    ) throws RepositoryException;

    /**
     * Create {@link Token} record to the database.
     * @param token example of class that we want store in the database.
     * @return {@link Token} not null.
     * @throws RepositoryException
     */
    Token create(
            Token token
    ) throws RepositoryException;

    /**
     * Update current {@link Token} record in the database.
     * @param token example of class that we want to update in the database.
     * @return {@link Token} not null.
     * @throws RepositoryException
     */
    Token update(
            Token token
    ) throws RepositoryException;

    /**
     * Delete current {@link Token} record from the database.
     * @param token example of class that we want to delete in the database.
     * @throws RepositoryException
     */
    void delete(
            Token token
    ) throws RepositoryException;

    /**
     * Delete existing {@link Token} record from the database by {@code uuid}.
     * @param identifier identifier of the deleting record.
     * @throws RepositoryException
     */
    void delete(
            String identifier
    ) throws RepositoryException;
}
