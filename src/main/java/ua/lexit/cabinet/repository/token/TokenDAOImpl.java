package ua.lexit.cabinet.repository.token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.lexit.cabinet.entities.token.Token;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.entities.users.Collaborator;
import ua.lexit.cabinet.entities.users.User;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.UserType;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class TokenDAOImpl implements TokenDAO {

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    final JdbcTemplate jdbcTemplate;

    public TokenDAOImpl(
            JdbcTemplate jdbcTemplate
    ) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public Token renew(
            Token token
    ) throws RepositoryException {
        if (token.getUuid() == null) {
            throw new RepositoryException("Token.uuid argument cannot be null.");
        }

        return get(token.getUuid());
    }

    @Override
    public Token get(
            String identifier
    ) throws RepositoryException {

        if (identifier == null) {
            throw new RepositoryException("Token.uuid (identifier argument) cannot be null.");
        }

        if (identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            try {
                String sql = "" +
                        "SELECT T.UUID, " +
                        "       T.CREATE_DATE, " +
                        "       U.UUID AS USER_UUID, " +
                        "       U.CREATE_DATE AS USER_CREATE_DATE, " +
                        "       U.TYPE AS USER_TYPE, " +
                        "       U.STATUS AS USER_STATUS, " +
                        "       U.LOGIN AS USER_LOGIN, " +
                        "       U.EMAIL AS USER_EMAIL, " +
                        "       U.PHONE AS USER_PHONE, " +
                        "       U.PASSWORD AS USER_PASSWORD, " +
                        "       U.FIRST_NAME AS USER_FIRST_NAME, " +
                        "       U.SECOND_NAME AS USER_SECOND_NAME, " +
                        "       U.LAST_NAME AS USER_LAST_NAME, " +
                        "       U.SHORT_NAME AS USER_SHORT_NAME, " +
                        "       U.FULL_NAME AS USER_FULL_NAME, " +
                        "       T.TOKEN, " +
                        "       T.EXPIRY_DATE " +
                        "FROM TOKEN T " +
                        "LEFT JOIN USER U ON T.USER_UUID = U.UUID " +
                        "WHERE T.UUID = ? ";

                log.debug("SQL QUERY : " + sql);

                return jdbcTemplate.queryForObject(sql, new Object[]{identifier}, new TokenRowMapper());
            } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
                throw new RepositoryException("Token get process wasn't successful. Record with Token.uuid=" + identifier + " doesn't exist in the database.");
            }
        } else {
            try {
                String sql = "" +
                        "SELECT T.UUID, " +
                        "       T.CREATE_DATE, " +
                        "       U.UUID AS USER_UUID, " +
                        "       U.CREATE_DATE AS USER_CREATE_DATE, " +
                        "       U.TYPE AS USER_TYPE, " +
                        "       U.STATUS AS USER_STATUS, " +
                        "       U.LOGIN AS USER_LOGIN, " +
                        "       U.EMAIL AS USER_EMAIL, " +
                        "       U.PHONE AS USER_PHONE, " +
                        "       U.PASSWORD AS USER_PASSWORD, " +
                        "       U.FIRST_NAME AS USER_FIRST_NAME, " +
                        "       U.SECOND_NAME AS USER_SECOND_NAME, " +
                        "       U.LAST_NAME AS USER_LAST_NAME, " +
                        "       U.SHORT_NAME AS USER_SHORT_NAME, " +
                        "       U.FULL_NAME AS USER_FULL_NAME, " +
                        "       T.TOKEN, " +
                        "       T.EXPIRY_DATE " +
                        "FROM TOKEN T " +
                        "LEFT JOIN USER U ON T.USER_UUID = U.UUID " +
                        "WHERE T.TOKEN = ? ";

                log.debug("SQL QUERY : " + sql);

                return jdbcTemplate.queryForObject(sql, new Object[]{identifier}, new TokenRowMapper());
            } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
                throw new RepositoryException("Token get process wasn't successful. Record with Token.token=" + identifier + " doesn't exist in the database.");
            }
        }
    }

    @Override
    public List<Token> getList() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT T.UUID, " +
                    "       T.CREATE_DATE, " +
                    "       U.UUID AS USER_UUID, " +
                    "       U.CREATE_DATE AS USER_CREATE_DATE, " +
                    "       U.TYPE AS USER_TYPE, " +
                    "       U.STATUS AS USER_STATUS, " +
                    "       U.LOGIN AS USER_LOGIN, " +
                    "       U.EMAIL AS USER_EMAIL, " +
                    "       U.PHONE AS USER_PHONE, " +
                    "       U.PASSWORD AS USER_PASSWORD, " +
                    "       U.FIRST_NAME AS USER_FIRST_NAME, " +
                    "       U.SECOND_NAME AS USER_SECOND_NAME, " +
                    "       U.LAST_NAME AS USER_LAST_NAME, " +
                    "       U.SHORT_NAME AS USER_SHORT_NAME, " +
                    "       U.FULL_NAME AS USER_FULL_NAME, " +
                    "       T.TOKEN, " +
                    "       T.EXPIRY_DATE " +
                    "FROM TOKEN T " +
                    "LEFT JOIN USER U ON T.USER_UUID = U.UUID " +
                    "WHERE 1=1 ";

            log.debug("SQL QUERY : " + sql);

            return jdbcTemplate.queryForObject(sql, new Object[]{}, new TokenRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Token> getList(
            List<String> tokenUuids,
            List<String> userUuids
    ) throws RepositoryException {

        try {
            String sql = "" +
                    "SELECT T.UUID, " +
                    "       T.CREATE_DATE, " +
                    "       U.UUID AS USER_UUID, " +
                    "       U.CREATE_DATE AS USER_CREATE_DATE, " +
                    "       U.TYPE AS USER_TYPE, " +
                    "       U.STATUS AS USER_STATUS, " +
                    "       U.LOGIN AS USER_LOGIN, " +
                    "       U.EMAIL AS USER_EMAIL, " +
                    "       U.PHONE AS USER_PHONE, " +
                    "       U.PASSWORD AS USER_PASSWORD, " +
                    "       U.FIRST_NAME AS USER_FIRST_NAME, " +
                    "       U.SECOND_NAME AS USER_SECOND_NAME, " +
                    "       U.LAST_NAME AS USER_LAST_NAME, " +
                    "       U.SHORT_NAME AS USER_SHORT_NAME, " +
                    "       U.FULL_NAME AS USER_FULL_NAME, " +
                    "       T.TOKEN, " +
                    "       T.EXPIRY_DATE " +
                    "FROM TOKEN T " +
                    "LEFT JOIN USER U on T.USER_UUID = U.UUID " +
                    "WHERE 1=1 ";

            if (tokenUuids != null) {
                if (tokenUuids.size() != 0) {
                    sql = sql + " AND T.UUID IN (";
                    for (int i = 0; i < tokenUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + tokenUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (userUuids != null) {
                if (userUuids.size() != 0) {
                    sql = sql + " AND U.UUID IN (";
                    for (int i = 0; i < userUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + userUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            log.debug("SQL QUERY : " + sql);

            return jdbcTemplate.queryForObject(sql, new Object[]{}, new TokenRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }

    }

    @Override
    public Integer getListSize() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM TOKEN T " +
                    "WHERE 1=1 ";

            log.debug("SQL QUERY : " + sql);

            return jdbcTemplate.queryForObject(sql, new Object[]{}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public Integer getListSize(
            List<String> tokenUuids,
            List<String> userUuids
    ) throws RepositoryException {

        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM TOKEN T " +
                    "LEFT JOIN USER U on T.USER_UUID = U.UUID " +
                    "WHERE 1=1 ";

            if (tokenUuids != null) {
                if (tokenUuids.size() != 0) {
                    sql = sql + " AND T.UUID IN (";
                    for (int i = 0; i < tokenUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + tokenUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (userUuids != null) {
                if (userUuids.size() != 0) {
                    sql = sql + " AND U.UUID IN (";
                    for (int i = 0; i < userUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + userUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            log.debug("SQL QUERY : " + sql);

            return jdbcTemplate.queryForObject(sql, new Object[]{}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public Token create(
            Token token
    ) throws RepositoryException {

        if (token.getUuid() != null) {
            throw new RepositoryException("Token.uuid argument cannot be filled.");
        }

        String sql = "" +
                "INSERT INTO TOKEN (UUID, CREATE_DATE, USER_UUID, TOKEN, EXPIRY_DATE) " +
                "VALUES (?,?,?,?,?)";

        log.debug("SQL QUERY : " + sql);

        String uuid = UUID.randomUUID().toString();
        if (jdbcTemplate.update(
                sql,
                uuid,
                ((token.getCreateDate() != null) ? Date.from(token.getCreateDate()) : null),
                ((token.getUser() != null) ? ((token.getUser().getUuid() != null) ? token.getUser().getUuid() : null) : null),
                ((token.getToken() != null) ? token.getToken() : null),
                ((token.getExpiryDate() != null) ? Date.from(token.getExpiryDate()) : null)
        ) == 1) {
            return get(uuid);
        } else {
            throw new RepositoryException("Token create process wasn't successful.");
        }
    }

    @Override
    public Token update(
            Token token
    ) throws RepositoryException {

        if ((token).getUuid() == null) {
            throw new RepositoryException("Token.uuid argument cannot be null.");
        }

        String sql = "" +
                "UPDATE TOKEN " +
                "   SET CREATE_DATE = ?, " +
                "       USER_UUID = ?, " +
                "       TOKEN = ?, " +
                "       EXPIRY_DATE = ? " +
                " WHERE UUID = ?";

        log.debug("SQL QUERY : " + sql);

        if (jdbcTemplate.update(
                sql,
                ((token.getCreateDate() != null) ? Date.from(token.getCreateDate()) : null),
                ((token.getUser() != null) ? ((token.getUser().getUuid() != null) ? token.getUser().getUuid() : null) : null),
                ((token.getToken() != null) ? token.getToken() : null),
                ((token.getExpiryDate() != null) ? Date.from(token.getExpiryDate()) : null),
                token.getUuid()
        ) == 1) {
            return get(token.getUuid());
        } else {
            throw new RepositoryException("Token update process wasn't successful.");
        }
    }

    @Override
    public void delete(
            Token token
    ) throws RepositoryException {

        if ((token).getUuid() == null) {
            throw new RepositoryException("Token.uuid argument cannot be null.");
        }

        delete((token).getUuid());
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {

        if (identifier == null) {
            throw new RepositoryException("Token.uuid (identifier argument) cannot be null.");
        }

        if (!identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            throw new RepositoryException("(" + identifier + ") Identifier argument is wrong format.");
        }

        String sql = "" +
                "DELETE " +
                "FROM TOKEN " +
                "WHERE UUID = ?";

        log.debug("SQL QUERY : " + sql);

        if (jdbcTemplate.update(sql, identifier) != 1) {
            throw new RepositoryException("Token delete process wasn't successful.");
        }
    }

    private static class TokenRowMapper implements RowMapper<Token> {
        @Override
        public Token mapRow(ResultSet rs, int rowNum) throws SQLException {
            Token element = new Token();
            element.setUuid(rs.getString("UUID"));
            element.setCreateDate(((rs.getObject("CREATE_DATE") != null) ? ((Timestamp) rs.getObject("CREATE_DATE")).toInstant() : null));

            if (rs.getObject("USER_UUID") != null) {
                User user = null;

                if (rs.getString("USER_TYPE").equals(UserType.COLLABORATOR.name())) {
                    user = new Collaborator();
                } else if (rs.getString("USER_TYPE").equals(UserType.CLIENT.name())) {
                    user = new Client();
                }

                if (user != null) {
                    user.setUuid(rs.getString("USER_UUID"));
                    user.setCreateDate(((rs.getObject("USER_CREATE_DATE") != null) ? ((Timestamp) rs.getObject("USER_CREATE_DATE")).toInstant() : null));
                    user.setType(((rs.getObject("USER_TYPE") != null) ? (UserType.valueOf(rs.getString("USER_TYPE"))) : null));
                    user.setStatus(((rs.getObject("USER_STATUS") != null) ? (UserStatus.valueOf(rs.getString("USER_STATUS"))) : null));
                    user.setLogin(rs.getString("USER_LOGIN"));
                    user.setEmail(rs.getString("USER_EMAIL"));
                    user.setPhone(rs.getString("USER_PHONE"));
                    user.setPassword(rs.getString("USER_PASSWORD"));
                    user.setFirstName(rs.getString("USER_FIRST_NAME"));
                    user.setSecondName(rs.getString("USER_SECOND_NAME"));
                    user.setLastName(rs.getString("USER_LAST_NAME"));
                    user.setShortName(rs.getString("USER_SHORT_NAME"));
                    user.setFullName(rs.getString("USER_FULL_NAME"));

                    element.setUser(user);
                } else {
                    element.setUser(null);
                }
            } else {
                element.setUser(null);
            }

            element.setToken(rs.getString("TOKEN"));
            element.setExpiryDate(((rs.getObject("EXPIRY_DATE") != null) ? ((Timestamp) rs.getObject("EXPIRY_DATE")).toInstant() : null));
            return element;
        }
    }

    private static class TokenRowMapperList implements RowMapper<List<Token>> {

        private final TokenRowMapper rowMapper = new TokenRowMapper();

        @Override
        public List<Token> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<Token> resultList = new ArrayList<>();
            boolean resultNext = true;

            while (resultNext) {
                Token element;

                element = rowMapper.mapRow(rs, rowNum);

                resultList.add(element);

                resultNext = rs.next();
            }

            return resultList;
        }
    }
}
