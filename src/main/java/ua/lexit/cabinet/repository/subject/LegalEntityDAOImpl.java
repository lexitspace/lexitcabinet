package ua.lexit.cabinet.repository.subject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.entities.subject.LegalEntity;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.statuses.AddressStatus;
import ua.lexit.cabinet.enums.statuses.SubjectStatus;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.AddressType;
import ua.lexit.cabinet.enums.types.SubjectType;
import ua.lexit.cabinet.enums.types.UserType;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class LegalEntityDAOImpl implements LegalEntityDAO {

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    final JdbcTemplate jdbcTemplate;

    public LegalEntityDAOImpl(
            JdbcTemplate jdbcTemplate
    ) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public LegalEntity renew(
            LegalEntity legalEntity
    ) throws RepositoryException {
        if (legalEntity.getUuid() == null) {
            throw new RepositoryException("LegalEntity.uuid argument cannot be null.");
        }

        return get(legalEntity.getUuid());
    }

    @Override
    public LegalEntity get(
            String identifier
    ) throws RepositoryException {
        if (identifier == null) {
            throw new RepositoryException("LegalEntity.uuid (identifier argument) cannot be null.");
        }

        if (!identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            throw new RepositoryException("(" + identifier + ") Identifier argument is wrong format.");
        }
        try {
            String sql = "" +
                    "SELECT S.UUID, " +
                    "       S.CREATE_DATE, " +
                    "       S.TYPE, " +
                    "       S.STATUS, " +
                    "       U.UUID         AS USER_UUID, " +
                    "       U.CREATE_DATE  AS USER_CREATE_DATE, " +
                    "       U.TYPE         AS USER_TYPE, " +
                    "       U.STATUS       AS USER_STATUS, " +
                    "       U.LOGIN        AS USER_LOGIN, " +
                    "       U.EMAIL        AS USER_EMAIL, " +
                    "       U.PHONE        AS USER_PHONE, " +
                    "       U.PASSWORD     AS USER_PASSWORD, " +
                    "       U.FIRST_NAME   AS USER_FIRST_NAME, " +
                    "       U.SECOND_NAME  AS USER_SECOND_NAME, " +
                    "       U.LAST_NAME    AS USER_LAST_NAME, " +
                    "       U.SHORT_NAME   AS USER_SHORT_NAME, " +
                    "       U.FULL_NAME    AS USER_FULL_NAME, " +
                    "       S.TITLE, " +
                    "       S.LEGAL_CODE, " +
                    "       AJ.UUID        AS ADDRESS_UUID, " +
                    "       AJ.CREATE_DATE AS ADDRESS_CREATE_DATE, " +
                    "       AJ.TYPE        AS ADDRESS_TYPE, " +
                    "       AJ.STATUS      AS ADDRESS_STATUS, " +
                    "       AJ.TITLE       AS ADDRESS_TITLE, " +
                    "       AJ.LIMITATION  AS ADDRESS_LIMITATION " +
                    "FROM SUBJECT S " +
                    "         LEFT JOIN USER U ON U.UUID = S.USER_UUID AND U.TYPE = ? " +
                    "         LEFT JOIN ADDRESS_JURIDICAL AJ ON AJ.UUID = S.ADDRESS_UUID " +
                    "WHERE S.UUID = ? " +
                    "  AND S.TYPE = ? ";

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.CLIENT.name(), identifier, SubjectType.LEGAL_ENTITY.name()}, new LegalEntityRowMapper());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            throw new RepositoryException("LegalEntity get process wasn't successful. Record with LegalEntity.uuid=" + identifier + " doesn't exist in the database.");
        }
    }

    @Override
    public List<LegalEntity> getList() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT S.UUID, " +
                    "       S.CREATE_DATE, " +
                    "       S.TYPE, " +
                    "       S.STATUS, " +
                    "       U.UUID         AS USER_UUID, " +
                    "       U.CREATE_DATE  AS USER_CREATE_DATE, " +
                    "       U.TYPE         AS USER_TYPE, " +
                    "       U.STATUS       AS USER_STATUS, " +
                    "       U.LOGIN        AS USER_LOGIN, " +
                    "       U.EMAIL        AS USER_EMAIL, " +
                    "       U.PHONE        AS USER_PHONE, " +
                    "       U.PASSWORD     AS USER_PASSWORD, " +
                    "       U.FIRST_NAME   AS USER_FIRST_NAME, " +
                    "       U.SECOND_NAME  AS USER_SECOND_NAME, " +
                    "       U.LAST_NAME    AS USER_LAST_NAME, " +
                    "       U.SHORT_NAME   AS USER_SHORT_NAME, " +
                    "       U.FULL_NAME    AS USER_FULL_NAME, " +
                    "       S.TITLE, " +
                    "       S.LEGAL_CODE, " +
                    "       AJ.UUID        AS ADDRESS_UUID, " +
                    "       AJ.CREATE_DATE AS ADDRESS_CREATE_DATE, " +
                    "       AJ.TYPE        AS ADDRESS_TYPE, " +
                    "       AJ.STATUS      AS ADDRESS_STATUS, " +
                    "       AJ.TITLE       AS ADDRESS_TITLE, " +
                    "       AJ.LIMITATION  AS ADDRESS_LIMITATION " +
                    "FROM SUBJECT S " +
                    "         LEFT JOIN USER U ON U.UUID = S.USER_UUID AND U.TYPE = ? " +
                    "         LEFT JOIN ADDRESS_JURIDICAL AJ ON AJ.UUID = S.ADDRESS_UUID " +
                    "WHERE 1=1 " +
                    "  AND S.TYPE = ? ";

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.CLIENT.name(), SubjectType.LEGAL_ENTITY.name()}, new LegalEntityRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<LegalEntity> getList(
            List<String> legalEntityUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids,
            Integer limit,
            Integer offset
    ) throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT S.UUID, " +
                    "       S.CREATE_DATE, " +
                    "       S.TYPE, " +
                    "       S.STATUS, " +
                    "       U.UUID         AS USER_UUID, " +
                    "       U.CREATE_DATE  AS USER_CREATE_DATE, " +
                    "       U.TYPE         AS USER_TYPE, " +
                    "       U.STATUS       AS USER_STATUS, " +
                    "       U.LOGIN        AS USER_LOGIN, " +
                    "       U.EMAIL        AS USER_EMAIL, " +
                    "       U.PHONE        AS USER_PHONE, " +
                    "       U.PASSWORD     AS USER_PASSWORD, " +
                    "       U.FIRST_NAME   AS USER_FIRST_NAME, " +
                    "       U.SECOND_NAME  AS USER_SECOND_NAME, " +
                    "       U.LAST_NAME    AS USER_LAST_NAME, " +
                    "       U.SHORT_NAME   AS USER_SHORT_NAME, " +
                    "       U.FULL_NAME    AS USER_FULL_NAME, " +
                    "       S.TITLE, " +
                    "       S.LEGAL_CODE, " +
                    "       AJ.UUID        AS ADDRESS_UUID, " +
                    "       AJ.CREATE_DATE AS ADDRESS_CREATE_DATE, " +
                    "       AJ.TYPE        AS ADDRESS_TYPE, " +
                    "       AJ.STATUS      AS ADDRESS_STATUS, " +
                    "       AJ.TITLE       AS ADDRESS_TITLE, " +
                    "       AJ.LIMITATION  AS ADDRESS_LIMITATION " +
                    "FROM SUBJECT S " +
                    "         LEFT JOIN USER U ON U.UUID = S.USER_UUID AND U.TYPE = ? " +
                    "         LEFT JOIN ADDRESS_JURIDICAL AJ ON AJ.UUID = S.ADDRESS_UUID " +
                    "WHERE 1=1 " +
                    "  AND S.TYPE = ? ";

            if (legalEntityUuids != null) {
                if (legalEntityUuids.size() != 0) {
                    sql = sql + " AND S.UUID IN (";
                    for (int i = 0; i < legalEntityUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + legalEntityUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (statusCodes != null) {
                if (statusCodes.size() != 0) {
                    sql = sql + " AND S.STATUS IN (";
                    for (int i = 0; i < statusCodes.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + statusCodes.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (userUuids != null) {
                if (userUuids.size() != 0) {
                    sql = sql + " AND U.UUID IN (";
                    for (int i = 0; i < userUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + userUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (addressUuids != null) {
                if (addressUuids.size() != 0) {
                    sql = sql + " AND AJ.UUID IN (";
                    for (int i = 0; i < addressUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + addressUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (limit != null) {
                // language=SQL
                sql = sql + " LIMIT " + limit;
                if (offset != null) {
                    // language=SQL
                    sql = sql + " OFFSET " + offset;
                }
            }

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.CLIENT.name(), SubjectType.LEGAL_ENTITY.name()}, new LegalEntityRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM SUBJECT S " +
                    "WHERE 1=1 " +
                    "  AND S.TYPE = ?";

            log.debug("SQL QUERY : " + sql);

            return jdbcTemplate.queryForObject(sql, new Object[]{SubjectType.LEGAL_ENTITY.name()}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public Integer getListSize(
            List<String> legalEntityUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids
    ) throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM SUBJECT S " +
                    "   LEFT JOIN USER U ON U.UUID = S.USER_UUID AND U.TYPE = ? " +
                    "   LEFT JOIN ADDRESS_JURIDICAL AJ ON AJ.UUID = S.ADDRESS_UUID " +
                    "WHERE 1=1 " +
                    "  AND S.TYPE = ? ";

            if (legalEntityUuids != null) {
                if (legalEntityUuids.size() != 0) {
                    sql = sql + " AND S.UUID IN (";
                    for (int i = 0; i < legalEntityUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + legalEntityUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (statusCodes != null) {
                if (statusCodes.size() != 0) {
                    sql = sql + " AND S.STATUS IN (";
                    for (int i = 0; i < statusCodes.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + statusCodes.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (userUuids != null) {
                if (userUuids.size() != 0) {
                    sql = sql + " AND U.UUID IN (";
                    for (int i = 0; i < userUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + userUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (addressUuids != null) {
                if (addressUuids.size() != 0) {
                    sql = sql + " AND AJ.UUID IN (";
                    for (int i = 0; i < addressUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + addressUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.CLIENT.name(), SubjectType.LEGAL_ENTITY.name()}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public LegalEntity create(
            LegalEntity legalEntity
    ) throws RepositoryException {
        if (legalEntity.getUuid() != null) {
            throw new RepositoryException("LegalEntity.uuid argument cannot be filled.");
        }

        String sql = "" +
                "INSERT INTO SUBJECT (UUID, CREATE_DATE, TYPE, STATUS, USER_UUID, TITLE, LEGAL_CODE, ADDRESS_UUID) " +
                "VALUES (?,?,?,?,?,?,?,?)";
        String uuid = UUID.randomUUID().toString();
        if (jdbcTemplate.update(
                sql,
                uuid,
                ((legalEntity.getCreateDate() != null) ? Date.from(legalEntity.getCreateDate()) : null),
                SubjectType.LEGAL_ENTITY.name(),
                ((legalEntity.getStatus() != null) ? legalEntity.getStatus().name() : null),
                ((legalEntity.getClient() != null) ? ((legalEntity.getClient().getUuid() != null) ? legalEntity.getClient().getUuid() : null) : null),
                ((legalEntity.getTitle() != null) ? legalEntity.getTitle() : null),
                ((legalEntity.getLegalCode() != null) ? legalEntity.getLegalCode() : null),
                ((legalEntity.getJuridicalAddress() != null) ? ((legalEntity.getJuridicalAddress().getUuid() != null) ? legalEntity.getJuridicalAddress().getUuid() : null) : null)
        ) == 1) {
            return get(uuid);
        } else {
            throw new RepositoryException("LegalEntity create process wasn't successful.");
        }
    }

    @Override
    public LegalEntity update(
            LegalEntity legalEntity
    ) throws RepositoryException {
        if (legalEntity.getUuid() == null) {
            throw new RepositoryException("LegalEntity.uuid argument cannot be null.");
        }

        String sql = "" +
                "UPDATE SUBJECT " +
                "   SET CREATE_DATE = ?, " +
                "       TYPE = ?, " +
                "       STATUS = ?, " +
                "       USER_UUID = ?, " +
                "       TITLE = ?, " +
                "       LEGAL_CODE = ?, " +
                "       ADDRESS_UUID = ? " +
                " WHERE UUID = ? " +
                "   AND TYPE = ? ";

        if (jdbcTemplate.update(
                sql,
                ((legalEntity.getCreateDate() != null) ? Date.from(legalEntity.getCreateDate()) : null),
                SubjectType.LEGAL_ENTITY.name(),
                ((legalEntity.getStatus() != null) ? legalEntity.getStatus().name() : null),
                ((legalEntity.getClient() != null) ? ((legalEntity.getClient().getUuid() != null) ? legalEntity.getClient().getUuid() : null) : null),
                ((legalEntity.getTitle() != null) ? legalEntity.getTitle() : null),
                ((legalEntity.getLegalCode() != null) ? legalEntity.getLegalCode() : null),
                ((legalEntity.getJuridicalAddress() != null) ? ((legalEntity.getJuridicalAddress().getUuid() != null) ? legalEntity.getJuridicalAddress().getUuid() : null) : null),
                legalEntity.getUuid(),
                SubjectType.LEGAL_ENTITY.name()
        ) == 1) {
            return get(legalEntity.getUuid());
        } else {
            throw new RepositoryException("LegalEntity update process wasn't successful.");
        }
    }

    @Override
    public void delete(
            LegalEntity legalEntity
    ) throws RepositoryException {
        if (legalEntity.getUuid() == null) {
            throw new RepositoryException("LegalEntity.uuid argument cannot be null.");
        }

        delete(legalEntity.getUuid());
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        if (identifier == null) {
            throw new RepositoryException("LegalEntity.uuid (identifier argument) cannot be null.");
        }

        if (!identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            throw new RepositoryException("(" + identifier + ") Identifier argument is wrong format.");
        }

        String sql = "" +
                "DELETE " +
                "FROM SUBJECT " +
                "WHERE UUID = ? " +
                "  AND TYPE = ? ";

        if (jdbcTemplate.update(sql, identifier, SubjectType.LEGAL_ENTITY.name()) != 1) {
            throw new RepositoryException("LegalEntity delete process wasn't successful.");
        }
    }


    private static class LegalEntityRowMapper implements RowMapper<LegalEntity> {
        @Override
        public LegalEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
            LegalEntity element = new LegalEntity();

            element.setUuid(rs.getString("UUID"));
            element.setCreateDate(((rs.getObject("CREATE_DATE") != null) ? ((Timestamp) rs.getObject("CREATE_DATE")).toInstant() : null));

            element.setType(((rs.getObject("TYPE") != null) ? (SubjectType.valueOf(rs.getString("TYPE"))) : null));
            element.setStatus(((rs.getObject("STATUS") != null) ? (SubjectStatus.valueOf(rs.getString("STATUS"))) : null));

            if (rs.getObject("USER_UUID") != null) {
                Client client = null;

                if (rs.getString("USER_TYPE").equals(UserType.CLIENT.name())) {
                    client = new Client();
                }

                if (client != null) {
                    client.setUuid(rs.getString("USER_UUID"));
                    client.setCreateDate(((rs.getObject("USER_CREATE_DATE") != null) ? ((Timestamp) rs.getObject("USER_CREATE_DATE")).toInstant() : null));

                    client.setType(((rs.getObject("USER_TYPE") != null) ? (UserType.valueOf(rs.getString("USER_TYPE"))) : null));
                    client.setStatus(((rs.getObject("USER_STATUS") != null) ? (UserStatus.valueOf(rs.getString("USER_STATUS"))) : null));

                    client.setLogin(rs.getString("USER_LOGIN"));
                    client.setEmail(rs.getString("USER_EMAIL"));
                    client.setPhone(rs.getString("USER_PHONE"));
                    client.setPassword(rs.getString("USER_PASSWORD"));
                    client.setFirstName(rs.getString("USER_FIRST_NAME"));
                    client.setSecondName(rs.getString("USER_SECOND_NAME"));
                    client.setLastName(rs.getString("USER_LAST_NAME"));
                    client.setShortName(rs.getString("USER_SHORT_NAME"));
                    client.setFullName(rs.getString("USER_FULL_NAME"));

                    element.setClient(client);
                } else {
                    element.setClient(null);
                }
            } else {
                element.setClient(null);
            }

            element.setTitle(rs.getString("TITLE"));
            element.setLegalCode(rs.getString("LEGAL_CODE"));

            if (rs.getObject("ADDRESS_UUID") != null) {
                JuridicalAddress juridicalAddress = null;

                if (rs.getString("ADDRESS_TYPE").equals(AddressType.JURIDICAL.name())) {
                    juridicalAddress = new JuridicalAddress();
                }

                if (juridicalAddress != null) {

                    juridicalAddress.setUuid(rs.getString("ADDRESS_UUID"));
                    juridicalAddress.setCreateDate(((rs.getObject("ADDRESS_CREATE_DATE") != null) ? ((Timestamp) rs.getObject("ADDRESS_CREATE_DATE")).toInstant() : null));

                    juridicalAddress.setType(((rs.getObject("ADDRESS_TYPE") != null) ? (AddressType.valueOf(rs.getString("ADDRESS_TYPE"))) : null));
                    juridicalAddress.setStatus(((rs.getObject("ADDRESS_STATUS") != null) ? (AddressStatus.valueOf(rs.getString("ADDRESS_STATUS"))) : null));

                    juridicalAddress.setTitle(rs.getString("ADDRESS_TITLE"));
                    juridicalAddress.setLimitation(rs.getInt("ADDRESS_LIMITATION"));

                    element.setJuridicalAddress(juridicalAddress);
                } else {
                    element.setJuridicalAddress(null);
                }
            } else {
                element.setJuridicalAddress(null);
            }

            return element;
        }
    }

    private static class LegalEntityRowMapperList implements RowMapper<List<LegalEntity>> {

        private final LegalEntityRowMapper rowMapper = new LegalEntityRowMapper();

        @Override
        public List<LegalEntity> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<LegalEntity> resultList = new ArrayList<>();
            boolean resultNext = true;
            while (resultNext) {
                LegalEntity element = rowMapper.mapRow(rs, rowNum);

                resultList.add(element);

                resultNext = rs.next();
            }

            return resultList;
        }
    }
}
