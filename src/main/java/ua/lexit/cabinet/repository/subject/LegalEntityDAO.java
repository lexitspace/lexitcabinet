package ua.lexit.cabinet.repository.subject;

import ua.lexit.cabinet.entities.subject.LegalEntity;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.util.List;

public interface LegalEntityDAO {

    /**
     * Renew current {@link LegalEntity} record from the database.
     *
     * @param legalEntity example of class that we want to renew in the database.
     * @return {@link LegalEntity} not null.
     * @throws RepositoryException
     */
    LegalEntity renew(
            LegalEntity legalEntity
    ) throws RepositoryException;

    /**
     * Get existing {@link LegalEntity} record from the database.
     *
     * @param identifier - identifier of the getting record.
     * @return {@link LegalEntity} not null.
     * @throws RepositoryException
     */
    LegalEntity get(
            String identifier
    ) throws RepositoryException;

    /**
     * Get existing {@link LegalEntity} records from the database.
     *
     * @return {@link List <LegalEntity>} not null.
     * @throws RepositoryException
     */
    List<LegalEntity> getList() throws RepositoryException;

    /**
     * Get existing {@link LegalEntity} records from the database.
     *
     * @param legalEntityUuids -
     * @param statusCodes      -
     * @param userUuids        -
     * @param addressUuids     -
     * @param limit            -
     * @param offset           -
     * @return {@link List <LegalEntity>} not null.
     * @throws RepositoryException
     */
    List<LegalEntity> getList(
            List<String> legalEntityUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids,
            Integer limit,
            Integer offset
    ) throws RepositoryException;

    /**
     * Get size {@link LegalEntity} records from the database.
     *
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize() throws RepositoryException;

    /**
     * Get size {@link LegalEntity} records from the database.
     *
     * @param legalEntityUuids -
     * @param statusCodes      -
     * @param userUuids        -
     * @param addressUuids     -
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize(
            List<String> legalEntityUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids
    ) throws RepositoryException;

    /**
     * Create {@link LegalEntity} record to the database.
     *
     * @param legalEntity example of class that we want store in the database.
     * @return {@link LegalEntity} not null.
     * @throws RepositoryException
     */
    LegalEntity create(
            LegalEntity legalEntity
    ) throws RepositoryException;

    /**
     * Update current {@link LegalEntity} record in the database.
     *
     * @param legalEntity example of class that we want to update in the database.
     * @return {@link LegalEntity} not null.
     * @throws RepositoryException
     */
    LegalEntity update(
            LegalEntity legalEntity
    ) throws RepositoryException;

    /**
     * Delete current {@link LegalEntity} record from the database.
     *
     * @param legalEntity example of class that we want to delete in the database.
     * @throws RepositoryException
     */
    void delete(
            LegalEntity legalEntity
    ) throws RepositoryException;

    /**
     * Delete existing {@link LegalEntity} record from the database by {@code uuid}.
     *
     * @param identifier identifier of the deleting record.
     * @throws RepositoryException
     */
    void delete(
            String identifier
    ) throws RepositoryException;
}
