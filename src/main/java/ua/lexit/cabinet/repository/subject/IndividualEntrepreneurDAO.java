package ua.lexit.cabinet.repository.subject;

import ua.lexit.cabinet.entities.subject.IndividualEntrepreneur;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.util.List;

public interface IndividualEntrepreneurDAO {

    /**
     * Renew current {@link IndividualEntrepreneur} record from the database.
     *
     * @param individualEntrepreneur example of class that we want to renew in the database.
     * @return {@link IndividualEntrepreneur} not null.
     * @throws RepositoryException
     */
    IndividualEntrepreneur renew(
            IndividualEntrepreneur individualEntrepreneur
    ) throws RepositoryException;

    /**
     * Get existing {@link IndividualEntrepreneur} record from the database.
     *
     * @param identifier - identifier of the getting record.
     * @return {@link IndividualEntrepreneur} not null.
     * @throws RepositoryException
     */
    IndividualEntrepreneur get(
            String identifier
    ) throws RepositoryException;

    /**
     * Get existing {@link IndividualEntrepreneur} records from the database.
     *
     * @return {@link List <IndividualEntrepreneur>} not null.
     * @throws RepositoryException
     */
    List<IndividualEntrepreneur> getList() throws RepositoryException;

    /**
     * Get existing {@link IndividualEntrepreneur} records from the database.
     *
     * @param individualEntrepreneurUuids -
     * @param statusCodes                 -
     * @param userUuids                   -
     * @param addressUuids                -
     * @param limit                       -
     * @param offset                      -
     * @return {@link List <IndividualEntrepreneur>} not null.
     * @throws RepositoryException
     */
    List<IndividualEntrepreneur> getList(
            List<String> individualEntrepreneurUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids,
            Integer limit,
            Integer offset
    ) throws RepositoryException;

    /**
     * Get size {@link IndividualEntrepreneur} records from the database.
     *
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize() throws RepositoryException;

    /**
     * Get size {@link IndividualEntrepreneur} records from the database.
     *
     * @param individualEntrepreneurUuids -
     * @param statusCodes                 -
     * @param userUuids                   -
     * @param addressUuids                -
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize(
            List<String> individualEntrepreneurUuids,
            List<String> statusCodes,
            List<String> userUuids,
            List<String> addressUuids
    ) throws RepositoryException;

    /**
     * Create {@link IndividualEntrepreneur} record to the database.
     *
     * @param individualEntrepreneur example of class that we want store in the database.
     * @return {@link IndividualEntrepreneur} not null.
     * @throws RepositoryException
     */
    IndividualEntrepreneur create(
            IndividualEntrepreneur individualEntrepreneur
    ) throws RepositoryException;

    /**
     * Update current {@link IndividualEntrepreneur} record in the database.
     *
     * @param individualEntrepreneur example of class that we want to update in the database.
     * @return {@link IndividualEntrepreneur} not null.
     * @throws RepositoryException
     */
    IndividualEntrepreneur update(
            IndividualEntrepreneur individualEntrepreneur
    ) throws RepositoryException;

    /**
     * Delete current {@link IndividualEntrepreneur} record from the database.
     *
     * @param individualEntrepreneur example of class that we want to delete in the database.
     * @throws RepositoryException
     */
    void delete(
            IndividualEntrepreneur individualEntrepreneur
    ) throws RepositoryException;

    /**
     * Delete existing {@link IndividualEntrepreneur} record from the database by {@code uuid}.
     *
     * @param identifier identifier of the deleting record.
     * @throws RepositoryException
     */
    void delete(
            String identifier
    ) throws RepositoryException;
}
