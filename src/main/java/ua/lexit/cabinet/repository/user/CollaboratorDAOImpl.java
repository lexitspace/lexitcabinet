package ua.lexit.cabinet.repository.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.lexit.cabinet.entities.users.Collaborator;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.UserType;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class CollaboratorDAOImpl implements CollaboratorDAO {

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    final JdbcTemplate jdbcTemplate;

    public CollaboratorDAOImpl(
            JdbcTemplate jdbcTemplate
    ) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public Collaborator renew(
            Collaborator collaborator
    ) throws RepositoryException {
        if (collaborator.getUuid() == null) {
            throw new RepositoryException("Collaborator.uuid argument cannot be null.");
        }

        return get(collaborator.getUuid());
    }

    @Override
    public Collaborator get(
            String identifier
    ) throws RepositoryException {
        if (identifier == null) {
            throw new RepositoryException("Collaborator.uuid (identifier argument) cannot be null.");
        }

        if (identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            try {
                String sql = "" +
                        "SELECT U.UUID, " +
                        "       U.CREATE_DATE, " +
                        "       U.TYPE, " +
                        "       U.STATUS, " +
                        "       U.LOGIN, " +
                        "       U.EMAIL, " +
                        "       U.PHONE, " +
                        "       U.PASSWORD, " +
                        "       U.FIRST_NAME, " +
                        "       U.SECOND_NAME, " +
                        "       U.LAST_NAME, " +
                        "       U.SHORT_NAME, " +
                        "       U.FULL_NAME " +
                        "FROM USER U " +
                        "WHERE U.UUID = ? " +
                        "  AND U.TYPE = ? ";

                return jdbcTemplate.queryForObject(sql, new Object[]{identifier, UserType.COLLABORATOR.name()}, new CollaboratorRowMapper());
            } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
                throw new RepositoryException("Collaborator get process wasn't successful. Record with Collaborator.uuid=" + identifier + " doesn't exist in the database.");
            }
        } else {
            try {
                String sql = "" +
                        "SELECT U.UUID, " +
                        "       U.CREATE_DATE, " +
                        "       U.TYPE, " +
                        "       U.STATUS, " +
                        "       U.LOGIN, " +
                        "       U.EMAIL, " +
                        "       U.PHONE, " +
                        "       U.PASSWORD, " +
                        "       U.FIRST_NAME, " +
                        "       U.SECOND_NAME, " +
                        "       U.LAST_NAME, " +
                        "       U.SHORT_NAME, " +
                        "       U.FULL_NAME " +
                        "FROM USER U " +
                        "WHERE U.TYPE = ? " +
                        "  AND (U.LOGIN = ? " +
                        "       OR U.PHONE = ? " +
                        "       OR U.EMAIL = ?) ";

                return jdbcTemplate.queryForObject(sql, new Object[]{UserType.COLLABORATOR.name(), identifier, identifier, identifier}, new CollaboratorRowMapper());
            } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
                throw new RepositoryException("Collaborator get process wasn't successful. " +
                        "Record with Collaborator.login=" + identifier + " or " +
                        "Collaborator.phone=" + identifier + " or " +
                        "Collaborator.email=" + identifier + " doesn't exist in the database.");
            }
        }
    }

    @Override
    public List<Collaborator> getList() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT U.UUID, " +
                    "       U.CREATE_DATE, " +
                    "       U.TYPE, " +
                    "       U.STATUS, " +
                    "       U.LOGIN, " +
                    "       U.EMAIL, " +
                    "       U.PHONE, " +
                    "       U.PASSWORD, " +
                    "       U.FIRST_NAME, " +
                    "       U.SECOND_NAME, " +
                    "       U.LAST_NAME, " +
                    "       U.SHORT_NAME, " +
                    "       U.FULL_NAME " +
                    "FROM USER U " +
                    "WHERE 1=1 " +
                    "  AND U.TYPE = ? ";

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.COLLABORATOR.name()}, new CollaboratorRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<Collaborator> getList(
            List<String> collaboratorUuids,
            List<String> statusCodes,
            Integer limit,
            Integer offset
    ) throws RepositoryException {

        try {
            String sql = "" +
                    "SELECT U.UUID, " +
                    "       U.CREATE_DATE, " +
                    "       U.TYPE, " +
                    "       U.STATUS, " +
                    "       U.LOGIN, " +
                    "       U.EMAIL, " +
                    "       U.PHONE, " +
                    "       U.PASSWORD, " +
                    "       U.FIRST_NAME, " +
                    "       U.SECOND_NAME, " +
                    "       U.LAST_NAME, " +
                    "       U.SHORT_NAME, " +
                    "       U.FULL_NAME " +
                    "FROM USER U " +
                    "WHERE 1=1 " +
                    "  AND U.TYPE = ? ";

            if (collaboratorUuids != null) {
                if (collaboratorUuids.size() != 0) {
                    sql = sql + " AND U.UUID IN (";
                    for (int i = 0; i < collaboratorUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + collaboratorUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (statusCodes!= null) {
                if (statusCodes.size() != 0) {
                    sql = sql + " AND U.STATUS IN (";
                    for (int i = 0; i < statusCodes.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + statusCodes.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (limit != null) {
                // language=SQL
                sql = sql + " LIMIT " + limit;
                if (offset != null) {
                    // language=SQL
                    sql = sql + " OFFSET " + offset;
                }
            }

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.COLLABORATOR.name()}, new CollaboratorRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }

    }

    @Override
    public Integer getListSize() throws RepositoryException {

        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM USER U " +
                    "WHERE 1=1 " +
                    "  AND U.TYPE = ? ";

            log.debug("SQL QUERY : " + sql);

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.COLLABORATOR.name()}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public Integer getListSize(
            List<String> collaboratorUuids,
            List<String> statusCodes
    ) throws RepositoryException {

        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM USER U " +
                    "WHERE 1=1 " +
                    "  AND U.TYPE = ? ";

            if (collaboratorUuids != null) {
                if (collaboratorUuids.size() != 0) {
                    sql = sql + " AND U.UUID IN (";
                    for (int i = 0; i < collaboratorUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + collaboratorUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (statusCodes != null) {
                if (statusCodes.size() != 0) {
                    sql = sql + " AND U.STATUS IN (";
                    for (int i = 0; i < statusCodes.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + statusCodes.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.COLLABORATOR.name()}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }

    }

    @Override
    public Collaborator create(
            Collaborator collaborator
    ) throws RepositoryException {

        if (collaborator.getUuid() != null) {
            throw new RepositoryException("Collaborator.uuid argument cannot be filled.");
        }

        String sql = "" +
                "INSERT INTO USER (UUID, CREATE_DATE, TYPE, STATUS, LOGIN, EMAIL, PHONE, PASSWORD, FIRST_NAME, SECOND_NAME, LAST_NAME, SHORT_NAME, FULL_NAME) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        String uuid = UUID.randomUUID().toString();
        if (jdbcTemplate.update(
                sql,
                uuid,
                ((collaborator.getCreateDate() != null) ? Date.from(collaborator.getCreateDate()) : null),
                UserType.COLLABORATOR.name(),
                ((collaborator.getStatus() != null) ? collaborator.getStatus().name() : null),
                ((collaborator.getLogin() != null) ? collaborator.getLogin() : null),
                ((collaborator.getEmail() != null) ? collaborator.getEmail() : null),
                ((collaborator.getPhone() != null) ? collaborator.getPhone() : null),
                ((collaborator.getPassword() != null) ? collaborator.getPassword() : null),
                ((collaborator.getFirstName() != null) ? collaborator.getFirstName() : null),
                ((collaborator.getSecondName() != null) ? collaborator.getSecondName() : null),
                ((collaborator.getLastName() != null) ? collaborator.getLastName() : null),
                ((collaborator.getShortName() != null) ? collaborator.getShortName() : null),
                ((collaborator.getFullName() != null) ? collaborator.getFullName() : null)
        ) == 1) {
            return get(uuid);
        } else {
            throw new RepositoryException("Collaborator create process wasn't successful.");
        }

    }

    @Override
    public Collaborator update(
            Collaborator collaborator
    ) throws RepositoryException {

        if (collaborator.getUuid() == null) {
            throw new RepositoryException("Collaborator.uuid argument cannot be null.");
        }

        String sql = "" +
                "UPDATE USER " +
                "   SET CREATE_DATE = ?, " +
                "       TYPE = ?, " +
                "       STATUS = ?, " +
                "       LOGIN = ?, " +
                "       EMAIL = ?, " +
                "       PHONE = ?, " +
                "       PASSWORD = ?, " +
                "       FIRST_NAME = ?, " +
                "       SECOND_NAME = ?, " +
                "       LAST_NAME = ?, " +
                "       SHORT_NAME = ?, " +
                "       FULL_NAME = ? " +
                " WHERE UUID = ? " +
                "   AND TYPE = ? ";

        if (jdbcTemplate.update(
                sql,
                ((collaborator.getCreateDate() != null) ? Date.from(collaborator.getCreateDate()) : null),
                UserType.COLLABORATOR.name(),
                ((collaborator.getStatus() != null) ? collaborator.getStatus().name() : null),
                ((collaborator.getLogin() != null) ? collaborator.getLogin() : null),
                ((collaborator.getEmail() != null) ? collaborator.getEmail() : null),
                ((collaborator.getPhone() != null) ? collaborator.getPhone() : null),
                ((collaborator.getPassword() != null) ? collaborator.getPassword() : null),
                ((collaborator.getFirstName() != null) ? collaborator.getFirstName() : null),
                ((collaborator.getSecondName() != null) ? collaborator.getSecondName() : null),
                ((collaborator.getLastName() != null) ? collaborator.getLastName() : null),
                ((collaborator.getShortName() != null) ? collaborator.getShortName() : null),
                ((collaborator.getFullName() != null) ? collaborator.getFullName() : null),
                collaborator.getUuid(),
                UserType.COLLABORATOR.name()
        ) == 1) {
            return get(collaborator.getUuid());
        } else {
            throw new RepositoryException("Collaborator update process wasn't successful.");
        }

    }

    @Override
    public void delete(
            Collaborator collaborator
    ) throws RepositoryException {
        if (collaborator.getUuid() == null) {
            throw new RepositoryException("Collaborator.uuid argument cannot be null.");
        }

        delete(collaborator.getUuid());
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {

        if (identifier == null) {
            throw new RepositoryException("Collaborator.uuid (identifier argument) cannot be null.");
        }

        if (!identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            throw new RepositoryException("(" + identifier + ") Identifier argument is wrong format.");
        }

        String sql = "" +
                "DELETE " +
                "FROM USER " +
                "WHERE UUID = ? " +
                "  AND TYPE = ? ";

        if (jdbcTemplate.update(sql, identifier, UserType.CLIENT) != 1) {
            throw new RepositoryException("Collaborator delete process wasn't successful.");
        }
    }

    private static class CollaboratorRowMapper implements RowMapper<Collaborator> {
        @Override
        public Collaborator mapRow(ResultSet rs, int rowNum) throws SQLException {

            Collaborator element = new Collaborator();

            element.setUuid(rs.getString("UUID"));
            element.setCreateDate(((rs.getObject("CREATE_DATE") != null) ? ((Timestamp) rs.getObject("CREATE_DATE")).toInstant() : null));

            element.setType(((rs.getObject("TYPE") != null) ? (UserType.valueOf(rs.getString("TYPE"))) : null));
            element.setStatus(((rs.getObject("STATUS") != null) ? (UserStatus.valueOf(rs.getString("STATUS"))) : null));

            element.setLogin(rs.getString("LOGIN"));
            element.setEmail(rs.getString("EMAIL"));
            element.setPhone(rs.getString("PHONE"));
            element.setPassword(rs.getString("PASSWORD"));
            element.setFirstName(rs.getString("FIRST_NAME"));
            element.setSecondName(rs.getString("SECOND_NAME"));
            element.setLastName(rs.getString("LAST_NAME"));
            element.setShortName(rs.getString("SHORT_NAME"));
            element.setFullName(rs.getString("FULL_NAME"));

            return element;
        }
    }

    private static class CollaboratorRowMapperList implements RowMapper<List<Collaborator>> {

        private final CollaboratorRowMapper rowMapper = new CollaboratorRowMapper();

        @Override
        public List<Collaborator> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<Collaborator> resultList = new ArrayList<>();
            boolean resultNext = true;

            while (resultNext) {
                Collaborator element;

                element = rowMapper.mapRow(rs, rowNum);

                resultList.add(element);

                resultNext = rs.next();
            }

            return resultList;
        }
    }
}


