package ua.lexit.cabinet.repository.address;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.enums.statuses.AddressStatus;
import ua.lexit.cabinet.enums.types.AddressType;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class JuridicalAddressDAOImpl implements JuridicalAddressDAO {

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    final JdbcTemplate jdbcTemplate;

    public JuridicalAddressDAOImpl(
            JdbcTemplate jdbcTemplate
    ) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public JuridicalAddress renew(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException {
        if (juridicalAddress.getUuid() == null) {
            throw new RepositoryException("JuridicalAddress.uuid argument cannot be null.");
        }

        return get(juridicalAddress.getUuid());
    }

    @Override
    public JuridicalAddress get(
            String identifier
    ) throws RepositoryException {
        if (identifier == null) {
            throw new RepositoryException("JuridicalAddress.uuid (identifier argument) cannot be null.");
        }

        if (!identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            throw new RepositoryException("(" + identifier + ") Identifier argument is wrong format.");
        }
        try {
            String sql = "" +
                    "SELECT AJ.UUID, " +
                    "       AJ.CREATE_DATE, " +
                    "       AJ.TYPE, " +
                    "       AJ.STATUS, " +
                    "       AJ.TITLE, " +
                    "       AJ.LIMITATION " +
                    "FROM ADDRESS_JURIDICAL AJ " +
                    "WHERE AJ.UUID = ? " +
                    "  AND AJ.TYPE = ? ";

            return jdbcTemplate.queryForObject(sql, new Object[]{identifier, AddressType.JURIDICAL.name()}, new JuridicalAddressRowMapper());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            throw new RepositoryException("JuridicalAddress get process wasn't successful. Record with JuridicalAddress.uuid=" + identifier + " doesn't exist in the database.");
        }
    }

    @Override
    public List<JuridicalAddress> getList() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT AJ.UUID, " +
                    "       AJ.CREATE_DATE, " +
                    "       AJ.TYPE, " +
                    "       AJ.STATUS, " +
                    "       AJ.TITLE, " +
                    "       AJ.LIMITATION " +
                    "FROM ADDRESS_JURIDICAL AJ " +
                    "WHERE 1=1 " +
                    "  AND AJ.TYPE = ? ";

            return jdbcTemplate.queryForObject(sql, new Object[]{AddressType.JURIDICAL.name()}, new JuridicalAddressRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<JuridicalAddress> getList(
            List<String> juridicalAddressUuids,
            List<String> statusCodes,
            Integer limit,
            Integer offset
    ) throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT AJ.UUID, " +
                    "       AJ.CREATE_DATE, " +
                    "       AJ.TYPE, " +
                    "       AJ.STATUS, " +
                    "       AJ.TITLE, " +
                    "       AJ.LIMITATION " +
                    "FROM ADDRESS_JURIDICAL AJ " +
                    "WHERE 1=1 " +
                    "  AND AJ.TYPE = ? ";

            if (juridicalAddressUuids != null) {
                if (juridicalAddressUuids.size() != 0) {
                    sql = sql + " AND AJ.UUID IN (";
                    for (int i = 0; i < juridicalAddressUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + juridicalAddressUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (statusCodes != null) {
                if (statusCodes.size() != 0) {
                    sql = sql + " AND AJ.STATUS IN (";
                    for (int i = 0; i < statusCodes.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + statusCodes.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (limit != null) {
                // language=SQL
                sql = sql + " LIMIT " + limit;
                if (offset != null) {
                    // language=SQL
                    sql = sql + " OFFSET " + offset;
                }
            }

            return jdbcTemplate.queryForObject(sql, new Object[]{AddressType.JURIDICAL.name()}, new JuridicalAddressRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM ADDRESS_JURIDICAL AJ " +
                    "WHERE 1=1 " +
                    "  AND AJ.TYPE = ?";

            log.debug("SQL QUERY : " + sql);

            return jdbcTemplate.queryForObject(sql, new Object[]{AddressType.JURIDICAL.name()}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public Integer getListSize(
            List<String> juridicalAddressUuids,
            List<String> statusCodes
    ) throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM ADDRESS_JURIDICAL AJ " +
                    "WHERE 1=1 " +
                    "  AND AJ.TYPE = ? ";

            if (juridicalAddressUuids != null) {
                if (juridicalAddressUuids.size() != 0) {
                    sql = sql + " AND AJ.UUID IN (";
                    for (int i = 0; i < juridicalAddressUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + juridicalAddressUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (statusCodes != null) {
                if (statusCodes.size() != 0) {
                    sql = sql + " AND AJ.STATUS IN (";
                    for (int i = 0; i < statusCodes.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + statusCodes.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            return jdbcTemplate.queryForObject(sql, new Object[]{AddressType.JURIDICAL.name()}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public JuridicalAddress create(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException {
        if (juridicalAddress.getUuid() != null) {
            throw new RepositoryException("JuridicalAddress.uuid argument cannot be filled.");
        }

        String sql = "" +
                "INSERT INTO ADDRESS_JURIDICAL (UUID, CREATE_DATE, TYPE, STATUS, TITLE, LIMITATION) " +
                "VALUES (?,?,?,?,?,?)";
        String uuid = UUID.randomUUID().toString();
        if (jdbcTemplate.update(
                sql,
                uuid,
                ((juridicalAddress.getCreateDate() != null) ? Date.from(juridicalAddress.getCreateDate()) : null),
                AddressType.JURIDICAL.name(),
                ((juridicalAddress.getStatus() != null) ? juridicalAddress.getStatus().name() : null),
                ((juridicalAddress.getTitle() != null) ? juridicalAddress.getTitle() : null),
                ((juridicalAddress.getLimitation() != null) ? juridicalAddress.getLimitation() : null)
        ) == 1) {
            return get(uuid);
        } else {
            throw new RepositoryException("JuridicalAddress create process wasn't successful.");
        }
    }

    @Override
    public JuridicalAddress update(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException {
        if (juridicalAddress.getUuid() == null) {
            throw new RepositoryException("JuridicalAddress.uuid argument cannot be null.");
        }

        String sql = "" +
                "UPDATE ADDRESS_JURIDICAL " +
                "   SET CREATE_DATE = ?, " +
                "       TYPE = ?, " +
                "       STATUS = ?, " +
                "       TITLE = ?, " +
                "       LIMITATION = ? " +
                " WHERE UUID = ? " +
                "   AND TYPE = ? ";

        if (jdbcTemplate.update(
                sql,
                ((juridicalAddress.getCreateDate() != null) ? Date.from(juridicalAddress.getCreateDate()) : null),
                AddressType.JURIDICAL.name(),
                ((juridicalAddress.getStatus() != null) ? juridicalAddress.getStatus().name() : null),
                ((juridicalAddress.getTitle() != null) ? juridicalAddress.getTitle() : null),
                ((juridicalAddress.getLimitation() != null) ? juridicalAddress.getLimitation() : null),
                juridicalAddress.getUuid(),
                AddressType.JURIDICAL.name()
        ) == 1) {
            return get(juridicalAddress.getUuid());
        } else {
            throw new RepositoryException("JuridicalAddress update process wasn't successful.");
        }
    }

    @Override
    public void delete(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException {
        if (juridicalAddress.getUuid() == null) {
            throw new RepositoryException("JuridicalAddress.uuid argument cannot be null.");
        }

        delete(juridicalAddress.getUuid());
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        if (identifier == null) {
            throw new RepositoryException("JuridicalAddress.uuid (identifier argument) cannot be null.");
        }

        if (!identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            throw new RepositoryException("(" + identifier + ") Identifier argument is wrong format.");
        }

        String sql = "" +
                "DELETE " +
                "FROM ADDRESS_JURIDICAL " +
                "WHERE UUID = ? " +
                "  AND TYPE = ? ";

        if (jdbcTemplate.update(sql, identifier, AddressType.JURIDICAL.name()) != 1) {
            throw new RepositoryException("JuridicalAddress delete process wasn't successful.");
        }
    }

    private static class JuridicalAddressRowMapper implements RowMapper<JuridicalAddress> {
        @Override
        public JuridicalAddress mapRow(ResultSet rs, int rowNum) throws SQLException {
            JuridicalAddress element = new JuridicalAddress();

            element.setUuid(rs.getString("UUID"));
            element.setCreateDate(((rs.getObject("CREATE_DATE") != null) ? ((Timestamp) rs.getObject("CREATE_DATE")).toInstant() : null));

            element.setType(((rs.getObject("TYPE") != null) ? (AddressType.valueOf(rs.getString("TYPE"))) : null));
            element.setStatus(((rs.getObject("STATUS") != null) ? (AddressStatus.valueOf(rs.getString("STATUS"))) : null));

            element.setTitle(rs.getString("TITLE"));
            element.setLimitation(rs.getInt("LIMITATION"));

            return element;
        }
    }

    private static class JuridicalAddressRowMapperList implements RowMapper<List<JuridicalAddress>> {

        private final JuridicalAddressRowMapper rowMapper = new JuridicalAddressRowMapper();

        @Override
        public List<JuridicalAddress> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<JuridicalAddress> resultList = new ArrayList<>();
            boolean resultNext = true;
            while (resultNext) {
                JuridicalAddress element = rowMapper.mapRow(rs, rowNum);

                resultList.add(element);

                resultNext = rs.next();
            }

            return resultList;
        }
    }
}
