package ua.lexit.cabinet.repository.address;

import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.util.List;

public interface JuridicalAddressDAO {

    /**
     * Renew current {@link JuridicalAddress} record from the database.
     *
     * @param juridicalAddress example of class that we want to renew in the database.
     * @return {@link JuridicalAddress} not null.
     * @throws RepositoryException
     */
    JuridicalAddress renew(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException;

    /**
     * Get existing {@link JuridicalAddress} record from the database.
     *
     * @param identifier - identifier of the getting record.
     * @return {@link JuridicalAddress} not null.
     * @throws RepositoryException
     */
    JuridicalAddress get(
            String identifier
    ) throws RepositoryException;

    /**
     * Get existing {@link JuridicalAddress} records from the database.
     *
     * @return {@link List <JuridicalAddress>} not null.
     * @throws RepositoryException
     */
    List<JuridicalAddress> getList() throws RepositoryException;

    /**
     * Get existing {@link JuridicalAddress} records from the database.
     *
     * @param juridicalAddressUuids -
     * @param statusCodes         -
     * @param limit                 -
     * @param offset                -
     * @return {@link List <JuridicalAddress>} not null.
     * @throws RepositoryException
     */
    List<JuridicalAddress> getList(
            List<String> juridicalAddressUuids,
            List<String> statusCodes,
            Integer limit,
            Integer offset
    ) throws RepositoryException;

    /**
     * Get size {@link JuridicalAddress} records from the database.
     *
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize() throws RepositoryException;

    /**
     * Get size {@link JuridicalAddress} records from the database.
     *
     * @param juridicalAddressUuids -
     * @param statusCodes           -
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize(
            List<String> juridicalAddressUuids,
            List<String> statusCodes
    ) throws RepositoryException;

    /**
     * Create {@link JuridicalAddress} record to the database.
     *
     * @param juridicalAddress example of class that we want store in the database.
     * @return {@link JuridicalAddress} not null.
     * @throws RepositoryException
     */
    JuridicalAddress create(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException;

    /**
     * Update current {@link JuridicalAddress} record in the database.
     *
     * @param juridicalAddress example of class that we want to update in the database.
     * @return {@link JuridicalAddress} not null.
     * @throws RepositoryException
     */
    JuridicalAddress update(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException;

    /**
     * Delete current {@link JuridicalAddress} record from the database.
     *
     * @param juridicalAddress example of class that we want to delete in the database.
     * @throws RepositoryException
     */
    void delete(
            JuridicalAddress juridicalAddress
    ) throws RepositoryException;

    /**
     * Delete existing {@link JuridicalAddress} record from the database by {@code uuid}.
     *
     * @param identifier identifier of the deleting record.
     * @throws RepositoryException
     */
    void delete(
            String identifier
    ) throws RepositoryException;
}
