package ua.lexit.cabinet.repository.address;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ua.lexit.cabinet.entities.address.DeliveryAddress;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.address.GISFlatType;
import ua.lexit.cabinet.enums.address.GISStreetType;
import ua.lexit.cabinet.enums.address.delivery.AddressDeliveryService;
import ua.lexit.cabinet.enums.address.delivery.AddressDeliveryType;
import ua.lexit.cabinet.enums.statuses.AddressStatus;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.AddressType;

import ua.lexit.cabinet.enums.types.UserType;
import ua.lexit.cabinet.exceptions.RepositoryException;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public class DeliveryAddressDAOImpl implements DeliveryAddressDAO {

    private Logger log = LoggerFactory.getLogger(this.getClass().getSimpleName());

    final JdbcTemplate jdbcTemplate;

    public DeliveryAddressDAOImpl(
            JdbcTemplate jdbcTemplate
    ) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public DeliveryAddress renew(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException {
        if (deliveryAddress.getUuid() == null) {
            throw new RepositoryException("DeliveryAddress.uuid argument cannot be null.");
        }

        return get(deliveryAddress.getUuid());
    }

    @Override
    public DeliveryAddress get(
            String identifier
    ) throws RepositoryException {
        if (identifier == null) {
            throw new RepositoryException("DeliveryAddress.uuid (identifier argument) cannot be null.");
        }

        if (!identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            throw new RepositoryException("(" + identifier + ") Identifier argument is wrong format.");
        }
        try {
            String sql = "" +
                    "SELECT AD.UUID, " +
                    "       AD.CREATE_DATE, " +
                    "       AD.TYPE, " +
                    "       AD.STATUS, " +
                    "       U.UUID        AS USER_UUID, " +
                    "       U.CREATE_DATE AS USER_CREATE_DATE, " +
                    "       U.TYPE        AS USER_TYPE, " +
                    "       U.STATUS      AS USER_STATUS, " +
                    "       U.LOGIN       AS USER_LOGIN, " +
                    "       U.EMAIL       AS USER_EMAIL, " +
                    "       U.PHONE       AS USER_PHONE, " +
                    "       U.PASSWORD    AS USER_PASSWORD, " +
                    "       U.FIRST_NAME  AS USER_FIRST_NAME, " +
                    "       U.SECOND_NAME AS USER_SECOND_NAME, " +
                    "       U.LAST_NAME   AS USER_LAST_NAME, " +
                    "       U.SHORT_NAME  AS USER_SHORT_NAME, " +
                    "       U.FULL_NAME   AS USER_FULL_NAME, " +
                    "       AD.DELIVERY_SERVICE, " +
                    "       AD.DELIVERY_TYPE, " +
                    "       AD.RECIPIENT, " +
                    "       AD.PHONE_NUMBER, " +
                    "       AD.CITY, " +
                    "       AD.STREET_TYPE, " +
                    "       AD.STREET, " +
                    "       AD.HOUSE, " +
                    "       AD.CORPUS, " +
                    "       AD.ENTRANCE, " +
                    "       AD.FLOOR, " +
                    "       AD.FLAT_TYPE, " +
                    "       AD.FLAT, " +
                    "       AD.MAILBOX, " +
                    "       AD.ZIP_CODE," +
                    "       AD.WAREHOUSE " +
                    "FROM ADDRESS_DELIVERY AD " +
                    "   LEFT JOIN USER U ON U.UUID = AD.USER_UUID AND U.TYPE = ? " +
                    "WHERE AD.UUID = ? " +
                    "  AND AD.TYPE = ? ";

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.CLIENT.name(), identifier, AddressType.DELIVERY.name()}, new DeliveryAddressRowMapper());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            throw new RepositoryException("DeliveryAddress get process wasn't successful. Record with DeliveryAddress.uuid=" + identifier + " doesn't exist in the database.");
        }
    }

    @Override
    public List<DeliveryAddress> getList() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT AD.UUID, " +
                    "       AD.CREATE_DATE, " +
                    "       AD.TYPE, " +
                    "       AD.STATUS, " +
                    "       U.UUID        AS USER_UUID, " +
                    "       U.CREATE_DATE AS USER_CREATE_DATE, " +
                    "       U.TYPE        AS USER_TYPE, " +
                    "       U.STATUS      AS USER_STATUS, " +
                    "       U.LOGIN       AS USER_LOGIN, " +
                    "       U.EMAIL       AS USER_EMAIL, " +
                    "       U.PHONE       AS USER_PHONE, " +
                    "       U.PASSWORD    AS USER_PASSWORD, " +
                    "       U.FIRST_NAME  AS USER_FIRST_NAME, " +
                    "       U.SECOND_NAME AS USER_SECOND_NAME, " +
                    "       U.LAST_NAME   AS USER_LAST_NAME, " +
                    "       U.SHORT_NAME  AS USER_SHORT_NAME, " +
                    "       U.FULL_NAME   AS USER_FULL_NAME, " +
                    "       AD.DELIVERY_SERVICE, " +
                    "       AD.DELIVERY_TYPE, " +
                    "       AD.RECIPIENT, " +
                    "       AD.PHONE_NUMBER, " +
                    "       AD.CITY, " +
                    "       AD.STREET_TYPE, " +
                    "       AD.STREET, " +
                    "       AD.HOUSE, " +
                    "       AD.CORPUS, " +
                    "       AD.ENTRANCE, " +
                    "       AD.FLOOR, " +
                    "       AD.FLAT_TYPE, " +
                    "       AD.FLAT, " +
                    "       AD.MAILBOX, " +
                    "       AD.ZIP_CODE," +
                    "       AD.WAREHOUSE " +
                    "FROM ADDRESS_DELIVERY AD " +
                    "   LEFT JOIN USER U ON U.UUID = AD.USER_UUID AND U.TYPE = ? " +
                    "WHERE 1=1 " +
                    "  AND AD.TYPE = ? ";

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.CLIENT.name(), AddressType.DELIVERY.name()}, new DeliveryAddressRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<DeliveryAddress> getList(
            List<String> deliveryAddressUuids,
            List<String> statusCodes,
            List<String> userUuids,
            Integer limit,
            Integer offset
    ) throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT AD.UUID, " +
                    "       AD.CREATE_DATE, " +
                    "       AD.TYPE, " +
                    "       AD.STATUS, " +
                    "       U.UUID        AS USER_UUID, " +
                    "       U.CREATE_DATE AS USER_CREATE_DATE, " +
                    "       U.TYPE        AS USER_TYPE, " +
                    "       U.STATUS      AS USER_STATUS, " +
                    "       U.LOGIN       AS USER_LOGIN, " +
                    "       U.EMAIL       AS USER_EMAIL, " +
                    "       U.PHONE       AS USER_PHONE, " +
                    "       U.PASSWORD    AS USER_PASSWORD, " +
                    "       U.FIRST_NAME  AS USER_FIRST_NAME, " +
                    "       U.SECOND_NAME AS USER_SECOND_NAME, " +
                    "       U.LAST_NAME   AS USER_LAST_NAME, " +
                    "       U.SHORT_NAME  AS USER_SHORT_NAME, " +
                    "       U.FULL_NAME   AS USER_FULL_NAME, " +
                    "       AD.DELIVERY_SERVICE, " +
                    "       AD.DELIVERY_TYPE, " +
                    "       AD.RECIPIENT, " +
                    "       AD.PHONE_NUMBER, " +
                    "       AD.CITY, " +
                    "       AD.STREET_TYPE, " +
                    "       AD.STREET, " +
                    "       AD.HOUSE, " +
                    "       AD.CORPUS, " +
                    "       AD.ENTRANCE, " +
                    "       AD.FLOOR, " +
                    "       AD.FLAT_TYPE, " +
                    "       AD.FLAT, " +
                    "       AD.MAILBOX, " +
                    "       AD.ZIP_CODE," +
                    "       AD.WAREHOUSE " +
                    "FROM ADDRESS_DELIVERY AD " +
                    "   LEFT JOIN USER U ON U.UUID = AD.USER_UUID AND U.TYPE = ? " +
                    "WHERE 1=1 " +
                    "  AND AD.TYPE = ? ";

            if (deliveryAddressUuids != null) {
                if (deliveryAddressUuids.size() != 0) {
                    sql = sql + " AND AD.UUID IN (";
                    for (int i = 0; i < deliveryAddressUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + deliveryAddressUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (statusCodes != null) {
                if (statusCodes.size() != 0) {
                    sql = sql + " AND AD.STATUS IN (";
                    for (int i = 0; i < statusCodes.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + statusCodes.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (userUuids != null) {
                if (userUuids.size() != 0) {
                    sql = sql + " AND U.UUID IN (";
                    for (int i = 0; i < userUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + userUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (limit != null) {
                // language=SQL
                sql = sql + " LIMIT " + limit;
                if (offset != null) {
                    // language=SQL
                    sql = sql + " OFFSET " + offset;
                }
            }

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.CLIENT.name(), AddressType.DELIVERY.name()}, new DeliveryAddressRowMapperList());
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return new ArrayList<>();
        }
    }

    @Override
    public Integer getListSize() throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM ADDRESS_DELIVERY AD " +
                    "WHERE 1=1 " +
                    "  AND AD.TYPE = ?";

            log.debug("SQL QUERY : " + sql);

            return jdbcTemplate.queryForObject(sql, new Object[]{AddressType.DELIVERY.name()}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public Integer getListSize(
            List<String> deliveryAddressUuids,
            List<String> statusCodes,
            List<String> userUuids
    ) throws RepositoryException {
        try {
            String sql = "" +
                    "SELECT COUNT(1) " +
                    "FROM ADDRESS_DELIVERY AD " +
                    "   LEFT JOIN USER U ON U.UUID = AD.USER_UUID AND U.TYPE = ? " +
                    "WHERE 1=1 " +
                    "  AND AD.TYPE = ? ";

            if (deliveryAddressUuids != null) {
                if (deliveryAddressUuids.size() != 0) {
                    sql = sql + " AND AD.UUID IN (";
                    for (int i = 0; i < deliveryAddressUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + deliveryAddressUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (statusCodes != null) {
                if (statusCodes.size() != 0) {
                    sql = sql + " AND AD.STATUS IN (";
                    for (int i = 0; i < statusCodes.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + statusCodes.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            if (userUuids != null) {
                if (userUuids.size() != 0) {
                    sql = sql + " AND U.UUID IN (";
                    for (int i = 0; i < userUuids.size(); i++) {
                        if (i != 0) {
                            sql = sql + ",";
                        }
                        sql = sql + "'" + userUuids.get(i) + "'";
                    }
                    sql = sql + ") ";
                }
            }

            return jdbcTemplate.queryForObject(sql, new Object[]{UserType.CLIENT.name(), AddressType.DELIVERY.name()}, Integer.class);
        } catch (org.springframework.dao.EmptyResultDataAccessException exception) {
            return 0;
        }
    }

    @Override
    public DeliveryAddress create(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException {
        if (deliveryAddress.getUuid() != null) {
            throw new RepositoryException("DeliveryAddress.uuid argument cannot be filled.");
        }

        String sql = "" +
                "INSERT INTO ADDRESS_DELIVERY (UUID, CREATE_DATE, TYPE, STATUS, USER_UUID, DELIVERY_SERVICE, DELIVERY_TYPE, RECIPIENT, PHONE_NUMBER, CITY, " +
                "STREET_TYPE, STREET, HOUSE, CORPUS, ENTRANCE, FLOOR, FLAT_TYPE, FLAT, MAILBOX, ZIP_CODE, WAREHOUSE) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        String uuid = UUID.randomUUID().toString();
        if (jdbcTemplate.update(
                sql,
                uuid,
                ((deliveryAddress.getCreateDate() != null) ? Date.from(deliveryAddress.getCreateDate()) : null),
                AddressType.DELIVERY.name(),
                ((deliveryAddress.getStatus() != null) ? deliveryAddress.getStatus().name() : null),
                ((deliveryAddress.getClient() != null) ? ((deliveryAddress.getClient().getUuid() != null) ? deliveryAddress.getClient().getUuid() : null) : null),
                ((deliveryAddress.getDeliveryService() != null) ? deliveryAddress.getDeliveryService().name() : null),
                ((deliveryAddress.getDeliveryType() != null) ? deliveryAddress.getDeliveryType().name() : null),
                ((deliveryAddress.getRecipient() != null) ? deliveryAddress.getRecipient() : null),
                ((deliveryAddress.getPhoneNumber() != null) ? deliveryAddress.getPhoneNumber() : null),
                ((deliveryAddress.getCity() != null) ? deliveryAddress.getCity() : null),
                ((deliveryAddress.getStreetType() != null) ? deliveryAddress.getStreetType().name() : null),
                ((deliveryAddress.getStreet() != null) ? deliveryAddress.getStreet() : null),
                ((deliveryAddress.getHouse() != null) ? deliveryAddress.getHouse() : null),
                ((deliveryAddress.getCorpus() != null) ? deliveryAddress.getCorpus() : null),
                ((deliveryAddress.getEntrance() != null) ? deliveryAddress.getEntrance() : null),
                ((deliveryAddress.getFloor() != null) ? deliveryAddress.getFloor() : null),
                ((deliveryAddress.getFlatType() != null) ? deliveryAddress.getFlatType().name() : null),
                ((deliveryAddress.getFlat() != null) ? deliveryAddress.getFlat() : null),
                ((deliveryAddress.getMailbox() != null) ? deliveryAddress.getMailbox() : null),
                ((deliveryAddress.getZipCode() != null) ? deliveryAddress.getZipCode() : null),
                ((deliveryAddress.getWarehouse() != null) ? deliveryAddress.getWarehouse() : null)
        ) == 1) {
            return get(uuid);
        } else {
            throw new RepositoryException("DeliveryAddress create process wasn't successful.");
        }
    }

    @Override
    public DeliveryAddress update(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException {
        if (deliveryAddress.getUuid() == null) {
            throw new RepositoryException("DeliveryAddress.uuid argument cannot be null.");
        }
        String sql = "" +
                "UPDATE ADDRESS_DELIVERY " +
                "   SET CREATE_DATE = ?, " +
                "       TYPE = ?, " +
                "       STATUS = ?, " +
                "       USER_UUID = ?, " +
                "       DELIVERY_SERVICE = ?, " +
                "       DELIVERY_TYPE = ?, " +
                "       RECIPIENT = ?, " +
                "       PHONE_NUMBER = ?, " +
                "       CITY = ?, " +
                "       STREET_TYPE = ?, " +
                "       STREET = ?, " +
                "       HOUSE = ?, " +
                "       CORPUS = ?, " +
                "       ENTRANCE = ?, " +
                "       FLOOR = ?, " +
                "       FLAT_TYPE = ?, " +
                "       FLAT = ?, " +
                "       MAILBOX = ?, " +
                "       ZIP_CODE = ?, " +
                "       WAREHOUSE = ? " +
                " WHERE UUID = ? " +
                "   AND TYPE = ? ";

        if (jdbcTemplate.update(
                sql,
                ((deliveryAddress.getCreateDate() != null) ? Date.from(deliveryAddress.getCreateDate()) : null),
                AddressType.DELIVERY.name(),
                ((deliveryAddress.getStatus() != null) ? deliveryAddress.getStatus().name() : null),
                ((deliveryAddress.getClient() != null) ? ((deliveryAddress.getClient().getUuid() != null) ? deliveryAddress.getClient().getUuid() : null) : null),
                ((deliveryAddress.getDeliveryService() != null) ? deliveryAddress.getDeliveryService().name() : null),
                ((deliveryAddress.getDeliveryType() != null) ? deliveryAddress.getDeliveryType().name() : null),
                ((deliveryAddress.getRecipient() != null) ? deliveryAddress.getRecipient() : null),
                ((deliveryAddress.getPhoneNumber() != null) ? deliveryAddress.getPhoneNumber() : null),
                ((deliveryAddress.getCity() != null) ? deliveryAddress.getCity() : null),
                ((deliveryAddress.getStreetType() != null) ? deliveryAddress.getStreetType().name() : null),
                ((deliveryAddress.getStreet() != null) ? deliveryAddress.getStreet() : null),
                ((deliveryAddress.getHouse() != null) ? deliveryAddress.getHouse() : null),
                ((deliveryAddress.getCorpus() != null) ? deliveryAddress.getCorpus() : null),
                ((deliveryAddress.getEntrance() != null) ? deliveryAddress.getEntrance() : null),
                ((deliveryAddress.getFloor() != null) ? deliveryAddress.getFloor() : null),
                ((deliveryAddress.getFlatType() != null) ? deliveryAddress.getFlatType().name() : null),
                ((deliveryAddress.getFlat() != null) ? deliveryAddress.getFlat() : null),
                ((deliveryAddress.getMailbox() != null) ? deliveryAddress.getMailbox() : null),
                ((deliveryAddress.getZipCode() != null) ? deliveryAddress.getZipCode() : null),
                ((deliveryAddress.getWarehouse() != null) ? deliveryAddress.getWarehouse() : null),
                deliveryAddress.getUuid(),
                AddressType.DELIVERY.name()
        ) == 1) {
            return get(deliveryAddress.getUuid());
        } else {
            throw new RepositoryException("DeliveryAddress update process wasn't successful.");
        }
    }

    @Override
    public void delete(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException {
        if (deliveryAddress.getUuid() == null) {
            throw new RepositoryException("DeliveryAddress.uuid argument cannot be null.");
        }

        delete(deliveryAddress.getUuid());
    }

    @Override
    public void delete(
            String identifier
    ) throws RepositoryException {
        if (identifier == null) {
            throw new RepositoryException("DeliveryAddress.uuid (identifier argument) cannot be null.");
        }

        if (!identifier.matches("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[34][0-9a-fA-F]{3}-[89ab][0-9a-fA-F]{3}-[0-9a-fA-F]{12}")) {
            throw new RepositoryException("(" + identifier + ") Identifier argument is wrong format.");
        }

        String sql = "" +
                "DELETE " +
                "FROM ADDRESS_DELIVERY " +
                "WHERE UUID = ? " +
                "  AND TYPE = ? ";

        if (jdbcTemplate.update(sql, identifier, AddressType.DELIVERY.name()) != 1) {
            throw new RepositoryException("DeliveryAddress delete process wasn't successful.");
        }
    }


    private static class DeliveryAddressRowMapper implements RowMapper<DeliveryAddress> {
        @Override
        public DeliveryAddress mapRow(ResultSet rs, int rowNum) throws SQLException {
            DeliveryAddress element = new DeliveryAddress();

            element.setUuid(rs.getString("UUID"));
            element.setCreateDate(((rs.getObject("CREATE_DATE") != null) ? ((Timestamp) rs.getObject("CREATE_DATE")).toInstant() : null));

            element.setType(((rs.getObject("TYPE") != null) ? (AddressType.valueOf(rs.getString("TYPE"))) : null));
            element.setStatus(((rs.getObject("STATUS") != null) ? (AddressStatus.valueOf(rs.getString("STATUS"))) : null));

            if (rs.getObject("USER_UUID") != null) {
                Client client = null;

                if (rs.getString("USER_TYPE").equals(UserType.CLIENT.name())) {
                    client = new Client();
                }

                if (client != null) {
                    client.setUuid(rs.getString("USER_UUID"));
                    client.setCreateDate(((rs.getObject("USER_CREATE_DATE") != null) ? ((Timestamp) rs.getObject("USER_CREATE_DATE")).toInstant() : null));

                    client.setType(((rs.getObject("USER_TYPE") != null) ? (UserType.valueOf(rs.getString("USER_TYPE"))) : null));
                    client.setStatus(((rs.getObject("USER_STATUS") != null) ? (UserStatus.valueOf(rs.getString("USER_STATUS"))) : null));

                    client.setLogin(rs.getString("USER_LOGIN"));
                    client.setEmail(rs.getString("USER_EMAIL"));
                    client.setPhone(rs.getString("USER_PHONE"));
                    client.setPassword(rs.getString("USER_PASSWORD"));
                    client.setFirstName(rs.getString("USER_FIRST_NAME"));
                    client.setSecondName(rs.getString("USER_SECOND_NAME"));
                    client.setLastName(rs.getString("USER_LAST_NAME"));
                    client.setShortName(rs.getString("USER_SHORT_NAME"));
                    client.setFullName(rs.getString("USER_FULL_NAME"));

                    element.setClient(client);
                } else {
                    element.setClient(null);
                }
            } else {
                element.setClient(null);
            }

            element.setDeliveryService(((rs.getObject("DELIVERY_SERVICE") != null) ? (AddressDeliveryService.valueOf(rs.getString("SERVICE"))) : null));
            element.setDeliveryType(((rs.getObject("DELIVERY_TYPE") != null) ? (AddressDeliveryType.valueOf(rs.getString("DELIVERY_TYPE"))) : null));

            element.setRecipient(rs.getString("RECIPIENT"));
            element.setPhoneNumber(rs.getString("PHONE_NUMBER"));
            element.setCity(rs.getString("CITY"));

            element.setStreetType(((rs.getObject("STREET_TYPE") != null) ? (GISStreetType.valueOf(rs.getString("STREET_TYPE"))) : null));

            element.setStreet(rs.getString("STREET"));
            element.setHouse(rs.getString("HOUSE"));
            element.setCorpus(rs.getString("CORPUS"));
            element.setEntrance(rs.getString("ENTRANCE"));
            element.setFloor(rs.getString("FLOOR"));

            element.setFlatType(((rs.getObject("FLAT_TYPE") != null) ? (GISFlatType.valueOf(rs.getString("FLAT_TYPE"))) : null));

            element.setFlat(rs.getString("FLAT"));
            element.setMailbox(rs.getString("MAILBOX"));
            element.setZipCode(rs.getString("ZIP_CODE"));
            element.setWarehouse(rs.getString("WAREHOUSE"));

            return element;
        }
    }

    private static class DeliveryAddressRowMapperList implements RowMapper<List<DeliveryAddress>> {

        private final DeliveryAddressRowMapper rowMapper = new DeliveryAddressRowMapper();

        @Override
        public List<DeliveryAddress> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<DeliveryAddress> resultList = new ArrayList<>();
            boolean resultNext = true;
            while (resultNext) {
                DeliveryAddress element = rowMapper.mapRow(rs, rowNum);

                resultList.add(element);

                resultNext = rs.next();
            }
            return resultList;
        }
    }
}
