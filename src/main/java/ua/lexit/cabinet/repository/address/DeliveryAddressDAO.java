package ua.lexit.cabinet.repository.address;

import ua.lexit.cabinet.entities.address.DeliveryAddress;
import ua.lexit.cabinet.exceptions.RepositoryException;

import java.util.List;

public interface DeliveryAddressDAO {

    /**
     * Renew current {@link DeliveryAddress} record from the database.
     *
     * @param deliveryAddress example of class that we want to renew in the database.
     * @return {@link DeliveryAddress} not null.
     * @throws RepositoryException
     */
    DeliveryAddress renew(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException;

    /**
     * Get existing {@link DeliveryAddress} record from the database.
     *
     * @param identifier - identifier of the getting record.
     * @return {@link DeliveryAddress} not null.
     * @throws RepositoryException
     */
    DeliveryAddress get(
            String identifier
    ) throws RepositoryException;

    /**
     * Get existing {@link DeliveryAddress} records from the database.
     *
     * @return {@link List <DeliveryAddress>} not null.
     * @throws RepositoryException
     */
    List<DeliveryAddress> getList() throws RepositoryException;

    /**
     * Get existing {@link DeliveryAddress} records from the database.
     *
     * @param deliveryAddressUuids -
     * @param statusCodes          -
     * @param userUuids            -
     * @param limit                -
     * @param offset               -
     * @return {@link List <DeliveryAddress>} not null.
     * @throws RepositoryException
     */
    List<DeliveryAddress> getList(
            List<String> deliveryAddressUuids,
            List<String> statusCodes,
            List<String> userUuids,
            Integer limit,
            Integer offset
    ) throws RepositoryException;

    /**
     * Get size {@link DeliveryAddress} records from the database.
     *
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize() throws RepositoryException;

    /**
     * Get size {@link DeliveryAddress} records from the database.
     *
     * @param deliveryAddressUuids -
     * @param statusCodes          -
     * @param userUuids            -
     * @return {@link Integer} not null.
     * @throws RepositoryException
     */
    Integer getListSize(
            List<String> deliveryAddressUuids,
            List<String> statusCodes,
            List<String> userUuids
    ) throws RepositoryException;

    /**
     * Create {@link DeliveryAddress} record to the database.
     *
     * @param deliveryAddress example of class that we want store in the database.
     * @return {@link DeliveryAddress} not null.
     * @throws RepositoryException
     */
    DeliveryAddress create(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException;

    /**
     * Update current {@link DeliveryAddress} record in the database.
     *
     * @param deliveryAddress example of class that we want to update in the database.
     * @return {@link DeliveryAddress} not null.
     * @throws RepositoryException
     */
    DeliveryAddress update(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException;

    /**
     * Delete current {@link DeliveryAddress} record from the database.
     *
     * @param deliveryAddress example of class that we want to delete in the database.
     * @throws RepositoryException
     */
    void delete(
            DeliveryAddress deliveryAddress
    ) throws RepositoryException;

    /**
     * Delete existing {@link DeliveryAddress} record from the database by {@code uuid}.
     *
     * @param identifier identifier of the deleting record.
     * @throws RepositoryException
     */
    void delete(
            String identifier
    ) throws RepositoryException;
}
