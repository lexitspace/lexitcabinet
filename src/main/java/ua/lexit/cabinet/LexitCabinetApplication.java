package ua.lexit.cabinet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class LexitCabinetApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(LexitCabinetApplication.class);
        Environment env = app.run(args).getEnvironment();

//        SystemConfiguration systemProperties = SystemConfiguration.getInstance();
//        systemProperties.setProfile(env.getProperty("system.profiles.active"));
//        systemProperties.setVersion(env.getProperty("system.version"));
//        systemProperties.setOwner(env.getProperty("system.owner"));
//        systemProperties.setFounder(env.getProperty("system.founder"));
//        systemProperties.setResourceExternalTitle(env.getProperty("system.resource.external.title"));
//        systemProperties.setResourceExternalLink(env.getProperty("system.resource.external.link"));
//        systemProperties.setVersionNumeric(Double.parseDouble(env.getProperty("system.version.numeric")));
    }

}
