package ua.lexit.cabinet.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

    @Bean
    @Profile("dev")
    public DataSource devDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://104.248.250.64:3306/cabinet_dev?characterEncoding=utf8&useUnicode=yes&useSSL=false&serverTimezone=UTC");
        dataSource.setUsername("cabinet_dev");
        dataSource.setPassword("UfmF27MXVqOyQbCU");
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setMaximumPoolSize(1);
        return dataSource;
    }

    @Bean
    @Profile("test")
    public DataSource testDataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://104.248.250.64:3306/cabinet_dev?characterEncoding=utf8&useUnicode=yes&useSSL=false&serverTimezone=UTC");
        dataSource.setUsername("cabinet_dev");
        dataSource.setPassword("UfmF27MXVqOyQbCU");
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setMaximumPoolSize(1);
        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(
            DataSource dataSource
    ) {
        return new JdbcTemplate(dataSource);
    }

}
