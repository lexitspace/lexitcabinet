package ua.lexit.cabinet.api.additional.request;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class ActionChangeStringRequest{

    String value;

}
