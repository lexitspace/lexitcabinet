package ua.lexit.cabinet.api.additional.responses;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class StateResponse {

    private Integer code;
    private String message;
}
