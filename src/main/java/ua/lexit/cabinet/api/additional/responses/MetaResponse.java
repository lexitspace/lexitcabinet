package ua.lexit.cabinet.api.additional.responses;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class MetaResponse {

    private Integer size;
    private Integer globalSize;
    private Integer pages;
    private Integer currentPage;
}
