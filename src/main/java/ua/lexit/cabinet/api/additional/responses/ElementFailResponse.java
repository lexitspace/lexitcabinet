package ua.lexit.cabinet.api.additional.responses;

import lombok.*;

 @NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ElementFailResponse extends ElementResponse{
    private StateResponse state;
}
