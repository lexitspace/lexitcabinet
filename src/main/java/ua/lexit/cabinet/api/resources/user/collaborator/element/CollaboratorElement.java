package ua.lexit.cabinet.api.resources.user.collaborator.element;

import lombok.*;
import ua.lexit.cabinet.api.resources.user.client.element.ClientElement;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CollaboratorElement {

    private String uuid;
    private Instant createDate;
    private ClientElement.Type type;
    private ClientElement.Status status;
    private String login;
    private String email;
    private String phone;
    private String password;
    private String firstName;
    private String secondName;
    private String lastName;
    private String shortName;
    private String fullName;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Type {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Status {
        private String code;
        private String title;
    }
}
