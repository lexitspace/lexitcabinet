package ua.lexit.cabinet.api.resources.address.juridical.requests;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class JuridicalAddressPatchRequest {

    private String title;
    private Integer limitation;
}
