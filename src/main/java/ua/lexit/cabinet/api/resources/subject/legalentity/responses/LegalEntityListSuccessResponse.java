package ua.lexit.cabinet.api.resources.subject.legalentity.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementListResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.subject.legalentity.element.LegalEntityElement;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class LegalEntityListSuccessResponse extends ElementListResponse {

    private List<LegalEntityElement> data;
    private MetaResponse meta;
    private StateResponse state;
}
