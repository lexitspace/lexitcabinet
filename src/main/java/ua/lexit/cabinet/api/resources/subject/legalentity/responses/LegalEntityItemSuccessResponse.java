package ua.lexit.cabinet.api.resources.subject.legalentity.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementItemResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.subject.legalentity.element.LegalEntityElement;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class LegalEntityItemSuccessResponse extends ElementItemResponse {

    private LegalEntityElement data;
    private StateResponse state;
}
