package ua.lexit.cabinet.api.resources.user.client.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.user.client.element.ClientElement;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.UserType;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientAdapter {

    public Client adapt(
            ClientElement clientElement
    ) {

        Client element = new Client();
        element.setUuid(clientElement.getUuid());
        element.setCreateDate(clientElement.getCreateDate());
        element.setType(UserType.CLIENT);
        element.setStatus(((clientElement.getStatus() != null) ? ((clientElement.getStatus().getCode() != null) ? UserStatus.valueOf(clientElement.getStatus().getCode()) : null) : null));
        element.setLogin(clientElement.getLogin());
        element.setEmail(clientElement.getEmail());
        element.setPhone(clientElement.getPhone());
        element.setPassword(null);
        element.setFirstName(clientElement.getFirstName());
        element.setSecondName(clientElement.getSecondName());
        element.setLastName(clientElement.getLastName());
        element.setShortName(clientElement.getShortName());
        element.setFullName(clientElement.getFullName());

        return element;
    }

    public List<Client> adapt(
            List<ClientElement> clientElements
    ) {
        List<Client> elements = new ArrayList<>();

        for (ClientElement element : clientElements) {
            elements.add(adapt(element));
        }
        return elements;
    }
}
