package ua.lexit.cabinet.api.resources.subject.individualentrepreneur.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementListResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.element.IndividualEntrepreneurElement;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class IndividualEntrepreneurListSuccessResponse extends ElementListResponse {

    private List<IndividualEntrepreneurElement> data;
    private MetaResponse meta;
    private StateResponse state;
}
