package ua.lexit.cabinet.api.resources.user.client.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class ClientPostRequest {

    private String login;
    private String email;
    private String phone;
    private String password;
    private String firstName;
    private String secondName;
    private String lastName;
    private String shortName;
    private String fullName;
}
