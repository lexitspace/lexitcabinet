package ua.lexit.cabinet.api.resources.subject.individualentrepreneur.element;

import lombok.*;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class IndividualEntrepreneurElement {

    private String uuid;
    private Instant createDate;
    private Type type;
    private Status status;
    private IEClient client;
    private String title;
    private String legalCode;
    private IEJuridicalAddress juridicalAddress;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Type {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Status {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class IEClient {
        private String uuid;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class IEJuridicalAddress {
        private String uuid;
    }

}
