package ua.lexit.cabinet.api.resources.subject.individualentrepreneur.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementItemResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.element.IndividualEntrepreneurElement;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class IndividualEntrepreneurItemSuccessResponse extends ElementItemResponse {

    private IndividualEntrepreneurElement data;
    private StateResponse state;
}
