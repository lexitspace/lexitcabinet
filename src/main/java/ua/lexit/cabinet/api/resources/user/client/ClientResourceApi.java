package ua.lexit.cabinet.api.resources.user.client;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.lexit.cabinet.api.additional.request.ActionChangeStringRequest;
import ua.lexit.cabinet.api.additional.request.ActionChangeStringVerifiedRequest;
import ua.lexit.cabinet.api.additional.responses.ElementFailResponse;
import ua.lexit.cabinet.api.additional.responses.ElementResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.user.client.adapter.ClientElementAdapter;
import ua.lexit.cabinet.api.resources.user.client.requests.ClientPatchRequest;
import ua.lexit.cabinet.api.resources.user.client.requests.ClientPostRequest;
import ua.lexit.cabinet.api.resources.user.client.requests.ClientPutRequest;
import ua.lexit.cabinet.api.resources.user.client.responses.ClientItemSuccessResponse;
import ua.lexit.cabinet.api.resources.user.client.responses.ClientListSuccessResponse;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.UserType;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.services.user.ClientService;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Client api controllers
 */
@RestController
@RequestMapping("/api")
public class ClientResourceApi {

    private final ClientService clientService;
    private final ClientElementAdapter clientElementAdapter;

    public ClientResourceApi(
            ClientService clientService,
            ClientElementAdapter clientElementAdapter
    ) {
        this.clientService = clientService;
        this.clientElementAdapter = clientElementAdapter;
    }

    @GetMapping("/clients")
    public ResponseEntity<ElementResponse> getClients(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestParam(value = "uuid", required = false) List<String> listUuid,
            @RequestParam(value = "status", required = false) List<String> listStatus,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {

            List<Client> resultList = clientService.getList(
                    listUuid,
                    listStatus,
                    limit,
                    offset
            );

            Integer size = resultList.size();
            Integer globalSize = clientService.getListSize(
                    listUuid,
                    listStatus
            );
            limit = ((limit == null) ? size : ((limit == 0) ? 1 : limit));
            offset = ((offset == null) ? 0 : offset);
            Integer pages = ((limit != 0) ? ((globalSize % limit > 0) ? globalSize / limit + 1 : globalSize / limit) : 0);
            Integer currentPage = ((limit != 0) ? ((offset != 0 && offset % limit > 0) ? offset / limit : offset / limit + 1) : 0);
            
            elementResponse = new ClientListSuccessResponse(
                    clientElementAdapter.adapt(
                            resultList
                    ),
                    new MetaResponse(
                            size,
                            globalSize,
                            pages,
                            currentPage
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @GetMapping("/clients/{identifier}")
    public ResponseEntity<ElementResponse> getClient(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "identifier") String identifier
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            elementResponse = new ClientItemSuccessResponse(
                    clientElementAdapter.adapt(
                            clientService.get(identifier)
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

        @PatchMapping("/clients/{uuid}")
        public ResponseEntity<ElementResponse> updateClientPatch(
                @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
                @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
                @PathVariable(value = "uuid") String uuid,
                @RequestBody ClientPatchRequest clientPatchRequest
        ) {
            ElementResponse elementResponse = null;
            HttpHeaders httpHeaders = new HttpHeaders();
            HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

            try {
                Client client = clientService.get(uuid);

                client.setFirstName(((clientPatchRequest.getFirstName() == null) ? client.getFirstName() : ((!clientPatchRequest.getFirstName().equals("null")) ? clientPatchRequest.getFirstName() : null)));
                client.setSecondName(((clientPatchRequest.getSecondName() == null) ? client.getSecondName() : ((!clientPatchRequest.getSecondName().equals("null")) ? clientPatchRequest.getSecondName() : null)));
                client.setLastName(((clientPatchRequest.getLastName() == null) ? client.getLastName() : ((!clientPatchRequest.getLastName().equals("null")) ? clientPatchRequest.getLastName() : null)));
                client.setShortName(((clientPatchRequest.getShortName() == null) ? client.getShortName() : ((!clientPatchRequest.getShortName().equals("null")) ? clientPatchRequest.getShortName() : null)));
                client.setFullName(((clientPatchRequest.getFullName() == null) ? client.getFullName() : ((!clientPatchRequest.getFullName().equals("null")) ? clientPatchRequest.getFullName() : null)));

                elementResponse = new ClientItemSuccessResponse(
                        clientElementAdapter.adapt(
                                clientService.update(
                                        client
                                )
                        ),
                        new StateResponse(
                                1,
                                "OK"
                        )
                );
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Charset", "UTF-8");
                httpStatus = HttpStatus.OK;

            } catch (RepositoryException e) {
                elementResponse = new ElementFailResponse(
                        new StateResponse(
                                42,
                                e.getLocalizedMessage()
                        )
                );
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Charset", "UTF-8");
                httpStatus = HttpStatus.BAD_REQUEST;
            }

            return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
        }

    @PutMapping("/clients/{uuid}")
    public ResponseEntity<ElementResponse> updateClientPut(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ClientPutRequest clientPutRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Client client = clientService.get(uuid);

            client.setFirstName(((!clientPutRequest.getFirstName().equals("null")) ? clientPutRequest.getFirstName() : null));
            client.setSecondName(((!clientPutRequest.getSecondName().equals("null")) ? clientPutRequest.getSecondName() : null));
            client.setLastName(((!clientPutRequest.getLastName().equals("null")) ? clientPutRequest.getLastName() : null));
            client.setShortName(((!clientPutRequest.getShortName().equals("null")) ? clientPutRequest.getShortName() : null));
            client.setFullName(((!clientPutRequest.getFullName().equals("null")) ? clientPutRequest.getFullName() : null));

            elementResponse = new ClientItemSuccessResponse(
                    clientElementAdapter.adapt(
                            clientService.update(
                                    client
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/clients")
    public ResponseEntity<ElementResponse> createClient(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestBody ClientPostRequest clientPostRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Client client = new Client();

            client.setCreateDate(ZonedDateTime.now(ZoneId.of("Europe/Kiev")).toInstant());
            client.setType(UserType.CLIENT);
            client.setStatus(UserStatus.VERIFICATION);
            client.setLogin(clientPostRequest.getLogin());
            client.setEmail(clientPostRequest.getEmail());
            client.setPhone(clientPostRequest.getPhone());
            client.setPassword(RandomStringUtils.randomNumeric(6));
            client.setFirstName(clientPostRequest.getFirstName());
            client.setSecondName(clientPostRequest.getSecondName());
            client.setLastName(clientPostRequest.getLastName());
            client.setShortName(clientPostRequest.getShortName());
            client.setFullName(clientPostRequest.getFullName());

            elementResponse = new ClientItemSuccessResponse(
                    clientElementAdapter.adapt(
                            clientService.update(
                                    client
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {

            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "Client wasn't created."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @DeleteMapping("/clients/{uuid}")
    public ResponseEntity<ElementResponse> deleteClient(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            clientService.delete(
                    uuid
            );
            httpStatus = HttpStatus.OK;
        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "Client wasn't delete."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    // Client REST Actions

    @PostMapping("/clients/{uuid}/change/status")
    public ResponseEntity<ElementResponse> createClientStatus(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            UserStatus.valueOf(actionChangeStringRequest.getValue());

            Client client = clientService.get(uuid);

            client.setStatus(UserStatus.valueOf(actionChangeStringRequest.getValue()));

            elementResponse = new ClientItemSuccessResponse(
                    clientElementAdapter.adapt(
                            clientService.update(
                                    client
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/clients/{uuid}/change/login")
    public ResponseEntity<ElementResponse> createClientLogin(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Client client = clientService.get(uuid);

            client.setLogin(actionChangeStringRequest.getValue());

            elementResponse = new ClientItemSuccessResponse(
                    clientElementAdapter.adapt(
                            clientService.update(
                                    client
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/clients/{uuid}/change/phone")
    public ResponseEntity<ElementResponse> createClientPhone(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Client client = clientService.get(uuid);

            client.setPhone(actionChangeStringRequest.getValue());

            elementResponse = new ClientItemSuccessResponse(
                    clientElementAdapter.adapt(
                            clientService.update(
                                    client
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/clients/{uuid}/change/email")
    public ResponseEntity<ElementResponse> createClientEmail(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Client client = clientService.get(uuid);

            client.setEmail(actionChangeStringRequest.getValue());

            elementResponse = new ClientItemSuccessResponse(
                    clientElementAdapter.adapt(
                            clientService.update(
                                    client
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/clients/{uuid}/change/password")
    public ResponseEntity<ElementResponse> createClientPassword(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringVerifiedRequest actionChangeStringVerifiedRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Client client = clientService.get(uuid);

            if (client.getPassword().equals(actionChangeStringVerifiedRequest.getOldValue())) {
                client.setPassword(actionChangeStringVerifiedRequest.getNewValue());
                elementResponse = new ClientItemSuccessResponse(
                        clientElementAdapter.adapt(
                                clientService.update(
                                        client
                                )
                        ),
                        new StateResponse(
                                1,
                                "OK"
                        )
                );
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Charset", "UTF-8");
                httpStatus = HttpStatus.OK;
            } else {
                elementResponse = new ElementFailResponse(
                        new StateResponse(
                                21,
                                "Something bad with credentials."
                        )
                );
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Charset", "UTF-8");
                httpStatus = HttpStatus.BAD_REQUEST;
            }

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }
}


