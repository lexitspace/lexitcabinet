package ua.lexit.cabinet.api.resources.user.client.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementItemResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.user.client.element.ClientElement;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ClientItemSuccessResponse extends ElementItemResponse {

    private ClientElement data;
    private StateResponse state;
}
