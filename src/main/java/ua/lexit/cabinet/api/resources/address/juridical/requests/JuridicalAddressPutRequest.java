package ua.lexit.cabinet.api.resources.address.juridical.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class JuridicalAddressPutRequest {

    private String title;
    private Integer limitation;
}
