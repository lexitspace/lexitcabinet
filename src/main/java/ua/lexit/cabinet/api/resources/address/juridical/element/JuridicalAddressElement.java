package ua.lexit.cabinet.api.resources.address.juridical.element;

import lombok.*;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class JuridicalAddressElement {

    private String uuid;
    private Instant createDate;
    private Type type;
    private Status status;
    private String title;
    private Integer limitation;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Type {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Status {
        private String code;
        private String title;
    }
}
