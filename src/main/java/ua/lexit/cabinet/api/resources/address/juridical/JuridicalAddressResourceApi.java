//package ua.lexit.cabinet.api.resources.address.juridical;
//
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import ua.lexit.cabinet.api.additional.request.ActionChangeStringRequest;
//import ua.lexit.cabinet.api.additional.responses.ElementFailResponse;
//import ua.lexit.cabinet.api.additional.responses.ElementResponse;
//import ua.lexit.cabinet.api.additional.responses.MetaResponse;
//import ua.lexit.cabinet.api.additional.responses.StateResponse;
//import ua.lexit.cabinet.api.resources.address.juridical.adapter.JuridicalAddressElementAdapter;
//import ua.lexit.cabinet.api.resources.address.juridical.requests.JuridicalAddressPatchRequest;
//import ua.lexit.cabinet.api.resources.address.juridical.requests.JuridicalAddressPostRequest;
//import ua.lexit.cabinet.api.resources.address.juridical.requests.JuridicalAddressPutRequest;
//import ua.lexit.cabinet.api.resources.address.juridical.responses.JuridicalAddressItemSuccessResponse;
//import ua.lexit.cabinet.api.resources.address.juridical.responses.JuridicalAddressListSuccessResponse;
//import ua.lexit.cabinet.entities.address.JuridicalAddress;
//
//import ua.lexit.cabinet.enums.statuses.AddressStatus;
//
//import ua.lexit.cabinet.enums.types.AddressType;
//
//import ua.lexit.cabinet.exceptions.RepositoryException;
//import ua.lexit.cabinet.services.address.JuridicalAddressService;
//
//import java.time.ZoneId;
//import java.time.ZonedDateTime;
//import java.util.List;
//
///**
// * JuridicalAddress api controllers
// */
//@RestController
//@RequestMapping("/api")
//public class JuridicalAddressResourceApi {
//
//    private final JuridicalAddressService juridicalAddressService;
//    private final JuridicalAddressElementAdapter juridicalAddressElementAdapter;
//
//    public JuridicalAddressResourceApi(
//            JuridicalAddressService juridicalAddressService,
//            JuridicalAddressElementAdapter juridicalAddressElementAdapter
//    ) {
//        this.juridicalAddressService = juridicalAddressService;
//        this.juridicalAddressElementAdapter = juridicalAddressElementAdapter;
//    }
//
//    @GetMapping("/addresses/juridical")
//    public ResponseEntity<ElementResponse> getJuridicalAddresses(
//            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
//            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
//            @RequestParam(value = "uuid", required = false) List<String> listUuid,
//            @RequestParam(value = "status", required = false) List<String> listStatus,
//            @RequestParam(value = "limit", required = false) Integer limit,
//            @RequestParam(value = "offset", required = false) Integer offset
//    ) {
//        ElementResponse elementResponse = null;
//        HttpHeaders httpHeaders = new HttpHeaders();
//        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;
//
//        try {
//
//            List<JuridicalAddress> resultList = juridicalAddressService.getList(
//                    listUuid,
//                    listStatus,
//                    limit,
//                    offset
//            );
//
//            Integer size = resultList.size();
//            Integer globalSize = juridicalAddressService.getListSize(
//                    listUuid,
//                    listStatus
//            );
//            limit = ((limit == null) ? size : ((limit == 0) ? 1 : limit));
//            offset = ((offset == null) ? 0 : offset);
//            Integer pages = ((limit != 0) ? ((globalSize % limit > 0) ? globalSize / limit + 1 : globalSize / limit) : 0);
//            Integer currentPage = ((limit != 0) ? ((offset != 0 && offset % limit > 0) ? offset / limit : offset / limit + 1) : 0);
//
//            elementResponse = new JuridicalAddressListSuccessResponse(
//                    juridicalAddressElementAdapter.adapt(
//                            resultList
//                    ),
//                    new MetaResponse(
//                            size,
//                            globalSize,
//                            pages,
//                            currentPage
//                    ),
//                    new StateResponse(
//                            1,
//                            "OK"
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.OK;
//
//        } catch (RepositoryException e) {
//            elementResponse = new ElementFailResponse(
//                    new StateResponse(
//                            42,
//                            e.getLocalizedMessage()
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.BAD_REQUEST;
//        }
//
//        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
//    }
//
//    @GetMapping("/addresses/juridical/{identifier}")
//    public ResponseEntity<ElementResponse> getJuridicalAddress(
//            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
//            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
//            @PathVariable(value = "identifier") String identifier
//    ) {
//        ElementResponse elementResponse = null;
//        HttpHeaders httpHeaders = new HttpHeaders();
//        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;
//
//        try {
//            elementResponse = new JuridicalAddressItemSuccessResponse(
//                    juridicalAddressElementAdapter.adapt(
//                            juridicalAddressService.get(identifier)
//                    ),
//                    new StateResponse(
//                            1,
//                            "OK"
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.OK;
//
//        } catch (RepositoryException e) {
//            elementResponse = new ElementFailResponse(
//                    new StateResponse(
//                            42,
//                            e.getLocalizedMessage()
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.BAD_REQUEST;
//        }
//
//        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
//    }
//
//    @PatchMapping("/addresses/juridical/{uuid}")
//    public ResponseEntity<ElementResponse> updateJuridicalAddressPatch(
//            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
//            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
//            @PathVariable(value = "uuid") String uuid,
//            @RequestBody JuridicalAddressPatchRequest juridicalAddressPatchRequest
//    ) {
//        ElementResponse elementResponse = null;
//        HttpHeaders httpHeaders = new HttpHeaders();
//        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;
//
//        try {
//            JuridicalAddress juridicalAddress = juridicalAddressService.get(uuid);
//
//            juridicalAddress.setTitle(((juridicalAddressPatchRequest.getTitle() == null) ? juridicalAddress.getTitle() : ((!juridicalAddressPatchRequest.getTitle().equals("null")) ? juridicalAddressPatchRequest.getTitle() : null)));
//            juridicalAddress.setLimitation(((juridicalAddressPatchRequest.getLimitation() == null) ? juridicalAddress.getLimitation() : ((!juridicalAddressPatchRequest.getLimitation().equals(0)) ? juridicalAddressPatchRequest.getLimitation() : null)));
//
//            elementResponse = new JuridicalAddressItemSuccessResponse(
//                    juridicalAddressElementAdapter.adapt(
//                            juridicalAddressService.update(
//                                    juridicalAddress
//                            )
//                    ),
//                    new StateResponse(
//                            1,
//                            "OK"
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.OK;
//
//        } catch (RepositoryException e) {
//            elementResponse = new ElementFailResponse(
//                    new StateResponse(
//                            42,
//                            e.getLocalizedMessage()
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.BAD_REQUEST;
//        }
//
//        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
//    }
//
//    @PutMapping("/addresses/juridical/{uuid}")
//    public ResponseEntity<ElementResponse> updateJuridicalAddressPut(
//            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
//            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
//            @PathVariable(value = "uuid") String uuid,
//            @RequestBody JuridicalAddressPutRequest juridicalAddressPutRequest
//    ) {
//        ElementResponse elementResponse = null;
//        HttpHeaders httpHeaders = new HttpHeaders();
//        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;
//
//        try {
//            JuridicalAddress juridicalAddress = juridicalAddressService.get(uuid);
//
//            juridicalAddress.setTitle(((!juridicalAddressPutRequest.getTitle().equals("null")) ? juridicalAddressPutRequest.getTitle() : null));
//            juridicalAddress.setLimitation(((!juridicalAddressPutRequest.getLimitation().equals(0)) ? juridicalAddressPutRequest.getLimitation() : null));
//
//
//            elementResponse = new JuridicalAddressItemSuccessResponse(
//                    juridicalAddressElementAdapter.adapt(
//                            juridicalAddressService.update(
//                                    juridicalAddress
//                            )
//                    ),
//                    new StateResponse(
//                            1,
//                            "OK"
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.OK;
//
//        } catch (RepositoryException e) {
//            elementResponse = new ElementFailResponse(
//                    new StateResponse(
//                            42,
//                            e.getLocalizedMessage()
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.BAD_REQUEST;
//        }
//
//        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
//    }
//
//    @PostMapping("/addresses/juridical")
//    public ResponseEntity<ElementResponse> createJuridicalAddress(
//            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
//            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
//            @RequestBody JuridicalAddressPostRequest juridicalAddressPostRequest
//    ) {
//        ElementResponse elementResponse = null;
//        HttpHeaders httpHeaders = new HttpHeaders();
//        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;
//
//        try {
//            JuridicalAddress juridicalAddress = new JuridicalAddress();
//
//            juridicalAddress.setCreateDate(ZonedDateTime.now(ZoneId.of("Europe/Kiev")).toInstant());
//            juridicalAddress.setType(AddressType.JURIDICAL);
//            juridicalAddress.setStatus(AddressStatus.INACTIVE);
//            juridicalAddress.setTitle(juridicalAddressPostRequest.getTitle());
//            juridicalAddress.setLimitation(juridicalAddressPostRequest.getLimitation());
//
//            elementResponse = new JuridicalAddressItemSuccessResponse(
//                    juridicalAddressElementAdapter.adapt(
//                            juridicalAddressService.update(
//                                    juridicalAddress
//                            )
//                    ),
//                    new StateResponse(
//                            1,
//                            "OK"
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.OK;
//
//        } catch (RepositoryException e) {
//
//            elementResponse = new ElementFailResponse(
//                    new StateResponse(
//                            42,
//                            "Client wasn't created."
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.BAD_REQUEST;
//        }
//
//        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
//    }
//
//
//    @DeleteMapping("/addresses/juridical/{uuid}")
//    public ResponseEntity<ElementResponse> deleteJuridicalAddress(
//            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
//            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
//            @PathVariable(value = "uuid") String uuid
//    ) {
//        ElementResponse elementResponse = null;
//        HttpHeaders httpHeaders = new HttpHeaders();
//        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;
//
//        try {
//            juridicalAddressService.delete(
//                    uuid
//            );
//            httpStatus = HttpStatus.OK;
//        } catch (RepositoryException e) {
//            elementResponse = new ElementFailResponse(
//                    new StateResponse(
//                            42,
//                            "JuridicalAddress wasn't delete."
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.BAD_REQUEST;
//        }
//
//        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
//    }
//
//    // JuridicalAddress REST Actions
//
//
//    @PostMapping("/addresses/juridical/{uuid}/change/status")
//    public ResponseEntity<ElementResponse> createJuridicalAddressStatus(
//            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
//            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
//            @PathVariable(value = "uuid") String uuid,
//            @RequestBody ActionChangeStringRequest actionChangeStringRequest
//    ) {
//        ElementResponse elementResponse = null;
//        HttpHeaders httpHeaders = new HttpHeaders();
//        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;
//
//        try {
//            AddressStatus.valueOf(actionChangeStringRequest.getValue());
//
//            JuridicalAddress juridicalAddress = juridicalAddressService.get(uuid);
//
//            juridicalAddress.setStatus(AddressStatus.valueOf(actionChangeStringRequest.getValue()));
//
//            elementResponse = new JuridicalAddressItemSuccessResponse(
//                    juridicalAddressElementAdapter.adapt(
//                            juridicalAddressService.update(
//                                    juridicalAddress
//                            )
//                    ),
//                    new StateResponse(
//                            1,
//                            "OK"
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.OK;
//
//        } catch (RepositoryException e) {
//            elementResponse = new ElementFailResponse(
//                    new StateResponse(
//                            42,
//                            e.getLocalizedMessage()
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.BAD_REQUEST;
//        } catch (IllegalArgumentException e) {
//            elementResponse = new ElementFailResponse(
//                    new StateResponse(
//                            66,
//                            e.getLocalizedMessage()
//                    )
//            );
//            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
//            httpHeaders.add("Charset", "UTF-8");
//            httpStatus = HttpStatus.BAD_REQUEST;
//        }
//
//        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
//    }
//}