package ua.lexit.cabinet.api.resources.address.delivery;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.lexit.cabinet.api.additional.request.ActionChangeStringRequest;
import ua.lexit.cabinet.api.additional.responses.ElementFailResponse;
import ua.lexit.cabinet.api.additional.responses.ElementResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.address.delivery.adapter.DeliveryAddressElementAdapter;
import ua.lexit.cabinet.api.resources.address.delivery.requests.DeliveryAddressPatchRequest;
import ua.lexit.cabinet.api.resources.address.delivery.requests.DeliveryAddressPostRequest;
import ua.lexit.cabinet.api.resources.address.delivery.requests.DeliveryAddressPutRequest;
import ua.lexit.cabinet.api.resources.address.delivery.responses.DeliveryAddressItemSuccessResponse;
import ua.lexit.cabinet.api.resources.address.delivery.responses.DeliveryAddressListSuccessResponse;
import ua.lexit.cabinet.api.resources.user.client.responses.ClientItemSuccessResponse;
import ua.lexit.cabinet.entities.address.DeliveryAddress;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.address.GISFlatType;
import ua.lexit.cabinet.enums.address.GISStreetType;
import ua.lexit.cabinet.enums.address.delivery.AddressDeliveryService;
import ua.lexit.cabinet.enums.address.delivery.AddressDeliveryType;
import ua.lexit.cabinet.enums.statuses.AddressStatus;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.AddressType;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.services.address.DeliveryAddressService;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * DeliveryAddress api controllers
 */
@RestController
@RequestMapping("/api")
public class DeliveryAddressResourceApi {

    private final DeliveryAddressService deliveryAddressService;
    private final DeliveryAddressElementAdapter deliveryAddressElementAdapter;

    public DeliveryAddressResourceApi(
            DeliveryAddressService deliveryAddressService,
            DeliveryAddressElementAdapter deliveryAddressElementAdapter
    ) {
        this.deliveryAddressService = deliveryAddressService;
        this.deliveryAddressElementAdapter = deliveryAddressElementAdapter;
    }

    @GetMapping("/addresses/delivery")
    public ResponseEntity<ElementResponse> getDeliveryAddresses(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestParam(value = "uuid", required = false) List<String> listUuid,
            @RequestParam(value = "status", required = false) List<String> listStatus,
            @RequestParam(value = "user", required = false) List<String> listUser,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {

            List<DeliveryAddress> resultList = deliveryAddressService.getList(
                    listUuid,
                    listStatus,
                    listUser,
                    limit,
                    offset
            );

            Integer size = resultList.size();
            Integer globalSize = deliveryAddressService.getListSize(
                    listUuid,
                    listStatus,
                    listUser
            );
            limit = ((limit == null) ? size : ((limit == 0) ? 1 : limit));
            offset = ((offset == null) ? 0 : offset);
            Integer pages = ((limit != 0) ? ((globalSize % limit > 0) ? globalSize / limit + 1 : globalSize / limit) : 0);
            Integer currentPage = ((limit != 0) ? ((offset != 0 && offset % limit > 0) ? offset / limit : offset / limit + 1) : 0);

            elementResponse = new DeliveryAddressListSuccessResponse(
                    deliveryAddressElementAdapter.adapt(
                            resultList
                    ),
                    new MetaResponse(
                            size,
                            globalSize,
                            pages,
                            currentPage
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @GetMapping("/addresses/delivery/{identifier}")
    public ResponseEntity<ElementResponse> getDeliveryAddress(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "identifier") String identifier
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            elementResponse = new DeliveryAddressItemSuccessResponse(
                    deliveryAddressElementAdapter.adapt(
                            deliveryAddressService.get(identifier)
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PatchMapping("/addresses/delivery/{uuid}")
    public ResponseEntity<ElementResponse> updateDeliveryAddressPatch(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody DeliveryAddressPatchRequest deliveryAddressPatchRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            DeliveryAddress deliveryAddress = deliveryAddressService.get(uuid);

            deliveryAddress.setDeliveryService(((deliveryAddressPatchRequest.getDeliveryService() != null) ? (((deliveryAddressPatchRequest.getDeliveryService().getCode() != null) ? ((!deliveryAddressPatchRequest.getDeliveryService().getCode().equals("null")) ? AddressDeliveryService.valueOf(deliveryAddressPatchRequest.getDeliveryService().getCode()) : null) : deliveryAddress.getDeliveryService())) : deliveryAddress.getDeliveryService()));
            deliveryAddress.setDeliveryType(((deliveryAddressPatchRequest.getDeliveryType() != null) ? (((deliveryAddressPatchRequest.getDeliveryType().getCode() != null) ? ((!deliveryAddressPatchRequest.getDeliveryType().getCode().equals("null")) ? AddressDeliveryType.valueOf(deliveryAddressPatchRequest.getDeliveryType().getCode()) : null) : deliveryAddress.getDeliveryType())) : deliveryAddress.getDeliveryType()));
            deliveryAddress.setRecipient(((deliveryAddressPatchRequest.getRecipient() == null) ? deliveryAddress.getRecipient() : ((!deliveryAddressPatchRequest.getRecipient().equals("null")) ? deliveryAddressPatchRequest.getRecipient() : null)));
            deliveryAddress.setPhoneNumber(((deliveryAddressPatchRequest.getPhoneNumber() == null) ? deliveryAddress.getPhoneNumber() : ((!deliveryAddressPatchRequest.getPhoneNumber().equals("null")) ? deliveryAddressPatchRequest.getPhoneNumber() : null)));
            deliveryAddress.setCity(((deliveryAddressPatchRequest.getCity() == null) ? deliveryAddress.getCity() : ((!deliveryAddressPatchRequest.getCity().equals("null")) ? deliveryAddressPatchRequest.getCity() : null)));
            deliveryAddress.setStreetType(((deliveryAddressPatchRequest.getStreetType() != null) ? (((deliveryAddressPatchRequest.getStreetType().getCode() != null) ? ((!deliveryAddressPatchRequest.getStreetType().getCode().equals("null")) ? GISStreetType.valueOf(deliveryAddressPatchRequest.getStreetType().getCode()) : null) : deliveryAddress.getStreetType())) : deliveryAddress.getStreetType()));
            deliveryAddress.setStreet(((deliveryAddressPatchRequest.getStreet() == null) ? deliveryAddress.getStreet() : ((!deliveryAddressPatchRequest.getStreet().equals("null")) ? deliveryAddressPatchRequest.getStreet() : null)));
            deliveryAddress.setHouse(((deliveryAddressPatchRequest.getHouse() == null) ? deliveryAddress.getHouse() : ((!deliveryAddressPatchRequest.getHouse().equals("null")) ? deliveryAddressPatchRequest.getHouse() : null)));
            deliveryAddress.setCorpus(((deliveryAddressPatchRequest.getCorpus() == null) ? deliveryAddress.getCorpus() : ((!deliveryAddressPatchRequest.getCorpus().equals("null")) ? deliveryAddressPatchRequest.getCorpus() : null)));
            deliveryAddress.setEntrance(((deliveryAddressPatchRequest.getEntrance() == null) ? deliveryAddress.getEntrance() : ((!deliveryAddressPatchRequest.getEntrance().equals("null")) ? deliveryAddressPatchRequest.getEntrance() : null)));
            deliveryAddress.setFloor(((deliveryAddressPatchRequest.getFloor() == null) ? deliveryAddress.getFloor() : ((!deliveryAddressPatchRequest.getFloor().equals("null")) ? deliveryAddressPatchRequest.getFloor() : null)));
            deliveryAddress.setFlatType(((deliveryAddressPatchRequest.getFlatType() != null) ? (((deliveryAddressPatchRequest.getFlatType().getCode() != null) ? ((!deliveryAddressPatchRequest.getFlatType().getCode().equals("null")) ? GISFlatType.valueOf(deliveryAddressPatchRequest.getFlatType().getCode()) : null) : deliveryAddress.getFlatType())) : deliveryAddress.getFlatType()));
            deliveryAddress.setFlat(((deliveryAddressPatchRequest.getFlat() == null) ? deliveryAddress.getFlat() : ((!deliveryAddressPatchRequest.getFlat().equals("null")) ? deliveryAddressPatchRequest.getFlat() : null)));
            deliveryAddress.setMailbox(((deliveryAddressPatchRequest.getMailbox() == null) ? deliveryAddress.getMailbox() : ((!deliveryAddressPatchRequest.getMailbox().equals("null")) ? deliveryAddressPatchRequest.getMailbox() : null)));
            deliveryAddress.setZipCode(((deliveryAddressPatchRequest.getZipCode() == null) ? deliveryAddress.getZipCode() : ((!deliveryAddressPatchRequest.getZipCode().equals("null")) ? deliveryAddressPatchRequest.getZipCode() : null)));
            deliveryAddress.setWarehouse(((deliveryAddressPatchRequest.getWarehouse() == null) ? deliveryAddress.getWarehouse() : ((!deliveryAddressPatchRequest.getWarehouse().equals("null")) ? deliveryAddressPatchRequest.getWarehouse() : null)));

            elementResponse = new DeliveryAddressItemSuccessResponse(
                    deliveryAddressElementAdapter.adapt(
                            deliveryAddressService.update(
                                    deliveryAddress
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PutMapping("/addresses/delivery/{uuid}")
    public ResponseEntity<ElementResponse> updateDeliveryAddressPut(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody DeliveryAddressPutRequest deliveryAddressPutRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            DeliveryAddress deliveryAddress = deliveryAddressService.get(uuid);

            deliveryAddress.setDeliveryService(((deliveryAddressPutRequest.getDeliveryService() != null) ? (((deliveryAddressPutRequest.getDeliveryService().getCode() != null) ? ((!deliveryAddressPutRequest.getDeliveryService().getCode().equals("null")) ? AddressDeliveryService.valueOf(deliveryAddressPutRequest.getDeliveryService().getCode()) : null) : deliveryAddress.getDeliveryService())) : deliveryAddress.getDeliveryService()));
            deliveryAddress.setDeliveryType(((deliveryAddressPutRequest.getDeliveryType() != null) ? (((deliveryAddressPutRequest.getDeliveryType().getCode() != null) ? ((!deliveryAddressPutRequest.getDeliveryType().getCode().equals("null")) ? AddressDeliveryType.valueOf(deliveryAddressPutRequest.getDeliveryType().getCode()) : null) : deliveryAddress.getDeliveryType())) : deliveryAddress.getDeliveryType()));
            deliveryAddress.setRecipient(((deliveryAddressPutRequest.getRecipient() == null) ? deliveryAddress.getRecipient() : ((!deliveryAddressPutRequest.getRecipient().equals("null")) ? deliveryAddressPutRequest.getRecipient() : null)));
            deliveryAddress.setPhoneNumber(((deliveryAddressPutRequest.getPhoneNumber() == null) ? deliveryAddress.getPhoneNumber() : ((!deliveryAddressPutRequest.getPhoneNumber().equals("null")) ? deliveryAddressPutRequest.getPhoneNumber() : null)));
            deliveryAddress.setCity(((deliveryAddressPutRequest.getCity() == null) ? deliveryAddress.getCity() : ((!deliveryAddressPutRequest.getCity().equals("null")) ? deliveryAddressPutRequest.getCity() : null)));
            deliveryAddress.setStreetType(((deliveryAddressPutRequest.getStreetType() != null) ? (((deliveryAddressPutRequest.getStreetType().getCode() != null) ? ((!deliveryAddressPutRequest.getStreetType().getCode().equals("null")) ? GISStreetType.valueOf(deliveryAddressPutRequest.getStreetType().getCode()) : null) : deliveryAddress.getStreetType())) : deliveryAddress.getStreetType()));
            deliveryAddress.setStreet(((deliveryAddressPutRequest.getStreet() == null) ? deliveryAddress.getStreet() : ((!deliveryAddressPutRequest.getStreet().equals("null")) ? deliveryAddressPutRequest.getStreet() : null)));
            deliveryAddress.setHouse(((deliveryAddressPutRequest.getHouse() == null) ? deliveryAddress.getHouse() : ((!deliveryAddressPutRequest.getHouse().equals("null")) ? deliveryAddressPutRequest.getHouse() : null)));
            deliveryAddress.setCorpus(((deliveryAddressPutRequest.getCorpus() == null) ? deliveryAddress.getCorpus() : ((!deliveryAddressPutRequest.getCorpus().equals("null")) ? deliveryAddressPutRequest.getCorpus() : null)));
            deliveryAddress.setEntrance(((deliveryAddressPutRequest.getEntrance() == null) ? deliveryAddress.getEntrance() : ((!deliveryAddressPutRequest.getEntrance().equals("null")) ? deliveryAddressPutRequest.getEntrance() : null)));
            deliveryAddress.setFloor(((deliveryAddressPutRequest.getFloor() == null) ? deliveryAddress.getFloor() : ((!deliveryAddressPutRequest.getFloor().equals("null")) ? deliveryAddressPutRequest.getFloor() : null)));
            deliveryAddress.setFlatType(((deliveryAddressPutRequest.getFlatType() != null) ? (((deliveryAddressPutRequest.getFlatType().getCode() != null) ? ((!deliveryAddressPutRequest.getFlatType().getCode().equals("null")) ? GISFlatType.valueOf(deliveryAddressPutRequest.getFlatType().getCode()) : null) : deliveryAddress.getFlatType())) : deliveryAddress.getFlatType()));
            deliveryAddress.setFlat(((deliveryAddressPutRequest.getFlat() == null) ? deliveryAddress.getFlat() : ((!deliveryAddressPutRequest.getFlat().equals("null")) ? deliveryAddressPutRequest.getFlat() : null)));
            deliveryAddress.setMailbox(((deliveryAddressPutRequest.getMailbox() == null) ? deliveryAddress.getMailbox() : ((!deliveryAddressPutRequest.getMailbox().equals("null")) ? deliveryAddressPutRequest.getMailbox() : null)));
            deliveryAddress.setZipCode(((deliveryAddressPutRequest.getZipCode() == null) ? deliveryAddress.getZipCode() : ((!deliveryAddressPutRequest.getZipCode().equals("null")) ? deliveryAddressPutRequest.getZipCode() : null)));
            deliveryAddress.setWarehouse(((deliveryAddressPutRequest.getWarehouse() == null) ? deliveryAddress.getWarehouse() : ((!deliveryAddressPutRequest.getWarehouse().equals("null")) ? deliveryAddressPutRequest.getWarehouse() : null)));


            elementResponse = new DeliveryAddressItemSuccessResponse(
                    deliveryAddressElementAdapter.adapt(
                            deliveryAddressService.update(
                                    deliveryAddress
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/addresses/delivery")
    public ResponseEntity<ElementResponse> createDeliveryAddress(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestBody DeliveryAddressPostRequest deliveryAddressPostRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            DeliveryAddress deliveryAddress = new DeliveryAddress();

            deliveryAddress.setCreateDate(ZonedDateTime.now(ZoneId.of("Europe/Kiev")).toInstant());
            deliveryAddress.setType(AddressType.DELIVERY);
            deliveryAddress.setStatus(AddressStatus.ACTIVE);

            if (deliveryAddressPostRequest.getClient() != null) {
                Client client = new Client();
                client.setUuid(deliveryAddressPostRequest.getClient().getUuid());
                deliveryAddress.setClient(client);
            } else {
                deliveryAddress.setClient(null);
            }

            deliveryAddress.setDeliveryService((deliveryAddressPostRequest.getDeliveryService() != null) ? AddressDeliveryService.valueOf(deliveryAddressPostRequest.getDeliveryService().getCode()) : null);
            deliveryAddress.setDeliveryType((deliveryAddressPostRequest.getDeliveryType() != null) ? AddressDeliveryType.valueOf(deliveryAddressPostRequest.getDeliveryType().getCode()) : null);
            deliveryAddress.setRecipient(deliveryAddressPostRequest.getRecipient());
            deliveryAddress.setPhoneNumber(deliveryAddressPostRequest.getPhoneNumber());
            deliveryAddress.setCity(deliveryAddressPostRequest.getCity());
            deliveryAddress.setStreetType((deliveryAddressPostRequest.getStreetType() != null) ? GISStreetType.valueOf(deliveryAddressPostRequest.getStreetType().getCode()) : null);
            deliveryAddress.setStreet(deliveryAddressPostRequest.getStreet());
            deliveryAddress.setHouse(deliveryAddressPostRequest.getHouse());
            deliveryAddress.setCorpus(deliveryAddressPostRequest.getCorpus());
            deliveryAddress.setEntrance(deliveryAddressPostRequest.getEntrance());
            deliveryAddress.setFloor(deliveryAddressPostRequest.getFloor());
            deliveryAddress.setFlatType((deliveryAddressPostRequest.getFlatType() != null) ? GISFlatType.valueOf(deliveryAddressPostRequest.getFlatType().getCode()) : null);
            deliveryAddress.setFlat(deliveryAddressPostRequest.getFlat());
            deliveryAddress.setMailbox(deliveryAddressPostRequest.getMailbox());
            deliveryAddress.setZipCode(deliveryAddressPostRequest.getZipCode());
            deliveryAddress.setWarehouse(deliveryAddressPostRequest.getWarehouse());

            elementResponse = new DeliveryAddressItemSuccessResponse(
                    deliveryAddressElementAdapter.adapt(
                            deliveryAddressService.update(
                                    deliveryAddress
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {

            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "DeliveryAddress wasn't created."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @DeleteMapping("/addresses/delivery/{uuid}")
    public ResponseEntity<ElementResponse> deleteClient(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            deliveryAddressService.delete(
                    uuid
            );
            httpStatus = HttpStatus.OK;
        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "DeliveryAddress wasn't delete."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    // DeliveryAddress REST Actions

    @PostMapping("/addresses/delivery/{uuid}/change/status")
    public ResponseEntity<ElementResponse> createClientStatus(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            AddressStatus.valueOf(actionChangeStringRequest.getValue());

            DeliveryAddress deliveryAddress = deliveryAddressService.get(uuid);

            deliveryAddress.setStatus(AddressStatus.valueOf(actionChangeStringRequest.getValue()));

            elementResponse = new DeliveryAddressItemSuccessResponse(
                    deliveryAddressElementAdapter.adapt(
                            deliveryAddressService.update(
                                    deliveryAddress
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }
}
