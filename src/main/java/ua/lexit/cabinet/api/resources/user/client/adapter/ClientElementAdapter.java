package ua.lexit.cabinet.api.resources.user.client.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.user.client.element.ClientElement;
import ua.lexit.cabinet.entities.users.Client;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientElementAdapter {

    public ClientElement adapt(
            Client client
    ) {

        return new ClientElement(
                client.getUuid(),
                client.getCreateDate(),
                ((client.getType() != null) ? new ClientElement.Type(
                        client.getType().name(),
                        client.getType().getTitle()
                ) : null),
                ((client.getStatus() != null) ? new ClientElement.Status(
                        client.getStatus().name(),
                        client.getStatus().getTitle()
                ) : null),
                client.getLogin(),
                client.getEmail(),
                client.getPhone(),
                null,
                client.getFirstName(),
                client.getSecondName(),
                client.getLastName(),
                client.getShortName(),
                client.getFullName()
        );
    }

    public List<ClientElement> adapt(
            List<Client> clients
    ) {
        List<ClientElement> elements = new ArrayList<>();

        for (Client element : clients) {
            elements.add(adapt(element));
        }

        return elements;
    }
}
