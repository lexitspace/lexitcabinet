package ua.lexit.cabinet.api.resources.subject.legalentity.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class LegalEntityPostRequest {

    private Status status;
    private LEClient client;
    private String title;
    private String legalCode;
    private LEJuridicalAddress juridicalAddress;

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class Status {
        private String status;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class LEClient {
        private String uuid;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class LEJuridicalAddress {
        private String uuid;
    }
}
