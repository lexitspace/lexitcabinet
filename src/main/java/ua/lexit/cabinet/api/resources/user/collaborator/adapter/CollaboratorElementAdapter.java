package ua.lexit.cabinet.api.resources.user.collaborator.adapter;


import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.user.client.element.ClientElement;
import ua.lexit.cabinet.api.resources.user.collaborator.element.CollaboratorElement;
import ua.lexit.cabinet.entities.users.Collaborator;

import java.util.ArrayList;
import java.util.List;

@Service
public class CollaboratorElementAdapter {

    public CollaboratorElement adapt(
            Collaborator collaborator
    ) {

        return new CollaboratorElement(
                collaborator.getUuid(),
                collaborator.getCreateDate(),
                ((collaborator.getType() != null) ? new ClientElement.Type(
                        collaborator.getType().name(),
                        collaborator.getType().getTitle()
                ) : null),
                ((collaborator.getStatus() != null) ? new ClientElement.Status(
                        collaborator.getStatus().name(),
                        collaborator.getStatus().getTitle()
                ) : null),
                collaborator.getLogin(),
                collaborator.getEmail(),
                collaborator.getPhone(),
                null,
                collaborator.getFirstName(),
                collaborator.getSecondName(),
                collaborator.getLastName(),
                collaborator.getShortName(),
                collaborator.getFullName()
        );
    }

    public List<CollaboratorElement> adapt(
            List<Collaborator> collaborators
    ) {
        List<CollaboratorElement> elements = new ArrayList<>();

        for (Collaborator element : collaborators) {
            elements.add(adapt(element));
        }

        return elements;
    }
}
