package ua.lexit.cabinet.api.resources.address.delivery.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementItemResponse;
import ua.lexit.cabinet.api.additional.responses.ElementListResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.address.delivery.element.DeliveryAddressElement;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class DeliveryAddressListSuccessResponse extends ElementListResponse {

    private List<DeliveryAddressElement> data;
    private MetaResponse meta;
    private StateResponse state;
}
