package ua.lexit.cabinet.api.resources.user.client.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementListResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.user.client.element.ClientElement;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ClientListSuccessResponse extends ElementListResponse {

    private List<ClientElement> data;
    private MetaResponse meta;
    private StateResponse state;
}
