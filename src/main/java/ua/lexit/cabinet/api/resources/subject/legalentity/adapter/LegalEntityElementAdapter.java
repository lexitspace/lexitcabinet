package ua.lexit.cabinet.api.resources.subject.legalentity.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.subject.legalentity.element.LegalEntityElement;
import ua.lexit.cabinet.entities.subject.LegalEntity;

import java.util.ArrayList;
import java.util.List;

@Service
public class LegalEntityElementAdapter {

    public LegalEntityElement adapt(
            LegalEntity legalEntity
    ) {
        return new LegalEntityElement(
                legalEntity.getUuid(),
                legalEntity.getCreateDate(),
                ((legalEntity.getType() != null) ? new LegalEntityElement.Type(
                        legalEntity.getType().name(),
                        legalEntity.getType().getTitle()
                ) : null),
                ((legalEntity.getStatus() != null) ? new LegalEntityElement.Status(
                        legalEntity.getStatus().name(),
                        legalEntity.getStatus().getTitle()
                ) : null),
                ((legalEntity.getClient() != null) ? new LegalEntityElement.LEClient(
                        legalEntity.getClient().getUuid()
                ) : null),
                legalEntity.getTitle(),
                legalEntity.getLegalCode(),
                ((legalEntity.getJuridicalAddress() != null) ? new LegalEntityElement.LEJuridicalAddress(
                        legalEntity.getJuridicalAddress().getUuid()
                ) : null)
        );
    }

    public List<LegalEntityElement> adapt(List<LegalEntity> legalEntities) {
        List<LegalEntityElement> elements = new ArrayList<>();

        for (LegalEntity element : legalEntities) {
            elements.add(adapt(element));
        }

        return elements;
    }
}
