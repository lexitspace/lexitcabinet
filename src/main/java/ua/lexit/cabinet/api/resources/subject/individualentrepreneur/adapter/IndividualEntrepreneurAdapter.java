package ua.lexit.cabinet.api.resources.subject.individualentrepreneur.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.element.IndividualEntrepreneurElement;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.entities.subject.IndividualEntrepreneur;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.statuses.SubjectStatus;
import ua.lexit.cabinet.enums.types.SubjectType;

import java.util.ArrayList;
import java.util.List;

@Service
public class IndividualEntrepreneurAdapter {

    public IndividualEntrepreneur adapt(
            IndividualEntrepreneurElement individualEntrepreneurElement
    ) {

        IndividualEntrepreneur element = new IndividualEntrepreneur();
        element.setUuid(individualEntrepreneurElement.getUuid());
        element.setCreateDate(individualEntrepreneurElement.getCreateDate());
        element.setType(SubjectType.INDIVIDUAL_ENTREPRENEUR);
        element.setStatus(((individualEntrepreneurElement.getStatus() != null) ? ((individualEntrepreneurElement.getStatus().getCode() != null) ? SubjectStatus.valueOf(individualEntrepreneurElement.getStatus().getCode()) : null) : null));

        if (individualEntrepreneurElement.getClient() != null) {
            Client client = new Client();
            client.setUuid(individualEntrepreneurElement.getClient().getUuid());
            element.setClient(client);
        } else {
            element.setClient(null);
        }

        element.setTitle(individualEntrepreneurElement.getTitle());
        element.setLegalCode(individualEntrepreneurElement.getLegalCode());

        if (individualEntrepreneurElement.getJuridicalAddress() != null) {
            JuridicalAddress juridicalAddress = new JuridicalAddress();
            juridicalAddress.setUuid(individualEntrepreneurElement.getJuridicalAddress().getUuid());
            element.setJuridicalAddress(juridicalAddress);
        } else {
            element.setJuridicalAddress(null);
        }

        return element;
    }

    public List<IndividualEntrepreneur> adapt(
            List<IndividualEntrepreneurElement> individualEntrepreneurElements
    ) {
        List<IndividualEntrepreneur> elements = new ArrayList<>();

        for (IndividualEntrepreneurElement element : individualEntrepreneurElements) {
            elements.add(adapt(element));
        }

        return elements;
    }
}
