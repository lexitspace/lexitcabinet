package ua.lexit.cabinet.api.resources.address.delivery.element;

import lombok.*;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class DeliveryAddressElement {

    private String uuid;
    private Instant createDate;
    private Type type;
    private Status status;
    private DeliveryClient client;
    private DeliveryService deliveryService;
    private DeliveryType deliveryType;
    private String recipient;
    private String phoneNumber;
    private String city;
    private StreetType streetType;
    private String street;
    private String house;
    private String corpus;
    private String entrance;
    private String floor;
    private FlatType flatType;
    private String flat;
    private String mailbox;
    private String zipCode;
    private String warehouse;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Type {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Status {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class DeliveryClient {
        private String uuid;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class DeliveryService {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class DeliveryType {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class StreetType  {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class FlatType {
        private String code;
        private String title;
    }
}
