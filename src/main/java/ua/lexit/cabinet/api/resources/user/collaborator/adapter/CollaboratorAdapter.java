package ua.lexit.cabinet.api.resources.user.collaborator.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.user.collaborator.element.CollaboratorElement;
import ua.lexit.cabinet.entities.users.Collaborator;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.UserType;

import java.util.ArrayList;
import java.util.List;

@Service
public class CollaboratorAdapter {

    public Collaborator adapt(
            CollaboratorElement collaboratorElement
    ) {

        Collaborator element = new Collaborator();
        element.setUuid(collaboratorElement.getUuid());
        element.setCreateDate(collaboratorElement.getCreateDate());
        element.setType(UserType.COLLABORATOR);
        element.setStatus(((collaboratorElement.getStatus() != null) ? ((collaboratorElement.getStatus().getCode() != null) ? UserStatus.valueOf(collaboratorElement.getStatus().getCode()) : null) : null));
        element.setLogin(collaboratorElement.getLogin());
        element.setEmail(collaboratorElement.getEmail());
        element.setPhone(collaboratorElement.getPhone());
        element.setPassword(null);
        element.setFirstName(collaboratorElement.getFirstName());
        element.setSecondName(collaboratorElement.getSecondName());
        element.setLastName(collaboratorElement.getLastName());
        element.setShortName(collaboratorElement.getShortName());
        element.setFullName(collaboratorElement.getFullName());

        return element;
    }

    public List<Collaborator> adapt(
            List<CollaboratorElement> collaboratorElements
    ) {
        List<Collaborator> elements = new ArrayList<>();

        for (CollaboratorElement element : collaboratorElements) {
            elements.add(adapt(element));
        }
        return elements;
    }
}
