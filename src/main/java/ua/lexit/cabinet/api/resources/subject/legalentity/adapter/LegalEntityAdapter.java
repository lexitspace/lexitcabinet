package ua.lexit.cabinet.api.resources.subject.legalentity.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.subject.legalentity.element.LegalEntityElement;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.entities.subject.LegalEntity;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.statuses.SubjectStatus;
import ua.lexit.cabinet.enums.types.SubjectType;

import java.util.ArrayList;
import java.util.List;

@Service
public class LegalEntityAdapter {

    public LegalEntity adapt(
            LegalEntityElement legalEntityElement
    ) {

        LegalEntity element = new LegalEntity();
        element.setUuid(legalEntityElement.getUuid());
        element.setCreateDate(legalEntityElement.getCreateDate());
        element.setType(SubjectType.LEGAL_ENTITY);
        element.setStatus(((legalEntityElement.getStatus() != null) ? ((legalEntityElement.getStatus().getCode() != null) ? SubjectStatus.valueOf(legalEntityElement.getStatus().getCode()) : null) : null));

        if (legalEntityElement.getClient() != null) {
            Client client = new Client();
            client.setUuid(legalEntityElement.getClient().getUuid());
            element.setClient(client);
        } else {
            element.setClient(null);
        }

        element.setTitle(legalEntityElement.getTitle());
        element.setLegalCode(legalEntityElement.getLegalCode());

        if (legalEntityElement.getJuridicalAddress() != null) {
            JuridicalAddress juridicalAddress = new JuridicalAddress();
            juridicalAddress.setUuid(legalEntityElement.getJuridicalAddress().getUuid());
            element.setJuridicalAddress(juridicalAddress);
        } else {
            element.setJuridicalAddress(null);
        }

        return element;
    }

    public List<LegalEntity> adapt(
            List<LegalEntityElement> legalEntityElements
    ) {

        List<LegalEntity> elements = new ArrayList<>();

        for (LegalEntityElement element : legalEntityElements) {
            elements.add(adapt(element));
        }

        return elements;
    }
}
