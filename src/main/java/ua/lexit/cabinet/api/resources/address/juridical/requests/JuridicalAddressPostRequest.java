package ua.lexit.cabinet.api.resources.address.juridical.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class JuridicalAddressPostRequest {

    private String title;
    private Integer limitation;
}
