package ua.lexit.cabinet.api.resources.subject.legalentity.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class LegalEntityPutRequest {

    private String title;
    private String legalCode;
}
