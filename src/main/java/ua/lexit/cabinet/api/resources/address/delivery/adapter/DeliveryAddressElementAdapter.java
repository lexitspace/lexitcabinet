package ua.lexit.cabinet.api.resources.address.delivery.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.address.delivery.element.DeliveryAddressElement;
import ua.lexit.cabinet.entities.address.DeliveryAddress;

import java.util.ArrayList;
import java.util.List;

@Service
public class DeliveryAddressElementAdapter {

    public DeliveryAddressElement adapt(
            DeliveryAddress deliveryAddress
    ) {

        return new DeliveryAddressElement(
                deliveryAddress.getUuid(),
                deliveryAddress.getCreateDate(),
                ((deliveryAddress.getType() != null) ? new DeliveryAddressElement.Type(
                        deliveryAddress.getType().name(),
                        deliveryAddress.getType().getTitle()
                ) : null),
                ((deliveryAddress.getStatus() != null) ? new DeliveryAddressElement.Status(
                        deliveryAddress.getStatus().name(),
                        deliveryAddress.getStatus().getTitle()
                ) : null),
                ((deliveryAddress.getClient() != null) ? new DeliveryAddressElement.DeliveryClient(
                        deliveryAddress.getClient().getUuid()
                ) : null),
                ((deliveryAddress.getDeliveryService() != null) ? new DeliveryAddressElement.DeliveryService(
                        deliveryAddress.getDeliveryService().name(),
                        deliveryAddress.getDeliveryService().getTitle()
                ) : null),
                ((deliveryAddress.getDeliveryType() != null) ? new DeliveryAddressElement.DeliveryType(
                        deliveryAddress.getDeliveryType().name(),
                        deliveryAddress.getDeliveryType().getTitle()
                ) : null),
                deliveryAddress.getRecipient(),
                deliveryAddress.getPhoneNumber(),
                deliveryAddress.getCity(),
                ((deliveryAddress.getStreetType() != null) ? new DeliveryAddressElement.StreetType(
                        deliveryAddress.getStreetType().name(),
                        deliveryAddress.getStreetType().getTitle()
                ) : null),
                deliveryAddress.getStreet(),
                deliveryAddress.getHouse(),
                deliveryAddress.getCorpus(),
                deliveryAddress.getEntrance(),
                deliveryAddress.getFloor(),
                ((deliveryAddress.getFlatType() != null) ? new DeliveryAddressElement.FlatType(
                        deliveryAddress.getFlatType().name(),
                        deliveryAddress.getFlatType().getTitle()
                ) : null),
                deliveryAddress.getFlat(),
                deliveryAddress.getMailbox(),
                deliveryAddress.getZipCode(),
                deliveryAddress.getWarehouse()
        );
    }

    public List<DeliveryAddressElement> adapt(
            List<DeliveryAddress> deliveryAddresses
    ) {
        List<DeliveryAddressElement> elements = new ArrayList<>();

        for (DeliveryAddress element : deliveryAddresses) {
            elements.add(adapt(element));
        }

        return elements;
    }
}
