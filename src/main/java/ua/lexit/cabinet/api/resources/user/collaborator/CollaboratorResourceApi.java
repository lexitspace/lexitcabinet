package ua.lexit.cabinet.api.resources.user.collaborator;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.lexit.cabinet.api.additional.request.ActionChangeStringRequest;
import ua.lexit.cabinet.api.additional.request.ActionChangeStringVerifiedRequest;
import ua.lexit.cabinet.api.additional.responses.ElementFailResponse;
import ua.lexit.cabinet.api.additional.responses.ElementResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.user.collaborator.adapter.CollaboratorAdapter;
import ua.lexit.cabinet.api.resources.user.collaborator.adapter.CollaboratorElementAdapter;
import ua.lexit.cabinet.api.resources.user.collaborator.requests.CollaboratorPatchRequest;
import ua.lexit.cabinet.api.resources.user.collaborator.requests.CollaboratorPostRequest;
import ua.lexit.cabinet.api.resources.user.collaborator.requests.CollaboratorPutRequest;
import ua.lexit.cabinet.api.resources.user.collaborator.responses.CollaboratorItemSuccessResponse;
import ua.lexit.cabinet.api.resources.user.collaborator.responses.CollaboratorListSuccessResponse;
import ua.lexit.cabinet.entities.users.Collaborator;
import ua.lexit.cabinet.enums.statuses.UserStatus;
import ua.lexit.cabinet.enums.types.UserType;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.services.user.CollaboratorService;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Collaborator api controllers
 */
@RestController
@RequestMapping("/api")
public class CollaboratorResourceApi {

    private final CollaboratorService collaboratorService;
    private final CollaboratorElementAdapter collaboratorElementAdapter;

    public CollaboratorResourceApi(
            CollaboratorService collaboratorService,
            CollaboratorElementAdapter collaboratorElementAdapter
    ) {
        this.collaboratorService = collaboratorService;
        this.collaboratorElementAdapter = collaboratorElementAdapter;
    }

    @GetMapping("/collaborators")
    public ResponseEntity<ElementResponse> getCollaborators(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestParam(value = "uuid", required = false) List<String> listUuid,
            @RequestParam(value = "status", required = false) List<String> listStatus,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            List<Collaborator> resultList = collaboratorService.getList(
                    listUuid,
                    listStatus,
                    limit,
                    offset
            );

            Integer size = resultList.size();
            Integer globalSize = collaboratorService.getListSize(
                    listUuid,
                    listStatus
            );
            limit = ((limit == null) ? size : ((limit == 0) ? 1 : limit));
            offset = ((offset == null) ? 0 : offset);
            Integer pages = ((limit != 0) ? ((globalSize % limit > 0) ? globalSize / limit + 1 : globalSize / limit) : 0);
            Integer currentPage = ((limit != 0) ? ((offset != 0 && offset % limit > 0) ? offset / limit : offset / limit + 1) : 0);

            elementResponse = new CollaboratorListSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            resultList
                    ),
                    new MetaResponse(
                            size,
                            globalSize,
                            pages,
                            currentPage
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @GetMapping("/collaborators/{identifier}")
    public ResponseEntity<ElementResponse> getCollaborator(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "identifier") String identifier
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            elementResponse = new CollaboratorItemSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            collaboratorService.get(identifier)
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PatchMapping("/collaborators/{uuid}")
    public ResponseEntity<ElementResponse> updateCollaboratorPatch(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody CollaboratorPatchRequest collaboratorPatchRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Collaborator collaborator = collaboratorService.get(uuid);

            collaborator.setFirstName(((collaboratorPatchRequest.getFirstName() == null) ? collaborator.getFirstName() : ((!collaboratorPatchRequest.getFirstName().equals("null")) ? collaboratorPatchRequest.getFirstName() : null)));
            collaborator.setSecondName(((collaboratorPatchRequest.getSecondName() == null) ? collaborator.getSecondName() : ((!collaboratorPatchRequest.getSecondName().equals("null")) ? collaboratorPatchRequest.getSecondName() : null)));
            collaborator.setLastName(((collaboratorPatchRequest.getLastName() == null) ? collaborator.getLastName() : ((!collaboratorPatchRequest.getLastName().equals("null")) ? collaboratorPatchRequest.getLastName() : null)));
            collaborator.setShortName(((collaboratorPatchRequest.getShortName() == null) ? collaborator.getShortName() : ((!collaboratorPatchRequest.getShortName().equals("null")) ? collaboratorPatchRequest.getShortName() : null)));
            collaborator.setFullName(((collaboratorPatchRequest.getFullName() == null) ? collaborator.getFullName() : ((!collaboratorPatchRequest.getFullName().equals("null")) ? collaboratorPatchRequest.getFullName() : null)));

            elementResponse = new CollaboratorItemSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            collaboratorService.update(
                                    collaborator
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PutMapping("/collaborators/{uuid}")
    public ResponseEntity<ElementResponse> updateCollaboratorPut(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody CollaboratorPutRequest collaboratorPutRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Collaborator collaborator = collaboratorService.get(uuid);

            collaborator.setFirstName(((!collaboratorPutRequest.getFirstName().equals("null")) ? collaboratorPutRequest.getFirstName() : null));
            collaborator.setSecondName(((!collaboratorPutRequest.getSecondName().equals("null")) ? collaboratorPutRequest.getSecondName() : null));
            collaborator.setLastName(((!collaboratorPutRequest.getLastName().equals("null")) ? collaboratorPutRequest.getLastName() : null));
            collaborator.setShortName(((!collaboratorPutRequest.getShortName().equals("null")) ? collaboratorPutRequest.getShortName() : null));
            collaborator.setFullName(((!collaboratorPutRequest.getFullName().equals("null")) ? collaboratorPutRequest.getFullName() : null));

            elementResponse = new CollaboratorItemSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            collaboratorService.update(
                                    collaborator
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/collaborators")
    public ResponseEntity<ElementResponse> createCollaborator(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestBody CollaboratorPostRequest collaboratorPostRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Collaborator collaborator = new Collaborator();

            collaborator.setCreateDate(ZonedDateTime.now(ZoneId.of("Europe/Kiev")).toInstant());
            collaborator.setType(UserType.COLLABORATOR);
            collaborator.setStatus(UserStatus.VERIFICATION);
            collaborator.setLogin(collaboratorPostRequest.getLogin());
            collaborator.setEmail(collaboratorPostRequest.getEmail());
            collaborator.setPhone(collaboratorPostRequest.getPhone());
            collaborator.setPassword(RandomStringUtils.randomNumeric(6));
            collaborator.setFirstName(collaboratorPostRequest.getFirstName());
            collaborator.setSecondName(collaboratorPostRequest.getSecondName());
            collaborator.setLastName(collaboratorPostRequest.getLastName());
            collaborator.setShortName(collaboratorPostRequest.getShortName());
            collaborator.setFullName(collaboratorPostRequest.getFullName());

            elementResponse = new CollaboratorItemSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            collaboratorService.update(
                                    collaborator
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {

            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "Client wasn't created."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @DeleteMapping("/collaborators/{uuid}")
    public ResponseEntity<ElementResponse> deleteCollaborator(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            collaboratorService.delete(
                    uuid
            );
            httpStatus = HttpStatus.OK;
        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "Collaborator wasn't delete."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    // Client REST Actions

    @PostMapping("/collaborators/{uuid}/change/status")
    public ResponseEntity<ElementResponse> createCollaboratorStatus(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            UserStatus.valueOf(actionChangeStringRequest.getValue());

            Collaborator collaborator = collaboratorService.get(uuid);

            collaborator.setStatus(UserStatus.valueOf(actionChangeStringRequest.getValue()));

            elementResponse = new CollaboratorItemSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            collaboratorService.update(
                                    collaborator
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/collaborators/{uuid}/change/login")
    public ResponseEntity<ElementResponse> createCollaboratorLogin(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Collaborator collaborator = collaboratorService.get(uuid);

            collaborator.setLogin(actionChangeStringRequest.getValue());

            elementResponse = new CollaboratorItemSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            collaboratorService.update(
                                    collaborator
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/collaborators/{uuid}/change/phone")
    public ResponseEntity<ElementResponse> createCollaboratorPhone(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Collaborator collaborator = collaboratorService.get(uuid);

            collaborator.setPhone(actionChangeStringRequest.getValue());

            elementResponse = new CollaboratorItemSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            collaboratorService.update(
                                    collaborator
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/collaborators/{uuid}/change/email")
    public ResponseEntity<ElementResponse> createCollaboratorEmail(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Collaborator collaborator = collaboratorService.get(uuid);

            collaborator.setEmail(actionChangeStringRequest.getValue());

            elementResponse = new CollaboratorItemSuccessResponse(
                    collaboratorElementAdapter.adapt(
                            collaboratorService.update(
                                    collaborator
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/collaborators/{uuid}/change/password")
    public ResponseEntity<ElementResponse> createCollaboratorPassword(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringVerifiedRequest actionChangeStringVerifiedRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            Collaborator collaborator = collaboratorService.get(uuid);

            if (collaborator.getPassword().equals(actionChangeStringVerifiedRequest.getOldValue())) {
                collaborator.setPassword(actionChangeStringVerifiedRequest.getNewValue());
                elementResponse = new CollaboratorItemSuccessResponse(
                        collaboratorElementAdapter.adapt(
                                collaboratorService.update(
                                        collaborator
                                )
                        ),
                        new StateResponse(
                                1,
                                "OK"
                        )
                );
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Charset", "UTF-8");
                httpStatus = HttpStatus.OK;
            } else {
                elementResponse = new ElementFailResponse(
                        new StateResponse(
                                21,
                                "Something bad with credentials."
                        )
                );
                httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                httpHeaders.add("Charset", "UTF-8");
                httpStatus = HttpStatus.BAD_REQUEST;
            }

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }
}
