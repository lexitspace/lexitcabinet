package ua.lexit.cabinet.api.resources.subject.individualentrepreneur.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.element.IndividualEntrepreneurElement;
import ua.lexit.cabinet.entities.subject.IndividualEntrepreneur;


import java.util.ArrayList;
import java.util.List;

@Service
public class IndividualEntrepreneurElementAdapter {

    public IndividualEntrepreneurElement adapt(
            IndividualEntrepreneur individualEntrepreneur
    ) {

        return new IndividualEntrepreneurElement(
                individualEntrepreneur.getUuid(),
                individualEntrepreneur.getCreateDate(),
                ((individualEntrepreneur.getType() != null) ? new IndividualEntrepreneurElement.Type(
                        individualEntrepreneur.getType().name(),
                        individualEntrepreneur.getType().getTitle()
                ) : null),
                ((individualEntrepreneur.getStatus() != null) ? new IndividualEntrepreneurElement.Status(
                        individualEntrepreneur.getStatus().name(),
                        individualEntrepreneur.getStatus().getTitle()
                ) : null),
                ((individualEntrepreneur.getClient() != null) ? new IndividualEntrepreneurElement.IEClient(
                        individualEntrepreneur.getClient().getUuid()
                ) : null),
                individualEntrepreneur.getTitle(),
                individualEntrepreneur.getLegalCode(),
                ((individualEntrepreneur.getJuridicalAddress() != null) ? new IndividualEntrepreneurElement.IEJuridicalAddress(
                        individualEntrepreneur.getJuridicalAddress().getUuid()
                ) : null)
        );
    }

    public List<IndividualEntrepreneurElement> adapt(
            List<IndividualEntrepreneur> deliveryAddresses
    ) {
        List<IndividualEntrepreneurElement> elements = new ArrayList<>();

        for (IndividualEntrepreneur element : deliveryAddresses) {
            elements.add(adapt(element));
        }

        return elements;
    }
}
