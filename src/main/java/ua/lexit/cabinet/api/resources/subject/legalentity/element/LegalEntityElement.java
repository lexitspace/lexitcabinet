package ua.lexit.cabinet.api.resources.subject.legalentity.element;

import lombok.*;

import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class LegalEntityElement {

    private String uuid;
    private Instant createDate;
    private Type type;
    private Status status;
    private LEClient client;
    private String title;
    private String legalCode;
    private LEJuridicalAddress juridicalAddress;

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Type {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Status {
        private String code;
        private String title;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class LEClient {
        private String uuid;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class LEJuridicalAddress {
        private String uuid;
    }
}
