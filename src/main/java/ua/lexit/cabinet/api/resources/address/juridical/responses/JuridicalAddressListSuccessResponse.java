package ua.lexit.cabinet.api.resources.address.juridical.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementListResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.address.juridical.element.JuridicalAddressElement;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class JuridicalAddressListSuccessResponse extends ElementListResponse {

    private List<JuridicalAddressElement> data;
    private MetaResponse meta;
    private StateResponse state;
}
