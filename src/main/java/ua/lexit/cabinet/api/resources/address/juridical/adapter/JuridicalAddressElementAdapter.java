package ua.lexit.cabinet.api.resources.address.juridical.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.address.juridical.element.JuridicalAddressElement;
import ua.lexit.cabinet.entities.address.JuridicalAddress;

import java.util.ArrayList;
import java.util.List;

@Service
public class JuridicalAddressElementAdapter {

    public JuridicalAddressElement adapt(
            JuridicalAddress juridicalAddress
    ) {

        return new JuridicalAddressElement(
                juridicalAddress.getUuid(),
                juridicalAddress.getCreateDate(),
                ((juridicalAddress.getType() != null) ? new JuridicalAddressElement.Type(
                        juridicalAddress.getType().name(),
                        juridicalAddress.getType().getTitle()
                ) : null),
                ((juridicalAddress.getStatus() != null) ? new JuridicalAddressElement.Status(
                        juridicalAddress.getStatus().name(),
                        juridicalAddress.getStatus().getTitle()
                ) : null),
                juridicalAddress.getTitle(),
                juridicalAddress.getLimitation()
        );
    }

    public List<JuridicalAddressElement> adapt(
            List<JuridicalAddress> juridicalAddresses
    ) {

        List<JuridicalAddressElement> elements = new ArrayList<>();

        for (JuridicalAddress element : juridicalAddresses) {
            elements.add(adapt(element));
        }
        return elements;
    }
}
