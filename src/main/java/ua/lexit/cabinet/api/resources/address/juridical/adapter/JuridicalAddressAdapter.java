package ua.lexit.cabinet.api.resources.address.juridical.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.address.juridical.element.JuridicalAddressElement;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.enums.statuses.AddressStatus;
import ua.lexit.cabinet.enums.types.AddressType;

import java.util.ArrayList;
import java.util.List;

@Service
public class JuridicalAddressAdapter {

    public JuridicalAddress adapt(
            JuridicalAddressElement juridicalAddressElement
    ) {
        JuridicalAddress element = new JuridicalAddress();

        element.setUuid(juridicalAddressElement.getUuid());
        element.setCreateDate(juridicalAddressElement.getCreateDate());
        element.setType(AddressType.JURIDICAL);
        element.setStatus(((juridicalAddressElement.getStatus() != null) ? ((juridicalAddressElement.getStatus().getCode() != null) ? AddressStatus.valueOf(juridicalAddressElement.getStatus().getCode()) : null) : null));
        element.setTitle(juridicalAddressElement.getTitle());
        element.setLimitation(juridicalAddressElement.getLimitation());

        return element;
    }

    public List<JuridicalAddress> adapt(
            List<JuridicalAddressElement> juridicalAddressElements
    ) {

        List<JuridicalAddress> elements = new ArrayList<>();

        for (JuridicalAddressElement element : juridicalAddressElements) {
            elements.add(adapt(element));
        }
        return elements;
    }
}
