package ua.lexit.cabinet.api.resources.subject.individualentrepreneur;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.lexit.cabinet.api.additional.request.ActionChangeStringRequest;
import ua.lexit.cabinet.api.additional.responses.ElementFailResponse;
import ua.lexit.cabinet.api.additional.responses.ElementResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.adapter.IndividualEntrepreneurElementAdapter;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.requests.IndividualEntrepreneurPatchRequest;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.requests.IndividualEntrepreneurPostRequest;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.requests.IndividualEntrepreneurPutRequest;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.responses.IndividualEntrepreneurItemSuccessResponse;
import ua.lexit.cabinet.api.resources.subject.individualentrepreneur.responses.IndividualEntrepreneurListSuccessResponse;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.entities.subject.IndividualEntrepreneur;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.statuses.SubjectStatus;
import ua.lexit.cabinet.enums.types.SubjectType;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.services.address.JuridicalAddressService;
import ua.lexit.cabinet.services.subject.IndividualEntrepreneurService;
import ua.lexit.cabinet.services.user.ClientService;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * IndividualEntrepreneur api controllers
 */
@RestController
@RequestMapping("/api")
public class IndividualEntrepreneurResourceApi {

    private final IndividualEntrepreneurService individualEntrepreneurService;
    private final IndividualEntrepreneurElementAdapter individualEntrepreneurElementAdapter;
    private final ClientService clientService;
    private final JuridicalAddressService juridicalAddressService;

    public IndividualEntrepreneurResourceApi(
            IndividualEntrepreneurService individualEntrepreneurService,
            IndividualEntrepreneurElementAdapter individualEntrepreneurElementAdapter,
            ClientService clientService,
            JuridicalAddressService juridicalAddressService
    ) {
        this.individualEntrepreneurService = individualEntrepreneurService;
        this.individualEntrepreneurElementAdapter = individualEntrepreneurElementAdapter;
        this.clientService = clientService;
        this.juridicalAddressService = juridicalAddressService;
    }

    @GetMapping("/individualEntrepreneurs")
    public ResponseEntity<ElementResponse> getIndividualEntrepreneurs(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestParam(value = "uuid", required = false) List<String> listUuid,
            @RequestParam(value = "status", required = false) List<String> listStatus,
            @RequestParam(value = "user", required = false) List<String> listUser,
            @RequestParam(value = "address", required = false) List<String> listAddress,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            List<IndividualEntrepreneur> resultList = individualEntrepreneurService.getList(
                    listUuid,
                    listStatus,
                    listUser,
                    listAddress,
                    limit,
                    offset
            );

            Integer size = resultList.size();
            Integer globalSize = individualEntrepreneurService.getListSize(
                    listUuid,
                    listStatus,
                    listUser,
                    listAddress
            );
            limit = ((limit == null) ? size : ((limit == 0) ? 1 : limit));
            offset = ((offset == null) ? 0 : offset);
            Integer pages = ((limit != 0) ? ((globalSize % limit > 0) ? globalSize / limit + 1 : globalSize / limit) : 0);
            Integer currentPage = ((limit != 0) ? ((offset != 0 && offset % limit > 0) ? offset / limit : offset / limit + 1) : 0);

            elementResponse = new IndividualEntrepreneurListSuccessResponse(
                    individualEntrepreneurElementAdapter.adapt(
                            resultList
                    ),
                    new MetaResponse(
                            size,
                            globalSize,
                            pages,
                            currentPage
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @GetMapping("/individualEntrepreneurs/{identifier}")
    public ResponseEntity<ElementResponse> getIndividualEntrepreneur(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "identifier") String identifier
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            elementResponse = new IndividualEntrepreneurItemSuccessResponse(
                    individualEntrepreneurElementAdapter.adapt(
                            individualEntrepreneurService.get(identifier)
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PatchMapping("/individualEntrepreneurs/{uuid}")
    public ResponseEntity<ElementResponse> updateIndividualEntrepreneurPatch(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody IndividualEntrepreneurPatchRequest individualEntrepreneurPatchRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            IndividualEntrepreneur entrepreneur = individualEntrepreneurService.get(uuid);

            entrepreneur.setTitle(((individualEntrepreneurPatchRequest.getTitle() == null) ? entrepreneur.getTitle() : ((!individualEntrepreneurPatchRequest.getTitle().equals("null")) ? individualEntrepreneurPatchRequest.getTitle() : null)));
            entrepreneur.setLegalCode(((individualEntrepreneurPatchRequest.getLegalCode() == null) ? entrepreneur.getLegalCode() : ((!individualEntrepreneurPatchRequest.getLegalCode().equals("null")) ? individualEntrepreneurPatchRequest.getLegalCode() : null)));

            elementResponse = new IndividualEntrepreneurItemSuccessResponse(
                    individualEntrepreneurElementAdapter.adapt(
                            individualEntrepreneurService.update(
                                    entrepreneur
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PutMapping("/individualEntrepreneurs/{uuid}")
    public ResponseEntity<ElementResponse> updateIndividualEntrepreneurPut(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody IndividualEntrepreneurPutRequest individualEntrepreneurPutRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            IndividualEntrepreneur entrepreneur = individualEntrepreneurService.get(uuid);

            entrepreneur.setTitle(((individualEntrepreneurPutRequest.getTitle() == null) ? entrepreneur.getTitle() : ((!individualEntrepreneurPutRequest.getTitle().equals("null")) ? individualEntrepreneurPutRequest.getTitle() : null)));
            entrepreneur.setLegalCode(((individualEntrepreneurPutRequest.getLegalCode() == null) ? entrepreneur.getLegalCode() : ((!individualEntrepreneurPutRequest.getLegalCode().equals("null")) ? individualEntrepreneurPutRequest.getLegalCode() : null)));

            elementResponse = new IndividualEntrepreneurItemSuccessResponse(
                    individualEntrepreneurElementAdapter.adapt(
                            individualEntrepreneurService.update(
                                    entrepreneur
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/individualEntrepreneurs")
    public ResponseEntity<ElementResponse> createIndividualEntrepreneur(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestBody IndividualEntrepreneurPostRequest individualEntrepreneurPostRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            IndividualEntrepreneur entrepreneur = new IndividualEntrepreneur();

            entrepreneur.setCreateDate(ZonedDateTime.now(ZoneId.of("Europe/Kiev")).toInstant());
            entrepreneur.setType(SubjectType.INDIVIDUAL_ENTREPRENEUR);
            entrepreneur.setStatus(SubjectStatus.CREATING);

            if (individualEntrepreneurPostRequest.getClient() != null) {
                Client client = new Client();
                client.setUuid(individualEntrepreneurPostRequest.getClient().getUuid());
                entrepreneur.setClient(client);
            } else {
                entrepreneur.setClient(null);
            }

            entrepreneur.setTitle(individualEntrepreneurPostRequest.getTitle());
            entrepreneur.setTitle(individualEntrepreneurPostRequest.getLegalCode());

            if (individualEntrepreneurPostRequest.getJuridicalAddress() != null) {
                JuridicalAddress juridicalAddress = new JuridicalAddress();
                juridicalAddress.setUuid(individualEntrepreneurPostRequest.getJuridicalAddress().getUuid());
                entrepreneur.setJuridicalAddress(juridicalAddress);
            } else {
                entrepreneur.setJuridicalAddress(null);
            }

            elementResponse = new IndividualEntrepreneurItemSuccessResponse(
                    individualEntrepreneurElementAdapter.adapt(
                            individualEntrepreneurService.update(
                                    entrepreneur
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {

            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "IndividualEntrepreneur wasn't created."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @DeleteMapping("/individualEntrepreneurs/{uuid}")
    public ResponseEntity<ElementResponse> deleteIndividualEntrepreneur(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            individualEntrepreneurService.delete(
                    uuid
            );
            httpStatus = HttpStatus.OK;
        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "IndividualEntrepreneur wasn't delete."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    // IndividualEntrepreneur REST Actions

    @PostMapping("/individualEntrepreneurs/{uuid}/change/status")
    public ResponseEntity<ElementResponse> createIndividualEntrepreneurStatus(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            SubjectStatus.valueOf(actionChangeStringRequest.getValue());

            IndividualEntrepreneur entrepreneur = individualEntrepreneurService.get(uuid);

            entrepreneur.setStatus(SubjectStatus.valueOf(actionChangeStringRequest.getValue()));

            elementResponse = new IndividualEntrepreneurItemSuccessResponse(
                    individualEntrepreneurElementAdapter.adapt(
                            individualEntrepreneurService.update(
                                    entrepreneur
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }


    @PostMapping("/individualEntrepreneurs/{uuid}/change/client")
    public ResponseEntity<ElementResponse> createIndividualEntrepreneurClient(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            IndividualEntrepreneur entrepreneur = individualEntrepreneurService.get(uuid);
            Client client = clientService.get(actionChangeStringRequest.getValue());

            entrepreneur.setClient(client);

            elementResponse = new IndividualEntrepreneurItemSuccessResponse(
                    individualEntrepreneurElementAdapter.adapt(
                            individualEntrepreneurService.update(
                                    entrepreneur
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/individualEntrepreneurs/{uuid}/change/juridicalAddress")
    public ResponseEntity<ElementResponse> createIndividualEntrepreneurJuridicalAddress(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            IndividualEntrepreneur entrepreneur = individualEntrepreneurService.get(uuid);
            JuridicalAddress juridicalAddress = juridicalAddressService.get(actionChangeStringRequest.getValue());

            entrepreneur.setJuridicalAddress(juridicalAddress   );

            elementResponse = new IndividualEntrepreneurItemSuccessResponse(
                    individualEntrepreneurElementAdapter.adapt(
                            individualEntrepreneurService.update(
                                    entrepreneur
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }
}