package ua.lexit.cabinet.api.resources.user.collaborator.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.user.collaborator.element.CollaboratorElement;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CollaboratorItemSuccessResponse extends ElementResponse {

    private CollaboratorElement data;
    private StateResponse state;
}
