package ua.lexit.cabinet.api.resources.subject.individualentrepreneur.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class IndividualEntrepreneurPostRequest {

    private Status status;
    private IEClient client;
    private String title;
    private String legalCode;
    private IEJuridicalAddress juridicalAddress;

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class Status {
        private String status;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class IEClient {
        private String uuid;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class IEJuridicalAddress {
        private String uuid;
    }
}
