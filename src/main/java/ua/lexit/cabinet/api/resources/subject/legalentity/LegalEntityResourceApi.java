package ua.lexit.cabinet.api.resources.subject.legalentity;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.lexit.cabinet.api.additional.request.ActionChangeStringRequest;
import ua.lexit.cabinet.api.additional.responses.ElementFailResponse;
import ua.lexit.cabinet.api.additional.responses.ElementResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.subject.legalentity.adapter.LegalEntityElementAdapter;
import ua.lexit.cabinet.api.resources.subject.legalentity.requests.LegalEntityPatchRequest;
import ua.lexit.cabinet.api.resources.subject.legalentity.requests.LegalEntityPostRequest;
import ua.lexit.cabinet.api.resources.subject.legalentity.requests.LegalEntityPutRequest;
import ua.lexit.cabinet.api.resources.subject.legalentity.responses.LegalEntityItemSuccessResponse;
import ua.lexit.cabinet.api.resources.subject.legalentity.responses.LegalEntityListSuccessResponse;
import ua.lexit.cabinet.entities.address.JuridicalAddress;
import ua.lexit.cabinet.entities.subject.LegalEntity;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.statuses.SubjectStatus;
import ua.lexit.cabinet.enums.types.SubjectType;
import ua.lexit.cabinet.exceptions.RepositoryException;
import ua.lexit.cabinet.services.address.JuridicalAddressService;
import ua.lexit.cabinet.services.subject.LegalEntityService;
import ua.lexit.cabinet.services.user.ClientService;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * LegalEntity api controllers
 */
@RestController
@RequestMapping("/api")
public class LegalEntityResourceApi {

    private final LegalEntityService legalEntityService;
    private final LegalEntityElementAdapter legalEntityElementAdapter;
    private final ClientService clientService;
    private final JuridicalAddressService juridicalAddressService;

    public LegalEntityResourceApi(
            LegalEntityService legalEntityService,
            LegalEntityElementAdapter legalEntityElementAdapter,
            ClientService clientService,
            JuridicalAddressService juridicalAddressService
    ) {
        this.legalEntityService = legalEntityService;
        this.legalEntityElementAdapter = legalEntityElementAdapter;
        this.clientService = clientService;
        this.juridicalAddressService = juridicalAddressService;
    }

    @GetMapping("/legalEntities")
    public ResponseEntity<ElementResponse> getLegalEntities(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestParam(value = "uuid", required = false) List<String> listUuid,
            @RequestParam(value = "status", required = false) List<String> listStatus,
            @RequestParam(value = "user", required = false) List<String> listUser,
            @RequestParam(value = "address", required = false) List<String> listAddress,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "offset", required = false) Integer offset
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            List<LegalEntity> resultList = legalEntityService.getList(
                    listUuid,
                    listStatus,
                    listUser,
                    listAddress,
                    limit,
                    offset
            );

            Integer size = resultList.size();
            Integer globalSize = legalEntityService.getListSize(
                    listUuid,
                    listStatus,
                    listUser,
                    listAddress
            );
            limit = ((limit == null) ? size : ((limit == 0) ? 1 : limit));
            offset = ((offset == null) ? 0 : offset);
            Integer pages = ((limit != 0) ? ((globalSize % limit > 0) ? globalSize / limit + 1 : globalSize / limit) : 0);
            Integer currentPage = ((limit != 0) ? ((offset != 0 && offset % limit > 0) ? offset / limit : offset / limit + 1) : 0);

            elementResponse = new LegalEntityListSuccessResponse(
                    legalEntityElementAdapter.adapt(
                            resultList
                    ),
                    new MetaResponse(
                            size,
                            globalSize,
                            pages,
                            currentPage
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @GetMapping("/legalEntities/{identifier}")
    public ResponseEntity<ElementResponse> getLegalEntity(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "identifier") String identifier
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            elementResponse = new LegalEntityItemSuccessResponse(
                    legalEntityElementAdapter.adapt(
                            legalEntityService.get(identifier)
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PatchMapping("/legalEntities/{uuid}")
    public ResponseEntity<ElementResponse> updateLegalEntityPatch(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody LegalEntityPatchRequest legalEntityPatchRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            LegalEntity legalEntity = legalEntityService.get(uuid);

            legalEntity.setTitle(((legalEntityPatchRequest.getTitle() == null) ? legalEntity.getTitle() : ((!legalEntityPatchRequest.getTitle().equals("null")) ? legalEntityPatchRequest.getTitle() : null)));
            legalEntity.setLegalCode(((legalEntityPatchRequest.getLegalCode() == null) ? legalEntity.getLegalCode() : ((!legalEntityPatchRequest.getLegalCode().equals("null")) ? legalEntityPatchRequest.getLegalCode() : null)));

            elementResponse = new LegalEntityItemSuccessResponse(
                    legalEntityElementAdapter.adapt(
                            legalEntityService.update(
                                    legalEntity
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PutMapping("/legalEntities/{uuid}")
    public ResponseEntity<ElementResponse> updateLegalEntitiesPut(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody LegalEntityPutRequest legalEntityPutRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            LegalEntity legalEntity = legalEntityService.get(uuid);

            legalEntity.setTitle(((legalEntityPutRequest.getTitle() == null) ? legalEntity.getTitle() : ((!legalEntityPutRequest.getTitle().equals("null")) ? legalEntityPutRequest.getTitle() : null)));
            legalEntity.setLegalCode(((legalEntityPutRequest.getLegalCode() == null) ? legalEntity.getLegalCode() : ((!legalEntityPutRequest.getLegalCode().equals("null")) ? legalEntityPutRequest.getLegalCode() : null)));

            elementResponse = new LegalEntityItemSuccessResponse(
                    legalEntityElementAdapter.adapt(
                            legalEntityService.update(
                                    legalEntity
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/legalEntities")
    public ResponseEntity<ElementResponse> createLegalEntity(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @RequestBody LegalEntityPostRequest legalEntityPostRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            LegalEntity legalEntity = new LegalEntity();

            legalEntity.setCreateDate(ZonedDateTime.now(ZoneId.of("Europe/Kiev")).toInstant());
            legalEntity.setType(SubjectType.LEGAL_ENTITY);
            legalEntity.setStatus(SubjectStatus.CREATING);

            if (legalEntityPostRequest.getClient() != null) {
                Client client = new Client();
                client.setUuid(legalEntityPostRequest.getClient().getUuid());
                legalEntity.setClient(client);
            } else {
                legalEntity.setClient(null);
            }

            legalEntity.setTitle(legalEntityPostRequest.getTitle());
            legalEntity.setTitle(legalEntityPostRequest.getLegalCode());

            if (legalEntityPostRequest.getJuridicalAddress() != null) {
                JuridicalAddress juridicalAddress = new JuridicalAddress();
                juridicalAddress.setUuid(legalEntityPostRequest.getJuridicalAddress().getUuid());
                legalEntity.setJuridicalAddress(juridicalAddress);
            } else {
                legalEntity.setJuridicalAddress(null);
            }

            elementResponse = new LegalEntityItemSuccessResponse(
                    legalEntityElementAdapter.adapt(
                            legalEntityService.update(
                                    legalEntity
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {

            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "LegalEntity wasn't created."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @DeleteMapping("/legalEntities/{uuid}")
    public ResponseEntity<ElementResponse> deleteLegalEntity(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            legalEntityService.delete(
                    uuid
            );
            httpStatus = HttpStatus.OK;
        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            "LegalEntity wasn't delete."
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    // IndividualEntrepreneur REST Actions

    @PostMapping("/legalEntities/{uuid}/change/status")
    public ResponseEntity<ElementResponse> createLegalEntityStatus(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            SubjectStatus.valueOf(actionChangeStringRequest.getValue());

            LegalEntity legalEntity = legalEntityService.get(uuid);

            legalEntity.setStatus(SubjectStatus.valueOf(actionChangeStringRequest.getValue()));

            elementResponse = new LegalEntityItemSuccessResponse(
                    legalEntityElementAdapter.adapt(
                            legalEntityService.update(
                                    legalEntity
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/legalEntities/{uuid}/change/client")
    public ResponseEntity<ElementResponse> createLegalEntityClient(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            LegalEntity legalEntity = legalEntityService.get(uuid);
            Client client = clientService.get(actionChangeStringRequest.getValue());

            legalEntity.setClient(client);

            elementResponse = new LegalEntityItemSuccessResponse(
                    legalEntityElementAdapter.adapt(
                            legalEntityService.update(
                                    legalEntity
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }

    @PostMapping("/legalEntities/{uuid}/change/juridicalAddress")
    public ResponseEntity<ElementResponse> createLegalEntityAddress(
            @RequestHeader(name = "Auth-Token", required = false) String AuthTokenHeader,
            @RequestParam(name = "Auth-Token", required = false) String AuthTokenParam,
            @PathVariable(value = "uuid") String uuid,
            @RequestBody ActionChangeStringRequest actionChangeStringRequest
    ) {
        ElementResponse elementResponse = null;
        HttpHeaders httpHeaders = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.I_AM_A_TEAPOT;

        try {
            LegalEntity legalEntity = legalEntityService.get(uuid);
            JuridicalAddress juridicalAddress = juridicalAddressService.get(actionChangeStringRequest.getValue());

            legalEntity.setJuridicalAddress(juridicalAddress);

            elementResponse = new LegalEntityItemSuccessResponse(
                    legalEntityElementAdapter.adapt(
                            legalEntityService.update(
                                    legalEntity
                            )
                    ),
                    new StateResponse(
                            1,
                            "OK"
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.OK;

        } catch (RepositoryException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            42,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        } catch (IllegalArgumentException e) {
            elementResponse = new ElementFailResponse(
                    new StateResponse(
                            66,
                            e.getLocalizedMessage()
                    )
            );
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.add("Charset", "UTF-8");
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(elementResponse, httpHeaders, httpStatus);
    }
}
