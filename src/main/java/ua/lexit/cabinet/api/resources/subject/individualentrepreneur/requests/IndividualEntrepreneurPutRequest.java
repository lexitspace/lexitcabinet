package ua.lexit.cabinet.api.resources.subject.individualentrepreneur.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class IndividualEntrepreneurPutRequest {

    private String title;
    private String legalCode;
}
