package ua.lexit.cabinet.api.resources.address.juridical.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementItemResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.address.juridical.element.JuridicalAddressElement;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class JuridicalAddressItemSuccessResponse extends ElementItemResponse {

    private JuridicalAddressElement data;
    private StateResponse state;
}
