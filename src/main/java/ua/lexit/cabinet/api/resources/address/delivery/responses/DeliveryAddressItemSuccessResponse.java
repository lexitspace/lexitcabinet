package ua.lexit.cabinet.api.resources.address.delivery.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementItemResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.address.delivery.element.DeliveryAddressElement;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class DeliveryAddressItemSuccessResponse extends ElementItemResponse {

    private DeliveryAddressElement data;
    private StateResponse state;
}
