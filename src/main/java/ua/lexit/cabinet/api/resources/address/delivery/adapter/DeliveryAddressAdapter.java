package ua.lexit.cabinet.api.resources.address.delivery.adapter;

import org.springframework.stereotype.Service;
import ua.lexit.cabinet.api.resources.address.delivery.element.DeliveryAddressElement;
import ua.lexit.cabinet.entities.address.DeliveryAddress;
import ua.lexit.cabinet.entities.users.Client;
import ua.lexit.cabinet.enums.address.GISFlatType;
import ua.lexit.cabinet.enums.address.GISStreetType;
import ua.lexit.cabinet.enums.address.delivery.AddressDeliveryService;
import ua.lexit.cabinet.enums.address.delivery.AddressDeliveryType;
import ua.lexit.cabinet.enums.statuses.AddressStatus;
import ua.lexit.cabinet.enums.types.AddressType;


import java.util.ArrayList;
import java.util.List;

@Service

public class DeliveryAddressAdapter {

    public DeliveryAddress adapt(
            DeliveryAddressElement deliveryAddressElement
    ) {
        DeliveryAddress element = new DeliveryAddress();

        element.setUuid(deliveryAddressElement.getUuid());
        element.setCreateDate(deliveryAddressElement.getCreateDate());
        element.setType(AddressType.DELIVERY);
        element.setStatus(((deliveryAddressElement.getStatus() != null) ? ((deliveryAddressElement.getStatus().getCode() != null) ? AddressStatus.valueOf(deliveryAddressElement.getStatus().getCode()) : null) : null));

        if (deliveryAddressElement.getClient() != null) {
            Client client = new Client();
            client.setUuid(deliveryAddressElement.getClient().getUuid());
            element.setClient(client);
        } else {
            element.setClient(null);
        }

        element.setDeliveryService(((deliveryAddressElement.getDeliveryService() != null) ? ((deliveryAddressElement.getDeliveryService().getCode() != null) ? AddressDeliveryService.valueOf(deliveryAddressElement.getDeliveryService().getCode()) : null) : null));
        element.setDeliveryType(((deliveryAddressElement.getDeliveryType() != null) ? ((deliveryAddressElement.getDeliveryType().getCode() != null) ? AddressDeliveryType.valueOf(deliveryAddressElement.getDeliveryType().getCode()) : null) : null));
        element.setRecipient(deliveryAddressElement.getRecipient());
        element.setPhoneNumber(deliveryAddressElement.getPhoneNumber());
        element.setCity(deliveryAddressElement.getCity());
        element.setStreetType(((deliveryAddressElement.getStreetType() != null) ? ((deliveryAddressElement.getStreetType().getCode() != null) ? GISStreetType.valueOf(deliveryAddressElement.getStreetType().getCode()) : null) : null));
        element.setStreet(deliveryAddressElement.getStreet());
        element.setHouse(deliveryAddressElement.getHouse());
        element.setCorpus(deliveryAddressElement.getCorpus());
        element.setEntrance(deliveryAddressElement.getEntrance());
        element.setFloor(deliveryAddressElement.getFloor());
        element.setFlatType(((deliveryAddressElement.getFlatType() != null) ? ((deliveryAddressElement.getFlatType().getCode() != null) ? GISFlatType.valueOf(deliveryAddressElement.getFlatType().getCode()) : null) : null));
        element.setFlat(deliveryAddressElement.getFlat());
        element.setMailbox(deliveryAddressElement.getMailbox());
        element.setZipCode(deliveryAddressElement.getZipCode());
        element.setWarehouse(deliveryAddressElement.getWarehouse());

        return element;
    }

    public List<DeliveryAddress> adapt(
            List<DeliveryAddressElement> deliveryAddressElements
    ) {
        List<DeliveryAddress> elements = new ArrayList<>();

        for (DeliveryAddressElement element : deliveryAddressElements) {
            elements.add(adapt(element));
        }
        return elements;
    }
}

