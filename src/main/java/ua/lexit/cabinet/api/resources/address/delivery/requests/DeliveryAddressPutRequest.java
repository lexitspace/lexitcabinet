package ua.lexit.cabinet.api.resources.address.delivery.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class DeliveryAddressPutRequest {

    private DeliveryService deliveryService;
    private DeliveryType deliveryType;
    private String recipient;
    private String phoneNumber;
    private String city;
    private StreetType streetType;
    private String street;
    private String house;
    private String corpus;
    private String entrance;
    private String floor;
    private FlatType flatType;
    private String flat;
    private String mailbox;
    private String zipCode;
    private String warehouse;

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class DeliveryClient {
        private String uuid;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class DeliveryService {
        private String code;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class DeliveryType {
        private String code;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class StreetType {
        private String code;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class FlatType {
        private String code;
    }
}
