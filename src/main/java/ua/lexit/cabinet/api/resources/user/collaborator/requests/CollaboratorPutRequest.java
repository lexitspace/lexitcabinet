package ua.lexit.cabinet.api.resources.user.collaborator.requests;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Getter
@Setter
public class CollaboratorPutRequest {

    private String firstName;
    private String secondName;
    private String lastName;
    private String shortName;
    private String fullName;
}
