package ua.lexit.cabinet.api.resources.user.collaborator.responses;

import lombok.*;
import ua.lexit.cabinet.api.additional.responses.ElementResponse;
import ua.lexit.cabinet.api.additional.responses.MetaResponse;
import ua.lexit.cabinet.api.additional.responses.StateResponse;
import ua.lexit.cabinet.api.resources.user.collaborator.element.CollaboratorElement;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CollaboratorListSuccessResponse extends ElementResponse {

    private List<CollaboratorElement> data;
    private MetaResponse meta;
    private StateResponse state;
}
