/*
 * Debug
 */
let debugMode = false;

function enableDebugMode() {
    debugMode = true;
}

function disableDebugMode() {
    debugMode = false;
}

function debug(str) {
    if (debugMode) {
        alert(str);
    }
}

let POST = 'POST';
let GET = 'GET';
let PUT = 'PUT';
let DELETE = 'DELETE';

/*
 * Api core
 */
function addQueryParam(name, values, query) {
    if (typeof values === 'object') {
        if (Array.isArray(values)) {
            for (let i = 0; i < values.length; i++) {
                query = query + name + '=' + values[i] + '&';
            }
        }
    } else if (typeof values === 'number') {
        query = query + name + '=' + values + '&';
    } else if (typeof values === 'string') {
        query = query + name + '=' + values + '&';
    }

    return query;
}

/*
 * Api implementation
 */
function getUsersStatuses(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/users/statuses';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getUserStatus(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/users/statuses/' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getUsersTypes(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/users/types';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getUserType(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/users/types' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}


function getSubjectsStatuses(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/subjects/statuses';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getSubjectStatus(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/subjects/statuses' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getSubjectsTypes(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/subjects/types';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getSubjectType(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/subjects/types' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}


function getPaymentsStatuses(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/payments/statuses';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getPaymentStatus(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/payments/statuses' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getPaymentsTypes(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/payments/types';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getPaymentType(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/payments/types' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}


function getMessagesStatuses(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/messages/statuses';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getMessageStatus(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/messages/statuses' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getMessagesTypes(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/messages/types';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getMessageType(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/messages/types' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}


function getLettersStatuses(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/letters/statuses';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getLetterStatus(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/letters/statuses' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getLettersTypes(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/letters/types';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getLetterType(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/letters/types' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}


function getContractsStatuses(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/contracts/statuses';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getContractStatus(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/contracts/statuses' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getContractsTypes(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/contracts/types';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getContractType(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/contracts/types' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}


function getAttachmentsTypes(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/attachments/types';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getAttachmentType(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/attachments/types' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}


function getAddressesStatuses(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/addresses/statuses';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getAddressStatus(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/addresses/statuses' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getAddressesTypes(actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/addresses/types';
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getAddressType(identifier, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/addresses/types' + identifier;
    let params = '';

    http.open("GET", url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}




function getUsersItem(uuid, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/users/'+uuid;
    let params = '';

    http.open(GET, url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getUsersList(uuid, typeUuid, statusUuid, orderBy, limit, offset, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/users';
    let params = '';

    params = addQueryParam('uuid', uuid, params);
    params = addQueryParam('type', type, params);
    params = addQueryParam('status', status, params);
    params = addQueryParam('orderBy', orderBy, params);
    params = addQueryParam('limit', limit, params);
    params = addQueryParam('offset', offset, params);

    http.open(GET, url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function putUsers(uuid, typeUuid, statusUuid, login, password, email, phone, firstName, secondName, lastName, shortName, fullName, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/users';

    http.open("PUT", url, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);
    let requestBody = {};
    requestBody.data = {};

    if (typeof uuid !== 'undefined') {
        if (typeof uuid === 'string') {
            requestBody.data.uuid = uuid;
        }
    }

    if (typeof typeUuid !== 'undefined') {

        requestBody.data.type = {};

        if (typeof typeUuid === 'string') {
            if (typeUuid !== ''){
                requestBody.data.type.uuid = typeUuid;
            } else {
                requestBody.data.type.uuid = 'null';
            }
        } else if (typeof typeUuid === 'object') {
            requestBody.data.type.uuid = 'null';
        }
    }

    if (typeof statusUuid !== 'undefined') {

        requestBody.data.status = {};

        if (typeof statusUuid === 'string') {
            if (statusUuid !== ''){
                requestBody.data.status.uuid = statusUuid;
            } else {
                requestBody.data.status.uuid = 'null';
            }
        } else if (typeof statusUuid === 'object') {
            requestBody.data.status.uuid = 'null';
        }
    }

    if (typeof login !== 'undefined') {

        if (typeof login === 'string') {
            if (login !== ''){
                requestBody.data.login = login;
            } else {
                requestBody.data.login = 'null';
            }
        } else if (typeof login === 'object') {
            requestBody.data.login = 'null';
        }
    }

    if (typeof password !== 'undefined') {

        if (typeof password === 'string') {
            if (password !== ''){
                requestBody.data.password = password;
            } else {
                requestBody.data.password = 'null';
            }
        } else if (typeof password === 'object') {
            requestBody.data.password = 'null';
        }
    }

    if (typeof email !== 'undefined') {

        if (typeof email === 'string') {
            if (email !== ''){
                requestBody.data.email = email;
            } else {
                requestBody.data.email = 'null';
            }
        } else if (typeof email === 'object') {
            requestBody.data.email = 'null';
        }
    }

    if (typeof phone !== 'undefined') {

        if (typeof phone === 'string') {
            if (phone !== ''){
                requestBody.data.phone = phone;
            } else {
                requestBody.data.phone = 'null';
            }
        } else if (typeof phone === 'object') {
            requestBody.data.phone = 'null';
        }
    }

    if (typeof firstName !== 'undefined') {

        if (typeof firstName === 'string') {
            if (firstName !== ''){
                requestBody.data.firstName = firstName;
            } else {
                requestBody.data.firstName = 'null';
            }
        } else if (typeof firstName === 'object') {
            requestBody.data.firstName = 'null';
        }
    }

    if (typeof secondName !== 'undefined') {

        if (typeof secondName === 'string') {
            if (secondName !== ''){
                requestBody.data.secondName = secondName;
            } else {
                requestBody.data.secondName = 'null';
            }
        } else if (typeof secondName === 'object') {
            requestBody.data.secondName = 'null';
        }
    }

    if (typeof lastName !== 'undefined') {

        if (typeof lastName === 'string') {
            if (lastName !== ''){
                requestBody.data.lastName = lastName;
            } else {
                requestBody.data.lastName = 'null';
            }
        } else if (typeof lastName === 'object') {
            requestBody.data.lastName = 'null';
        }
    }

    if (typeof shortName !== 'undefined') {

        if (typeof shortName === 'string') {
            if (shortName !== ''){
                requestBody.data.shortName = shortName;
            } else {
                requestBody.data.shortName = 'null';
            }
        } else if (typeof shortName === 'object') {
            requestBody.data.shortName = 'null';
        }
    }

    if (typeof fullName !== 'undefined') {

        if (typeof fullName === 'string') {
            if (fullName !== ''){
                requestBody.data.fullName = fullName;
            } else {
                requestBody.data.fullName = 'null';
            }
        } else if (typeof fullName === 'object') {
            requestBody.data.fullName = 'null';
        }
    }

    debug('Method : ' + PUT + '\n' +
        'URL : ' + url + '\n' +
        'Body : ' + JSON.stringify(requestBody));

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send(JSON.stringify(requestBody));
}


function getAddressDeliveryItem(uuid, actionSuccess, actionError){
    let http = new XMLHttpRequest();
    let url = '/api/addresses/delivery/'+uuid;
    let params = '';

    http.open(GET, url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getAddressDeliveryList(statusUuid, orderBy, limit, offset, actionSuccess, actionError){
    let http = new XMLHttpRequest();
    let url = '/api/addresses/delivery';
    let params = '';

    params = addQueryParam('status', statusUuid, params);
    params = addQueryParam('orderBy', orderBy, params);
    params = addQueryParam('limit', limit, params);
    params = addQueryParam('offset', offset, params);

    http.open(GET, url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function postAddressDelivery(deliveryService, deliveryType, recipient, phoneNumber, city, streetType, street, house, corpus, entrance, floor, flatType, flat, mailbox, zipCode, warehouse, actionSuccess, actionError){
    let http = new XMLHttpRequest();
    let url = '/api/addresses/delivery';

    http.open("POST", url, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);
    let requestBody = {};
    requestBody.data = {};

    if (typeof deliveryService !== 'undefined') {

        if (typeof deliveryService === 'string') {
            if (deliveryService !== ''){
                requestBody.data.deliveryService = deliveryService;
            } else {
                requestBody.data.deliveryService = 'null';
            }
        } else if (typeof deliveryService === 'object') {
            requestBody.data.deliveryService = 'null';
        }
    }

    if (typeof deliveryType !== 'undefined') {

        if (typeof deliveryType === 'string') {
            if (deliveryType !== ''){
                requestBody.data.deliveryType = deliveryType;
            } else {
                requestBody.data.deliveryType = 'null';
            }
        } else if (typeof deliveryType === 'object') {
            requestBody.data.deliveryType = 'null';
        }
    }

    if (typeof recipient !== 'undefined') {

        if (typeof recipient === 'string') {
            if (recipient !== ''){
                requestBody.data.recipient = recipient;
            } else {
                requestBody.data.recipient = 'null';
            }
        } else if (typeof recipient === 'object') {
            requestBody.data.recipient = 'null';
        }
    }

    if (typeof phoneNumber !== 'undefined') {

        if (typeof phoneNumber === 'string') {
            if (phoneNumber !== ''){
                requestBody.data.phoneNumber = phoneNumber;
            } else {
                requestBody.data.phoneNumber = 'null';
            }
        } else if (typeof phoneNumber === 'object') {
            requestBody.data.phoneNumber = 'null';
        }
    }

    if (typeof city !== 'undefined') {

        if (typeof city === 'string') {
            if (city !== ''){
                requestBody.data.city = city;
            } else {
                requestBody.data.city = 'null';
            }
        } else if (typeof city === 'object') {
            requestBody.data.city = 'null';
        }
    }

    if (typeof streetType !== 'undefined') {

        if (typeof streetType === 'string') {
            if (streetType !== ''){
                requestBody.data.streetType = streetType;
            } else {
                requestBody.data.streetType = 'null';
            }
        } else if (typeof streetType === 'object') {
            requestBody.data.streetType = 'null';
        }
    }

    if (typeof street !== 'undefined') {

        if (typeof street === 'string') {
            if (street !== ''){
                requestBody.data.street = street;
            } else {
                requestBody.data.street = 'null';
            }
        } else if (typeof street === 'object') {
            requestBody.data.street = 'null';
        }
    }

    if (typeof house !== 'undefined') {

        if (typeof house === 'string') {
            if (house !== ''){
                requestBody.data.house = house;
            } else {
                requestBody.data.house = 'null';
            }
        } else if (typeof house === 'object') {
            requestBody.data.house = 'null';
        }
    }

    if (typeof corpus !== 'undefined') {

        if (typeof corpus === 'string') {
            if (corpus !== ''){
                requestBody.data.corpus = corpus;
            } else {
                requestBody.data.corpus = 'null';
            }
        } else if (typeof corpus === 'object') {
            requestBody.data.corpus = 'null';
        }
    }

    if (typeof entrance !== 'undefined') {

        if (typeof entrance === 'string') {
            if (entrance !== ''){
                requestBody.data.entrance = entrance;
            } else {
                requestBody.data.entrance = 'null';
            }
        } else if (typeof entrance === 'object') {
            requestBody.data.entrance = 'null';
        }
    }

    if (typeof floor !== 'undefined') {

        if (typeof floor === 'string') {
            if (floor !== ''){
                requestBody.data.floor = floor;
            } else {
                requestBody.data.floor = 'null';
            }
        } else if (typeof floor === 'object') {
            requestBody.data.floor = 'null';
        }
    }

    if (typeof flatType !== 'undefined') {

        if (typeof flatType === 'string') {
            if (flatType !== ''){
                requestBody.data.flatType = flatType;
            } else {
                requestBody.data.flatType = 'null';
            }
        } else if (typeof flatType === 'object') {
            requestBody.data.flatType = 'null';
        }
    }

    if (typeof flat !== 'undefined') {

        if (typeof flat === 'string') {
            if (flat !== ''){
                requestBody.data.flat = flat;
            } else {
                requestBody.data.flat = 'null';
            }
        } else if (typeof flat === 'object') {
            requestBody.data.flat = 'null';
        }
    }

    if (typeof mailbox !== 'undefined') {

        if (typeof mailbox === 'string') {
            if (mailbox !== ''){
                requestBody.data.mailbox = mailbox;
            } else {
                requestBody.data.mailbox = 'null';
            }
        } else if (typeof mailbox === 'object') {
            requestBody.data.mailbox = 'null';
        }
    }

    if (typeof zipCode !== 'undefined') {

        if (typeof zipCode === 'string') {
            if (zipCode !== ''){
                requestBody.data.zipCode = zipCode;
            } else {
                requestBody.data.zipCode = 'null';
            }
        } else if (typeof zipCode === 'object') {
            requestBody.data.zipCode = 'null';
        }
    }

    if (typeof warehouse !== 'undefined') {

        if (typeof warehouse === 'string') {
            if (warehouse !== ''){
                requestBody.data.warehouse = warehouse;
            } else {
                requestBody.data.warehouse = 'null';
            }
        } else if (typeof warehouse === 'object') {
            requestBody.data.warehouse = 'null';
        }
    }

    debug('Method : ' + POST + '\n' +
        'URL : ' + url + '\n' +
        'Body : ' + JSON.stringify(requestBody));

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + POST + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + POST + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send(JSON.stringify(requestBody));
}

function putAddressDelivery(uuid, statusUuid, deliveryService, deliveryType, recipient, phoneNumber, city, streetType, street, house, corpus, entrance, floor, flatType, flat, mailbox, zipCode, warehouse, actionSuccess, actionError){
    let http = new XMLHttpRequest();
    let url = '/api/addresses/delivery';

    http.open("PUT", url, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);
    let requestBody = {};
    requestBody.data = {};

    if (typeof uuid !== 'undefined') {
        if (typeof uuid === 'string') {
            requestBody.data.uuid = uuid;
        }
    }

    if (typeof statusUuid !== 'undefined') {

        requestBody.data.status = {};

        if (typeof statusUuid === 'string') {
            if (statusUuid !== ''){
                requestBody.data.status.uuid = statusUuid;
            } else {
                requestBody.data.status.uuid = 'null';
            }
        } else if (typeof statusUuid === 'object') {
            requestBody.data.status.uuid = 'null';
        }
    }

    if (typeof deliveryService !== 'undefined') {

        if (typeof deliveryService === 'string') {
            if (deliveryService !== ''){
                requestBody.data.deliveryService = deliveryService;
            } else {
                requestBody.data.deliveryService = 'null';
            }
        } else if (typeof deliveryService === 'object') {
            requestBody.data.deliveryService = 'null';
        }
    }

    if (typeof deliveryType !== 'undefined') {

        if (typeof deliveryType === 'string') {
            if (deliveryType !== ''){
                requestBody.data.deliveryType = deliveryType;
            } else {
                requestBody.data.deliveryType = 'null';
            }
        } else if (typeof deliveryType === 'object') {
            requestBody.data.deliveryType = 'null';
        }
    }

    if (typeof recipient !== 'undefined') {

        if (typeof recipient === 'string') {
            if (recipient !== ''){
                requestBody.data.recipient = recipient;
            } else {
                requestBody.data.recipient = 'null';
            }
        } else if (typeof recipient === 'object') {
            requestBody.data.recipient = 'null';
        }
    }

    if (typeof phoneNumber !== 'undefined') {

        if (typeof phoneNumber === 'string') {
            if (phoneNumber !== ''){
                requestBody.data.phoneNumber = phoneNumber;
            } else {
                requestBody.data.phoneNumber = 'null';
            }
        } else if (typeof phoneNumber === 'object') {
            requestBody.data.phoneNumber = 'null';
        }
    }

    if (typeof city !== 'undefined') {

        if (typeof city === 'string') {
            if (city !== ''){
                requestBody.data.city = city;
            } else {
                requestBody.data.city = 'null';
            }
        } else if (typeof city === 'object') {
            requestBody.data.city = 'null';
        }
    }

    if (typeof streetType !== 'undefined') {

        if (typeof streetType === 'string') {
            if (streetType !== ''){
                requestBody.data.streetType = streetType;
            } else {
                requestBody.data.streetType = 'null';
            }
        } else if (typeof streetType === 'object') {
            requestBody.data.streetType = 'null';
        }
    }

    if (typeof street !== 'undefined') {

        if (typeof street === 'string') {
            if (street !== ''){
                requestBody.data.street = street;
            } else {
                requestBody.data.street = 'null';
            }
        } else if (typeof street === 'object') {
            requestBody.data.street = 'null';
        }
    }

    if (typeof house !== 'undefined') {

        if (typeof house === 'string') {
            if (house !== ''){
                requestBody.data.house = house;
            } else {
                requestBody.data.house = 'null';
            }
        } else if (typeof house === 'object') {
            requestBody.data.house = 'null';
        }
    }

    if (typeof corpus !== 'undefined') {

        if (typeof corpus === 'string') {
            if (corpus !== ''){
                requestBody.data.corpus = corpus;
            } else {
                requestBody.data.corpus = 'null';
            }
        } else if (typeof corpus === 'object') {
            requestBody.data.corpus = 'null';
        }
    }

    if (typeof entrance !== 'undefined') {

        if (typeof entrance === 'string') {
            if (entrance !== ''){
                requestBody.data.entrance = entrance;
            } else {
                requestBody.data.entrance = 'null';
            }
        } else if (typeof entrance === 'object') {
            requestBody.data.entrance = 'null';
        }
    }

    if (typeof floor !== 'undefined') {

        if (typeof floor === 'string') {
            if (floor !== ''){
                requestBody.data.floor = floor;
            } else {
                requestBody.data.floor = 'null';
            }
        } else if (typeof floor === 'object') {
            requestBody.data.floor = 'null';
        }
    }

    if (typeof flatType !== 'undefined') {

        if (typeof flatType === 'string') {
            if (flatType !== ''){
                requestBody.data.flatType = flatType;
            } else {
                requestBody.data.flatType = 'null';
            }
        } else if (typeof flatType === 'object') {
            requestBody.data.flatType = 'null';
        }
    }

    if (typeof flat !== 'undefined') {

        if (typeof flat === 'string') {
            if (flat !== ''){
                requestBody.data.flat = flat;
            } else {
                requestBody.data.flat = 'null';
            }
        } else if (typeof flat === 'object') {
            requestBody.data.flat = 'null';
        }
    }

    if (typeof mailbox !== 'undefined') {

        if (typeof mailbox === 'string') {
            if (mailbox !== ''){
                requestBody.data.mailbox = mailbox;
            } else {
                requestBody.data.mailbox = 'null';
            }
        } else if (typeof mailbox === 'object') {
            requestBody.data.mailbox = 'null';
        }
    }

    if (typeof zipCode !== 'undefined') {

        if (typeof zipCode === 'string') {
            if (zipCode !== ''){
                requestBody.data.zipCode = zipCode;
            } else {
                requestBody.data.zipCode = 'null';
            }
        } else if (typeof zipCode === 'object') {
            requestBody.data.zipCode = 'null';
        }
    }

    if (typeof warehouse !== 'undefined') {

        if (typeof warehouse === 'string') {
            if (warehouse !== ''){
                requestBody.data.warehouse = warehouse;
            } else {
                requestBody.data.warehouse = 'null';
            }
        } else if (typeof warehouse === 'object') {
            requestBody.data.warehouse = 'null';
        }
    }

    debug('Method : ' + PUT + '\n' +
        'URL : ' + url + '\n' +
        'Body : ' + JSON.stringify(requestBody));

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + PUT + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + PUT + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send(JSON.stringify(requestBody));
}

function getAddressJuridicalItem(uuid, actionSuccess, actionError){
    let http = new XMLHttpRequest();
    let url = '/api/addresses/juridical/'+uuid;
    let params = '';

    http.open(GET, url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getAddressJuridicalList(statusUuid, orderBy, limit, offset, actionSuccess, actionError){
    let http = new XMLHttpRequest();
    let url = '/api/addresses/juridical';
    let params = '';

    params = addQueryParam('status', status, params);
    params = addQueryParam('orderBy', orderBy, params);
    params = addQueryParam('limit', limit, params);
    params = addQueryParam('offset', offset, params);

    http.open(GET, url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function postAddressJuridical(title, limitation, actionSuccess, actionError){
    let http = new XMLHttpRequest();
    let url = '/api/addresses/juridical';

    http.open("POST", url, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);
    let requestBody = {};
    requestBody.data = {};

    if (typeof title !== 'undefined') {

        if (typeof title === 'string') {
            if (title !== ''){
                requestBody.data.title = title;
            } else {
                requestBody.data.title = 'null';
            }
        } else if (typeof title === 'object') {
            requestBody.data.title = 'null';
        }
    }

    if (typeof limitation !== 'undefined') {

        if (typeof limitation === 'number') {
            if (limitation !== ''){
                requestBody.data.limitation = limitation;
            } else {
                requestBody.data.limitation = 0;
            }
        } else if (typeof limitation === 'object') {
            requestBody.data.limitation = 0;
        }
    }

    debug('Method : ' + POST + '\n' +
        'URL : ' + url + '\n' +
        'Body : ' + JSON.stringify(requestBody));

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + POST + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + POST + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send(JSON.stringify(requestBody));
}

function putAddressJuridical(uuid, statusUuid, title, limitation, actionSuccess, actionError){
    let http = new XMLHttpRequest();
    let url = '/api/addresses/juridical';

    http.open("PUT", url, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);
    let requestBody = {};
    requestBody.data = {};

    if (typeof uuid !== 'undefined') {
        if (typeof uuid === 'string') {
            requestBody.data.uuid = uuid;
        }
    }

    if (typeof statusUuid !== 'undefined') {

        requestBody.data.status = {};

        if (typeof statusUuid === 'string') {
            if (statusUuid !== ''){
                requestBody.data.status.uuid = statusUuid;
            } else {
                requestBody.data.status.uuid = 'null';
            }
        } else if (typeof statusUuid === 'object') {
            requestBody.data.status.uuid = 'null';
        }
    }

    if (typeof title !== 'undefined') {

        if (typeof title === 'string') {
            if (title !== ''){
                requestBody.data.title = title;
            } else {
                requestBody.data.title = 'null';
            }
        } else if (typeof title === 'object') {
            requestBody.data.title = 'null';
        }
    }

    if (typeof limitation !== 'undefined') {

        if (typeof limitation === 'number') {
            if (limitation !== ''){
                requestBody.data.limitation = limitation;
            } else {
                requestBody.data.limitation = 0;
            }
        } else if (typeof limitation === 'object') {
            requestBody.data.limitation = 0;
        }
    }

    debug('Method : ' + PUT + '\n' +
        'URL : ' + url + '\n' +
        'Body : ' + JSON.stringify(requestBody));

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + PUT + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + PUT + '\n' +
                    'URL : ' + url + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send(JSON.stringify(requestBody));
}


function getSubjectsItem(uuid, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/subjects/'+uuid;
    let params = '';

    http.open(GET, url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}

function getSubjectsList(uuid, typeUuid, statusUuid, orderBy, limit, offset, actionSuccess, actionError) {
    let http = new XMLHttpRequest();
    let url = '/api/subjects';
    let params = '';

    params = addQueryParam('uuid', uuid, params);
    params = addQueryParam('type', typeUuid, params);
    params = addQueryParam('status', statusUuid, params);
    params = addQueryParam('orderBy', orderBy, params);
    params = addQueryParam('limit', limit, params);
    params = addQueryParam('offset', offset, params);

    http.open(GET, url + '?' + params, true);
    http.timeout = 10000;
    http.setRequestHeader("Content-type", "application/json");
    http.setRequestHeader("Auth-Token", AuthToken);

    debug('Method : ' + GET + '\n' +
        'URL : ' + url + '?' + params + '\n' +
        'Body : null');

    http.onreadystatechange = function () {
        if (http.readyState === 4) {
            let respData = JSON.parse(http.response);
            if (http.status === 200) {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionSuccess(respData)
                }
            } else {
                debug('Method : ' + GET + '\n' +
                    'URL : ' + url + '?' + params + '\n' +
                    'Body : ' + JSON.stringify(respData));
                if (typeof actionError !== 'undefined') {
                    actionError(respData)
                }
            }
        }
    };
    http.send();
}



