function init() {
    getUsersItem(UserUuid, profileBuildInfo, actionError);
}

function profileBuildInfoContent() {
    $("#content").empty();
    $("#content").append($('' +
        '<div class="layout-px-spacing">\n' +
        '    <div class="row layout-spacing">\n' +
        '        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 layout-top-spacing">\n' +
        '            <div class="user-profile layout-spacing">\n' +
        '                <div class="widget-content widget-content-area" id="info-element"></div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));
}

function profileBuildEditContent() {
    $("#content").empty();
    $("#content").append($('' +
        '<div class="layout-px-spacing">\n' +
        '    <div class="account-settings-container layout-top-spacing">\n' +
        '        <div class="account-content">\n' +
        '            <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"\n' +
        '                 data-offset="-100">\n' +
        '                <div class="row">\n' +
        '                    <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">\n' +
        '                        <form id="account-info" class="section general-info"></form>\n' +
        '                    </div>\n' +
        '                    <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">\n' +
        '                        <form id="profile-info" class="section general-info"></form>\n' +
        '                    </div>\n' +
        '                    <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">\n' +
        '                        <form id="contacts" class="section general-info"></form>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '        <div class="account-settings-footer">\n' +
        '            <div class="as-footer-container">\n' +
        '                <button onclick="getUsersItem(UserUuid, profileBuildInfo, actionError);" class="btn btn-warning">Відмінити редагування</button>\n' +
        '                <button onclick="saveEditedData();" class="btn btn-primary">Зберегти зміни</button>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));
}

function profileBuildInfo(responseData) {

    profileBuildInfoContent();

    let element = responseData.data;

    $("#info-element").empty();
    $("#info-element").append($('' +
        '<div class="d-flex justify-content-between">\n' +
        '    <h3 class="">Інформація</h3>\n' +
        '    <a href="#" onclick="getUsersItem(UserUuid, profileBuildEdit, actionError);" class="mt-2 edit-profile">\n' +
        '        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
        '             stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3">\n' +
        '            <path d="M12 20h9"></path>\n' +
        '            <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>\n' +
        '        </svg>\n' +
        '    </a>\n' +
        '</div>' +
        '<div class="text-center user-info">\n' +
        '    <p class="">' + ((element.fullName !== undefined) ? element.fullName : element.login) + '</p>\n' +
        '</div>' +
        '').fadeIn('slow'));

    $("#info-element").append($('' +
        '<div class="user-info-list">\n' +
        '    <div class="">\n' +
        '        <ul class="contacts-block list-unstyled" id="info-attributes">\n' +
        '        </ul>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));

    $("#info-attributes").append($('' +
        '<li class="contacts-block__item">\n' +
        '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
        '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
        '         stroke-linecap="round" stroke-linejoin="round"\n' +
        '         class="feather feather-user">\n' +
        '        <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path>\n' +
        '        <circle cx="12" cy="7" r="4"></circle>\n' +
        '    </svg>\n' +
        '    ' + ((element.login !== undefined) ? element.login : '') + '\n' +
        '</li>\n' +
        '').fadeIn('slow'));
    $("#info-attributes").append($('' +
        '<li class="contacts-block__item">\n' +
        '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
        '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
        '         stroke-linecap="round" stroke-linejoin="round"\n' +
        '         class="feather feather-lock">\n' +
        '        <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
        '        <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>\n' +
        '    </svg>\n' +
        '    ********\n' +
        '</li>\n' +
        '').fadeIn('slow'));
    if (element.status.title !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-sliders">\n' +
            '        <line x1="4" y1="21" x2="4" y2="14"></line>\n' +
            '        <line x1="4" y1="10" x2="4" y2="3"></line>\n' +
            '        <line x1="12" y1="21" x2="12" y2="12"></line>\n' +
            '        <line x1="12" y1="8" x2="12" y2="3"></line>\n' +
            '        <line x1="20" y1="21" x2="20" y2="16"></line>\n' +
            '        <line x1="20" y1="12" x2="20" y2="3"></line>\n' +
            '        <line x1="1" y1="14" x2="7" y2="14"></line>\n' +
            '        <line x1="9" y1="8" x2="15" y2="8"></line>\n' +
            '        <line x1="17" y1="16" x2="23" y2="16"></line>\n' +
            '    </svg>\n' +
            '    <span class="badge outline-badge-' + ((element.status.code === 'ACT') ? 'success' : ((element.status.code === 'INACT') ? 'danger' : 'dark')) + '"> ' + element.status.title + ' </span>\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
    if (element.type.title !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-sliders">\n' +
            '        <line x1="4" y1="21" x2="4" y2="14"></line>\n' +
            '        <line x1="4" y1="10" x2="4" y2="3"></line>\n' +
            '        <line x1="12" y1="21" x2="12" y2="12"></line>\n' +
            '        <line x1="12" y1="8" x2="12" y2="3"></line>\n' +
            '        <line x1="20" y1="21" x2="20" y2="16"></line>\n' +
            '        <line x1="20" y1="12" x2="20" y2="3"></line>\n' +
            '        <line x1="1" y1="14" x2="7" y2="14"></line>\n' +
            '        <line x1="9" y1="8" x2="15" y2="8"></line>\n' +
            '        <line x1="17" y1="16" x2="23" y2="16"></line>\n' +
            '    </svg>\n' +
            '    ' + element.type.title + '\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
    if (element.lastName !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-align-center">\n' +
            '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
            '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
            '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
            '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
            '    </svg>\n' +
            '    ' + element.lastName + '\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
    if (element.firstName != null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-align-center">\n' +
            '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
            '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
            '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
            '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
            '    </svg>\n' +
            '    ' + element.firstName + '\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
    if (element.secondName !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-align-center">\n' +
            '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
            '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
            '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
            '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
            '    </svg>\n' +
            '    ' + element.secondName + '\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
    if (element.shortName != null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-align-center">\n' +
            '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
            '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
            '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
            '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
            '    </svg>\n' +
            '    ' + element.shortName + '\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
    if (element.fullName !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-align-center">\n' +
            '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
            '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
            '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
            '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
            '    </svg>\n' +
            '    ' + element.fullName + '\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
    if (element.email !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <a href="mailto:' + element.email + '">\n' +
            '        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '             viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
            '             stroke-width="2" stroke-linecap="round" stroke-linejoin="round"\n' +
            '             class="feather feather-mail">\n' +
            '            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>\n' +
            '            <polyline points="22,6 12,13 2,6"></polyline>\n' +
            '        </svg>\n' +
            '        ' + element.email + '</a>\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
    if (element.phone !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <a href="tel:' + element.phone + '">\n' +
            '        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '             viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
            '             stroke-width="2"\n' +
            '             stroke-linecap="round" stroke-linejoin="round"\n' +
            '             class="feather feather-phone">\n' +
            '            <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>\n' +
            '        </svg>\n' +
            '        ' + element.phone + '</a>\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }
}

function profileBuildEdit(responseData) {

    profileBuildEditContent();

    let element = responseData.data;

    $("#account-info").append($('' +
        '<div class="info">\n' +
        '    <h6 class="">Інформація аккаунту</h6>\n' +
        '    <div class="row">\n' +
        '        <div class="col-lg-11 mx-auto">\n' +
        '            <div id="account-info-content" class="row">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));

    $("#account-info-content").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="login">Логін</label>\n' +
        '        <input type="text" class="form-control mb-4" id="login"\n' +
        '               placeholder="Логін" ' + ((element.login !== null) ? 'value="' + element.login + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));
    $("#account-info-content").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="password">Пароль</label>\n' +
        '        <input type="text" class="form-control mb-4" id="password"\n' +
        '               placeholder="Пароль" ' + ((element.password !== null) ? 'value="' + element.password + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));

    $("#profile-info").append($('' +
        '<div class="info">\n' +
        '    <h5 class="">Інформація користувача</h5>\n' +
        '    <div class="row">\n' +
        '        <div class="col-lg-11 mx-auto">\n' +
        '            <div id="profile-info-content1" class="row">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="row">\n' +
        '        <div class="col-lg-11 mx-auto">\n' +
        '            <div id="profile-info-content2" class="row">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));

    $("#profile-info-content1").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="lastName">Прізвище</label>\n' +
        '        <input type="text" class="form-control mb-4" id="lastName"\n' +
        '               onkeydown="setShortName();setFullName();"\n' +
        '               onkeypress="setShortName();setFullName();"\n' +
        '               onkeyup="setShortName();setFullName();"\n' +
        '               onchange="setShortName();setFullName();"\n' +
        '               placeholder="Прізвище" ' + ((element.lastName !== null) ? 'value="' + element.lastName + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));
    $("#profile-info-content1").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="firstName">Ім\'я</label>\n' +
        '        <input type="text" class="form-control mb-4" id="firstName"\n' +
        '               onkeydown="setShortName();setFullName();"\n' +
        '               onkeypress="setShortName();setFullName();"\n' +
        '               onkeyup="setShortName();setFullName();"\n' +
        '               onchange="setShortName();setFullName();"\n' +
        '               placeholder="Ім\'я" ' + ((element.firstName !== null) ? 'value="' + element.firstName + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));
    $("#profile-info-content1").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="secondName">По-батькові</label>\n' +
        '        <input type="text" class="form-control mb-4" id="secondName"\n' +
        '               onkeydown="setShortName();setFullName();"\n' +
        '               onkeypress="setShortName();setFullName();"\n' +
        '               onkeyup="setShortName();setFullName();"\n' +
        '               onchange="setShortName();setFullName();"\n' +
        '               placeholder="По-батькові" ' + ((element.secondName !== null) ? 'value="' + element.secondName + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));

    $("#profile-info-content2").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="shortName">ПІБ (скорочено)</label>\n' +
        '        <input disabled type="text" class="form-control mb-4" id="shortName"\n' +
        '               placeholder="ПІБ (скорочено)" ' + ((element.shortName !== null) ? 'value="' + element.shortName + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));
    $("#profile-info-content2").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="fullName">ПІБ (повністтю)</label>\n' +
        '        <input disabled type="text" class="form-control mb-4" id="fullName"\n' +
        '               placeholder="ПІБ (повністтю)" ' + ((element.fullName !== null) ? 'value="' + element.fullName + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));

    $("#contacts").append($('' +
        '<div class="info">\n' +
        '    <h5 class="">Контактна інформація</h5>\n' +
        '    <div class="row">\n' +
        '        <div class="col-md-11 mx-auto">\n' +
        '            <div id="contacts-content" class="row">\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));

    $("#contacts-content").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="email">Email</label>\n' +
        '        <input type="text" class="form-control mb-4" id="email"\n' +
        '               placeholder="Email" ' + ((element.email !== null) ? 'value="' + element.email + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));
    $("#contacts-content").append($('' +
        '<div class="col-md-6">\n' +
        '    <div class="form-group">\n' +
        '        <label for="phone">Номер телефону</label>\n' +
        '        <input type="text" class="form-control mb-4"\n' +
        '               id="phone" placeholder="Номер телефону"\n' +
        '               ' + ((element.phone !== null) ? 'value="' + element.phone + '"' : '') + '>\n' +
        '    </div>\n' +
        '</div>\n' +
        '').fadeIn('slow'));
}

function saveEditedData() {
    putUsers(UserUuid,
        undefined,
        undefined,
        (($("#login").val() !== '') ? $("#login").val() : null),
        (($("#password").val() !== '') ? $("#password").val() : null),
        (($("#email").val() !== '') ? $("#email").val() : null),
        (($("#phone").val() !== '') ? $("#phone").val() : null),
        (($("#firstName").val() !== '') ? $("#firstName").val() : null),
        (($("#secondName").val() !== '') ? $("#secondName").val() : null),
        (($("#lastName").val() !== '') ? $("#lastName").val() : null),
        (($("#shortName").val() !== '') ? $("#shortName").val() : null),
        (($("#fullName").val() !== '') ? $("#fullName").val() : null),
        profileBuildInfo,
        actionError);
}

function actionError(responseData) {
    alert('Error code : ' + responseData.state.code + '\n' + 'Error message : ' + responseData.state.message)
}

function setShortName() {
    if ($("#lastName").val() !== '') {
        $("#shortName").val($("#lastName").val());
    }

    if ($("#firstName").val() !== '') {
        $("#shortName").val($("#lastName").val() + ". " + $("#firstName").val().substr(0, 1));
    }

    if ($("#secondName").val() !== '') {
        $("#shortName").val($("#lastName").val() + " " + $("#firstName").val().substr(0, 1) + ". " + $("#secondName").val().substr(0, 1) + ".");
    }
}

function setFullName() {
    if ($("#lastName").val() !== '') {
        $("#fullName").val($("#lastName").val());
    }

    if ($("#firstName").val() !== '') {
        $("#fullName").val($("#lastName").val() + " " + $("#firstName").val());
    }

    if ($("#secondName").val() !== '') {
        $("#fullName").val($("#lastName").val() + " " + $("#firstName").val() + " " + $("#secondName").val());
    }
}