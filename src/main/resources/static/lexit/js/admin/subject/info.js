function init() {
    getSubjectsItem(
        InfoUuid,
        subjectBuildInfo,
        actionError
    );
}

function subjectBuildInfoContent() {
    $("#content").empty();
    $("#content").append($('' +
        '<div class="layout-px-spacing">\n' +
        '    <div class="row layout-spacing">\n' +
        '        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 layout-top-spacing">\n' +
        '            <div class="user-profile layout-spacing">\n' +
        '                <div class="widget-content widget-content-area" id="info-element"></div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));
}

function subjectBuildInfo(responseData) {

    subjectBuildInfoContent();

    let element = responseData.data;

    $("#info-element").empty();
    $("#info-element").append($('' +
        '<div class="d-flex justify-content-between">\n' +
        '    <h3 class="">Інформація</h3>\n' +
        '</div>' +
        '<div class="text-center user-info">\n' +
        '    <p class="">' + element.title + '</p>\n' +
        '</div>' +
        '').fadeIn('slow'));

    $("#info-element").append($('' +
        '<div class="user-info-list">\n' +
        '    <div class="">\n' +
        '        <ul class="contacts-block list-unstyled" id="info-attributes">\n' +
        '        </ul>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));

    if (element.type !== null) {
        if (element.type.uuid !== null) {
            $("#info-attributes").append($('' +
                '<li class="contacts-block__item">\n' +
                '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
                '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
                '         stroke-linecap="round" stroke-linejoin="round"\n' +
                '         class="feather feather-align-center">\n' +
                '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
                '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
                '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
                '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
                '    </svg>\n' +
                '    ' + element.type.title + '\n' +
                '</li>\n' +
                '').fadeIn('slow'));
        }
    }

    if (element.status !== null) {
        if (element.status.uuid !== null) {
            $("#info-attributes").append($('' +
                '<li class="contacts-block__item">\n' +
                '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
                '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
                '         stroke-linecap="round" stroke-linejoin="round"\n' +
                '         class="feather feather-align-center">\n' +
                '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
                '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
                '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
                '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
                '    </svg>\n' +
                '    ' + element.status.title + '\n' +
                '</li>\n' +
                '').fadeIn('slow'));
        }
    }

    if (element.title !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-align-center">\n' +
            '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
            '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
            '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
            '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
            '    </svg>\n' +
            '    ' + element.title + '\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }

    if (element.legalCode !== null) {
        $("#info-attributes").append($('' +
            '<li class="contacts-block__item">\n' +
            '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
            '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
            '         stroke-linecap="round" stroke-linejoin="round"\n' +
            '         class="feather feather-align-center">\n' +
            '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
            '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
            '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
            '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
            '    </svg>\n' +
            '    ' + element.legalCode + '\n' +
            '</li>\n' +
            '').fadeIn('slow'));
    }

    if (element.owner.lastName !== null) {
        if (element.owner.lastName !== null) {
            $("#info-attributes").append($('' +
                '<li class="contacts-block__item">\n' +
                '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
                '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
                '         stroke-linecap="round" stroke-linejoin="round"\n' +
                '         class="feather feather-align-center">\n' +
                '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
                '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
                '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
                '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
                '    </svg>\n' +
                '    ' + element.owner.lastName + '\n' +
                '</li>\n' +
                '').fadeIn('slow'));
        }
        if (element.owner.firstName != null) {
            $("#info-attributes").append($('' +
                '<li class="contacts-block__item">\n' +
                '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
                '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
                '         stroke-linecap="round" stroke-linejoin="round"\n' +
                '         class="feather feather-align-center">\n' +
                '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
                '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
                '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
                '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
                '    </svg>\n' +
                '    ' + element.owner.firstName + '\n' +
                '</li>\n' +
                '').fadeIn('slow'));
        }
        if (element.owner.secondName !== null) {
            $("#info-attributes").append($('' +
                '<li class="contacts-block__item">\n' +
                '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
                '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
                '         stroke-linecap="round" stroke-linejoin="round"\n' +
                '         class="feather feather-align-center">\n' +
                '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
                '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
                '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
                '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
                '    </svg>\n' +
                '    ' + element.owner.secondName + '\n' +
                '</li>\n' +
                '').fadeIn('slow'));
        }
        if (element.owner.shortName != null) {
            $("#info-attributes").append($('' +
                '<li class="contacts-block__item">\n' +
                '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
                '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
                '         stroke-linecap="round" stroke-linejoin="round"\n' +
                '         class="feather feather-align-center">\n' +
                '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
                '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
                '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
                '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
                '    </svg>\n' +
                '    ' + element.owner.shortName + '\n' +
                '</li>\n' +
                '').fadeIn('slow'));
        }
        if (element.owner.fullName !== null) {
            $("#info-attributes").append($('' +
                '<li class="contacts-block__item">\n' +
                '    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"\n' +
                '         viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"\n' +
                '         stroke-linecap="round" stroke-linejoin="round"\n' +
                '         class="feather feather-align-center">\n' +
                '        <line x1="18" y1="10" x2="6" y2="10"></line>\n' +
                '        <line x1="21" y1="6" x2="3" y2="6"></line>\n' +
                '        <line x1="21" y1="14" x2="3" y2="14"></line>\n' +
                '        <line x1="18" y1="18" x2="6" y2="18"></line>\n' +
                '    </svg>\n' +
                '    ' + element.owner.fullName + '\n' +
                '</li>\n' +
                '').fadeIn('slow'));
        }
    }
}

function actionError(responseData) {
    alert('Error code : ' + responseData.state.code + '\n' + 'Error message : ' + responseData.state.message)
}