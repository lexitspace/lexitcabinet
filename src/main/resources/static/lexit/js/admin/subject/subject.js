function init() {
    getSubjectsList(
        null,
        null,
        null,
        null,
        null,
        null,
        subjectSubjectBuildInfo,
        actionError);
}

function subjectSubjectBuildListContent() {
    $("#content").empty();
    $("#content").append($('' +
        '<div class="layout-px-spacing">\n' +
        '    <div class="row layout-top-spacing" id="cancel-row">\n' +
        '        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">\n' +
        '            <div class="widget-content widget-content-area br-6">\n' +
        '                <div class="table-responsive mb-4 mt-4">\n' +
        '                    <table id="data_pagination" class="table table-hover" style="width:100%">\n' +
        '                        <thead>\n' +
        '                            <tr>\n' +
        '                                <th>Назва</th>\n' +
        '                                <th>Адреса</th>\n' +
        '                                <th>ЕДРПОУ</th>\n' +
        '                                <th>Тип</th>\n' +
        '                                <th>Статус</th>\n' +
        '                                <th class="text-center">Дії</th>\n' +
        '                            </tr>\n' +
        '                        </thead>\n' +
        '                        <tbody id="subject-list-elements">\n' +
        '                        </tbody>\n' +
        '                        <tfoot>\n' +
        '                            <tr>\n' +
        '                                <th>Назва</th>\n' +
        '                                <th>Адреса</th>\n' +
        '                                <th>ЕДРПОУ</th>\n' +
        '                                <th>Тип</th>\n' +
        '                                <th>Статус</th>\n' +
        '                                <th class="text-center">Дії</th>\n' +
        '                            </tr>\n' +
        '                        </tfoot>\n' +
        '                    </table>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));
}

function subjectSubjectBuildInfo(responseData) {

    subjectSubjectBuildListContent();

    for (i = 0; i < responseData.meta.size; i++) {
        let element = responseData.data[i];

        let title = ((element.title !== null) ? element.title : '');

        let address = ((element.address !== null) ? element.address.title : '');

        let legalCode = ((element.legalCode !== null) ? element.legalCode : '');

        let status = (
            (element.status !== null) ?
                (
                    (element.status.uuid === SubjectStatusUuidACT) ?
                        '<span class="badge outline-badge-success"> ' + element.status.title + ' </span>' :
                        (
                            (element.status.uuid === SubjectStatusUuidCRT) ?
                                '<span class="badge outline-badge-warning"> ' + element.status.title + ' </span>' :
                                (
                                    (element.status.uuid === SubjectStatusUuidPSD) ?
                                        '<span class="badge outline-badge-secondary"> ' + element.status.title + ' </span>' :
                                        (
                                            (element.status.uuid === SubjectStatusUuidINACT) ?
                                                '<span class="badge outline-badge-danger"> ' + element.status.title + ' </span>' :
                                                '<span class="badge outline-badge-dark"> Невизначений </span>'
                                        )
                                )
                        )
                ) : ''
        );

        let type = ((element.type !== null) ? element.type.title : '');

        $("#subject-list-elements").append($('' +
            '<tr id="subject-' + element.uuid + '">\n' +
            '    <td>' + title + '</td>\n' +
            '    <td>' + address + '</td>\n' +
            '    <td>' + legalCode + '</td>\n' +
            '    <td>' + type + '</td>\n' +
            '    <td>' + status + '</td>\n' +
            '    <td id="subject-' + element.uuid + '-actions" class="text-center"></td>\n' +
            '</tr>\n' +
            '').fadeIn('slow'));

        $("#subject-" + element.uuid + "-actions").append($('' +
            '<a href="/cabinet/subject/info?uuid=' + element.uuid + '" target="_blank" class="btn btn-outline-info mb-2 mr-2">\n' +
            '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
            '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-info">\n' +
            '      <circle cx="12" cy="12" r="10"></circle>\n' +
            '      <line x1="12" y1="16" x2="12" y2="12"></line>\n' +
            '      <line x1="12" y1="8" x2="12.01" y2="8"></line>\n' +
            '   </svg>' +
            '</a>\n' +
            '').fadeIn('slow'));
    }

    $('#data_pagination').DataTable({
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Показано сторінок _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Пошук...",
            "sLengthMenu": "Результати :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [10, 20, 50],
        "pageLength": 10
    });

}

function actionError(responseData) {
    alert('Error code : ' + responseData.state.code + '\n' + 'Error message : ' + responseData.state.message);
}