function init() {
    getAddressDeliveryList(
        [AddressStatusUuidACT, AddressStatusUuidINACT],
        null,
        null,
        null,
        addressDeliveryBuildInfo,
        actionError);
}

function addressDeliveryBuildListContent() {
    $("#content").empty();
    $("#content").append($('' +
        '<div class="layout-px-spacing">\n' +
        '    <div class="row layout-top-spacing" id="cancel-row">\n' +
        '        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">\n' +
        '            <div class="widget-content widget-content-area br-6">\n' +
        '                <div class="table-responsive mb-4 mt-4">\n' +
        '                    <table id="data_pagination" class="table table-hover" style="width:100%">\n' +
        '                        <thead>\n' +
        '                            <tr>\n' +
        '                                <th>Адреса доставки</th>\n' +
        '                                <th>Служба доставки</th>\n' +
        '                                <th>Тип доставки</th>\n' +
        '                                <th>Номер отримувача</th>\n' +
        '                                <th>ПІБ отримувача</th>\n' +
        '                                <th>Статус</th>\n' +
        '                                <th class="text-center">Дії</th>\n' +
        '                            </tr>\n' +
        '                        </thead>\n' +
        '                        <tbody id="address-list-elements">\n' +
        '                        </tbody>\n' +
        '                        <tfoot>\n' +
        '                            <tr>\n' +
        '                                <th>Адреса доставки</th>\n' +
        '                                <th>Служба доставки</th>\n' +
        '                                <th>Тип доставки</th>\n' +
        '                                <th>Номер отримувача</th>\n' +
        '                                <th>ПІБ отримувача</th>\n' +
        '                                <th>Статус</th>\n' +
        '                                <th class="text-center">Дії</th>\n' +
        '                            </tr>\n' +
        '                        </tfoot>\n' +
        '                    </table>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));
}

function addressDeliveryBuildCreateContent() {
    $("#content").empty();
    $("#content").append($('' +
        '<div class="layout-px-spacing">\n' +
        '    <div class="row layout-top-spacing">\n' +
        '        <div id="basic" class="col-lg-12 layout-spacing">\n' +
        '            <div class="statbox widget box box-shadow">\n' +
        '                <div class="widget-header">\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">\n' +
        '                            <h4>Створення адреси доставки</h4>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <div class="widget-content widget-content-area">\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="phoneNumber">Номер телефону</label>\n' +
        '                                <input id="phoneNumber" type="text" placeholder="Номер телефону" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">' +
        '                            <div class="form-group">\n' +
        '                                <label for="recipient">Отримувач</label>\n' +
        '                                <input id="recipient" type="text" placeholder="Отримувач" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="deliveryService">Сервіс доставки</label>\n' +
        '                                <select id="deliveryService" class="form-control"></select>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto"></div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="deliveryType">Тип доставки</label>\n' +
        '                                <select id="deliveryType" class="form-control"></select>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto"></div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="city">Місто</label>\n' +
        '                                <input id="city" type="text" placeholder="Місто" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto"></div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="streetType">Тип вулиці</label>\n' +
        '                                <select id="streetType" class="form-control"></select>\n' +
        '                            </div>' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="street">Вулиця</label>\n' +
        '                                <input id="street" type="text" placeholder="Вулиця" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="house">Будинок</label>\n' +
        '                                <input id="house" type="text" placeholder="Будинок" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto"></div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="corpus">Корпус</label>\n' +
        '                                <input id="corpus" type="text" placeholder="Корпус" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto"></div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="entrance">Під\'їзд</label>\n' +
        '                                <input id="entrance" type="text" placeholder="Під\'їзд" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto"></div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="floor">Поверх</label>\n' +
        '                                <input id="floor" type="text" placeholder="Поверх" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto"></div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="flatType">Тип приміщення</label>\n' +
        '                                <select id="flatType" class="form-control"></select>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="flat">Номер приміщеня</label>\n' +
        '                                <input id="flat" type="text" placeholder="Номер приміщеня" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="mailbox">Поштова скриня</label>\n' +
        '                                <input id="mailbox" type="text" placeholder="Поштова скриня" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="zipCode">Індекс</label>\n' +
        '                                <input id="zipCode" type="text" placeholder="Індекс" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="warehouse">Відділення почтового сервісу</label>\n' +
        '                                <input id="warehouse" type="text" placeholder="Відділення почтового сервісу" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto"></div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-4 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <button id="cancelBtn" onclick="getAddressDeliveryList([AddressStatusUuidACT, AddressStatusUuidINACT], null, null, null, addressDeliveryBuildInfo, actionError);" class="btn btn-block btn-primary mb-2">Відмінити</button>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-4 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <button id="createBtn" onclick="createAddressDelivery();" class="btn btn-block btn-success mb-2">Створити</button>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>' +
        '</div>' +
        '').fadeIn('slow'));

    setStreetTypeList();
    setFlatTypeList();
    setDeliveryServiceList();
    setDeliveryTypeList();
}

function addressDeliveryBuildInfo(responseData) {

    addressDeliveryBuildListContent();

    for (i = 0; i < responseData.meta.size; i++) {
        let element = responseData.data[i];

        let address = '';
        address = address + ((element.zipCode !== null) ? element.zipCode : '');
        address = address + ((element.city !== null) ? ', м.' + element.city : '');
        address = address + ((element.street !== null) ? ', ' + ((element.streetType !== null) ? ((element.streetType !== null) ? element.streetType : '') : '') + element.street : '');
        address = address + ((element.house !== null) ? ', буд. ' + element.house : '');
        address = address + ((element.corpus !== null) ? ', корп. ' + element.corpus : '');
        address = address + ((element.entrance !== null) ? ', під\'їзд №' + element.entrance : '');
        address = address + ((element.floor !== null) ? ', поверх ' + element.floor : '');
        address = address + ((element.flat !== null) ? ', ' + ((element.flatType !== null) ? ((element.flatType !== null) ? element.flatType : '') : '') + element.flat : '');
        address = address + ((element.mailbox !== null) ? ', поштова скриня №' + element.mailbox : '');
        address = address + ((element.warehouse !== null) ? ', відділення №' + element.warehouse : '');

        let service = ((element.deliveryService !== null) ? element.deliveryService : '');
        let type = ((element.deliveryType !== null) ? element.deliveryType : '');
        let phoneNumber = ((element.phoneNumber !== null) ? element.phoneNumber : '');
        let recipient = ((element.recipient !== null) ? element.recipient : '');

        let status = ((element.status !== null) ? ((element.status.uuid === AddressStatusUuidACT) ? '<span class="badge outline-badge-success"> ' + element.status.title + ' </span>' : ((element.status.uuid === AddressStatusUuidINACT) ? '<span class="badge outline-badge-warning"> ' + element.status.title + ' </span>' : ((element.status.uuid === AddressStatusUuidDEL) ? '<span class="badge outline-badge-danger"> ' + element.status.title + ' </span>' : '<span class="badge outline-badge-dark"> Невизначений </span>'))) : '');

        $("#address-list-elements").append($('' +
            '<tr id="address-' + element.uuid + '">\n' +
            '    <td>' + address + '</td>\n' +
            '    <td>' + service + '</td>\n' +
            '    <td>' + type + '</td>\n' +
            '    <td>' + phoneNumber + '</td>\n' +
            '    <td>' + recipient + '</td>\n' +
            '    <td>' + status + '</td>\n' +
            '    <td id="address-' + element.uuid + '-actions" class="text-center"></td>\n' +
            '</tr>\n' +
            '').fadeIn('slow'));

        if (element.status !== null) {
            if (element.status.uuid === AddressStatusUuidACT) {
                $("#address-" + element.uuid + "-actions").append($('' +
                    '<a href="javascript:setStatusAddressDelivery(&quot;' + element.uuid + '&quot;, &quot;' + AddressStatusUuidINACT + '&quot;);" target="_blank" class="btn btn-outline-warning mb-2 mr-2">\n' +
                    '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                    '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock">\n' +
                    '       <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
                    '       <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>\n' +
                    '   </svg>' +
                    '</a>\n' +
                    '').fadeIn('slow'));
            } else if (element.status.uuid === AddressStatusUuidINACT) {
                $("#address-" + element.uuid + "-actions").append($('' +
                    '<a href="javascript:setStatusAddressDelivery(&quot;' + element.uuid + '&quot;, &quot;' + AddressStatusUuidACT + '&quot;);" target="_blank" class="btn btn-outline-success mb-2 mr-2">\n' +
                    '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                    '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-unlock">\n' +
                    '       <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
                    '       <path d="M7 11V7a5 5 0 0 1 9.9-1"></path>\n' +
                    '   </svg>' +
                    '</a>\n' +
                    '').fadeIn('slow'));
            }
        }

        if (element.status !== null) {
            if (element.status.uuid === AddressStatusUuidACT || element.status.uuid === AddressStatusUuidINACT) {
                $("#address-" + element.uuid + "-actions").append($('' +
                    '<a href="javascript:deleteAddressDelivery(&quot;' + element.uuid + '&quot;);" target="_blank" class="btn btn-outline-danger mb-2 mr-2">\n' +
                    '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                    '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2">\n' +
                    '       <polyline points="3 6 5 6 21 6"></polyline>\n' +
                    '       <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>\n' +
                    '       <line x1="10" y1="11" x2="10" y2="17"></line>\n' +
                    '       <line x1="14" y1="11" x2="14" y2="17"></line>\n' +
                    '   </svg>' +
                    '</a>\n' +
                    '').fadeIn('slow'));
            }
        }
    }

    $("#data_pagination").DataTable({
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Показано сторінок _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Пошук...",
            "sLengthMenu": "Результати :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [10, 20, 50],
        "pageLength": 10
    });

    $("#data_pagination_length").append($('' +
        '<label>' +
        '   <button onclick="addressDeliveryBuildCreateContent();" class="btn btn-outline-primary mb-2">Створити</button>' +
        '</label>' +
        '').fadeIn('slow'));

}


function setDeliveryServiceList(){
    $("#deliveryService").append($('<option value="Lexit" selected>Lexit</option>').fadeIn('slow'));
    $("#deliveryService").append($('<option value="Нова Пошта" selected>Нова Пошта</option>').fadeIn('slow'));
    $("#deliveryService").append($('<option value="УкрПошта" selected>УкрПошта</option>').fadeIn('slow'));
}

function setDeliveryTypeList(){
    $("#deliveryType").append($('<option value="Кур\'єром" selected>Кур\'єром</option>').fadeIn('slow'));
    $("#deliveryType").append($('<option value="На відділення" selected>На відділення</option>').fadeIn('slow'));
}

function setStreetTypeList(){

    $("#streetType").append($('<option value="просп." selected>Проспект</option>').fadeIn('slow'));
    $("#streetType").append($('<option value="бульв." selected>Бульвар</option>').fadeIn('slow'));
    $("#streetType").append($('<option value="вул." selected>Вулиця</option>').fadeIn('slow'));
    $("#streetType").append($('<option value="пров." selected>Провулок</option>').fadeIn('slow'));
    $("#streetType").append($('<option value="туп." selected>Тупик</option>').fadeIn('slow'));
    $("#streetType").append($('<option value="узв." selected>Узвіз</option>').fadeIn('slow'));
    $("#streetType").append($('<option value="проїзд" selected>Проїзд</option>').fadeIn('slow'));
    $("#streetType").append($('<option value="пл." selected>Площа</option>').fadeIn('slow'));

}

function setFlatTypeList(){

    $("#flatType").append($('<option value="прим." selected>Приміщення</option>').fadeIn('slow'));
    $("#flatType").append($('<option value="кім." selected>Кімната</option>').fadeIn('slow'));
    $("#flatType").append($('<option value="оф." selected>Офіс</option>').fadeIn('slow'));
    $("#flatType").append($('<option value="НП" selected>Нежиле приміщення</option>').fadeIn('slow'));

}



function createAddressDelivery(){

    let deliveryService = ''+document.getElementById("deliveryService").value;
    let deliveryType = ''+document.getElementById("deliveryType").value;
    let recipient = ''+document.getElementById("recipient").value;
    let phoneNumber = ''+document.getElementById("phoneNumber").value;
    let city = ''+document.getElementById("city").value;
    let streetType = ''+document.getElementById("streetType").value;
    let street = ''+document.getElementById("street").value;
    let house = ''+document.getElementById("house").value;
    let corpus = ''+document.getElementById("corpus").value;
    let entrance = ''+document.getElementById("entrance").value;
    let floor = ''+document.getElementById("floor").value;
    let flatType = ''+document.getElementById("flatType").value;
    let flat = ''+document.getElementById("flat").value;
    let mailbox = ''+document.getElementById("mailbox").value;
    let zipCode = ''+document.getElementById("zipCode").value;
    let warehouse = ''+document.getElementById("warehouse").value;

    postAddressDelivery(
        ((deliveryService !== '') ? deliveryService : undefined),
        ((deliveryType !== '') ? deliveryType : undefined),
        ((recipient !== '') ? recipient : undefined),
        ((phoneNumber !== '') ? phoneNumber : undefined),
        ((city !== '') ? city : undefined),
        ((streetType !== null) ? streetType : undefined),
        ((street !== '') ? street : undefined),
        ((house !== '') ? house : undefined),
        ((corpus !== '') ? corpus : undefined),
        ((entrance !== '') ? entrance : undefined),
        ((floor !== '') ? floor : undefined),
        ((flatType !== null) ? flatType : undefined),
        ((flat !== '') ? flat : undefined),
        ((mailbox !== '') ? mailbox : undefined),
        ((zipCode !== '') ? zipCode : undefined),
        ((warehouse !== '') ? warehouse : undefined),
        saveEditedData,
        actionError);

    $("#cancelBtn").attr("disabled", true);
    $("#createBtn").attr("disabled", true);
}

function saveEditedData(){
    getAddressDeliveryList(
        [AddressStatusUuidACT, AddressStatusUuidINACT],
        null,
        null,
        null,
        addressDeliveryBuildInfo,
        actionError);
}

function deleteAddressDelivery(uuid){
    if (typeof uuid === 'string') {
        putAddressDelivery(
            uuid,
            AddressStatusUuidDEL,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            deleteAddressDeliveryElement,
            actionError);
    }
}

function deleteAddressDeliveryElement(responseData){

    let element = responseData.data;

    let oTable = $('#data_pagination').dataTable();
    oTable.fnDeleteRow($("#address-" + element.uuid + "-actions"));
}

function setStatusAddressDelivery(uuid, statusUuid){
    if (typeof uuid === 'string' && typeof statusUuid === 'string') {
        if (statusUuid === AddressStatusUuidACT || statusUuid === AddressStatusUuidINACT) {
            putAddressDelivery(
                uuid,
                statusUuid,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                setStatusAddressDeliveryElement,
                actionError);
        }
    }
}

function setStatusAddressDeliveryElement(responseData){

    let element = responseData.data;

    let address = '';
    address = address + ((element.zipCode !== null) ? element.zipCode : '');
    address = address + ((element.city !== null) ? ', м.' + element.city : '');
    address = address + ((element.street !== null) ? ', ' + ((element.streetType !== null) ? ((element.streetType !== null) ? element.streetType : '') : '') + element.street : '');
    address = address + ((element.house !== null) ? ', буд. ' + element.house : '');
    address = address + ((element.corpus !== null) ? ', корп. ' + element.corpus : '');
    address = address + ((element.entrance !== null) ? ', під\'їзд №' + element.entrance : '');
    address = address + ((element.floor !== null) ? ', поверх ' + element.floor : '');
    address = address + ((element.flat !== null) ? ', ' + ((element.flatType !== null) ? ((element.flatType !== null) ? element.flatType : '') : '') + element.flat : '');
    address = address + ((element.mailbox !== null) ? ', поштова скриня №' + element.mailbox : '');
    address = address + ((element.warehouse !== null) ? ', відділення №' + element.warehouse : '');

    let service = ((element.deliveryService !== null) ? element.deliveryService : '');
    let type = ((element.deliveryType !== null) ? element.deliveryType : '');
    let phoneNumber = ((element.phoneNumber !== null) ? element.phoneNumber : '');
    let recipient = ((element.recipient !== null) ? element.recipient : '');

    let status = ((element.status !== null) ? ((element.status.uuid === AddressStatusUuidACT) ? '<span class="badge outline-badge-success"> ' + element.status.title + ' </span>' : ((element.status.uuid === AddressStatusUuidINACT) ? '<span class="badge outline-badge-warning"> ' + element.status.title + ' </span>' : ((element.status.uuid === AddressStatusUuidDEL) ? '<span class="badge outline-badge-danger"> ' + element.status.title + ' </span>' : '<span class="badge outline-badge-dark"> Невизначений </span>'))) : '');

    let actions = '';

    if (element.status !== null) {
        if (element.status.uuid === AddressStatusUuidACT) {
            actions = actions + '' +
                '<a href="javascript:setStatusAddressDelivery(&quot;' + element.uuid + '&quot;, &quot;' + AddressStatusUuidINACT + '&quot;);" target="_blank" class="btn btn-outline-warning mb-2 mr-2">\n' +
                '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock">\n' +
                '       <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
                '       <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>\n' +
                '   </svg>' +
                '</a>\n' +
                '';
        } else if (element.status.uuid === AddressStatusUuidINACT) {
            actions = actions + '' +
                '<a href="javascript:setStatusAddressDelivery(&quot;' + element.uuid + '&quot;, &quot;' + AddressStatusUuidACT + '&quot;);" target="_blank" class="btn btn-outline-success mb-2 mr-2">\n' +
                '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-unlock">\n' +
                '       <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
                '       <path d="M7 11V7a5 5 0 0 1 9.9-1"></path>\n' +
                '   </svg>' +
                '</a>\n' +
                '';
        }
    }

    if (element.status !== null) {
        if (element.status.uuid === AddressStatusUuidACT || element.status.uuid === AddressStatusUuidINACT) {
            actions = actions + '' +
                '<a href="javascript:deleteAddressDelivery(&quot;' + element.uuid + '&quot;);" target="_blank" class="btn btn-outline-danger mb-2 mr-2">\n' +
                '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2">\n' +
                '       <polyline points="3 6 5 6 21 6"></polyline>\n' +
                '       <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>\n' +
                '       <line x1="10" y1="11" x2="10" y2="17"></line>\n' +
                '       <line x1="14" y1="11" x2="14" y2="17"></line>\n' +
                '   </svg>' +
                '</a>\n' +
                '';
        }
    }

    let oTable = $('#data_pagination').dataTable();
    oTable.fnUpdate([address, service, type, phoneNumber, recipient, status, actions], $("#address-" + element.uuid + "-actions"));
}




function actionErrorCreation(responseData) {
    alert('Error code : ' + responseData.state.code + '\n' + 'Error message : ' + responseData.state.message);
    $("#cancelBtn").attr("disabled", false);
    $("#createBtn").attr("disabled", false);
}

function actionError(responseData) {
    alert('Error code : ' + responseData.state.code + '\n' + 'Error message : ' + responseData.state.message);
}