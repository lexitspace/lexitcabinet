function init() {
    getAddressJuridicalList(
        [AddressStatusUuidACT, AddressStatusUuidINACT],
        null,
        null,
        null,
        addressJuridicalBuildInfo,
        actionError);
}

function addressJuridicalBuildListContent() {
    $("#content").empty();
    $("#content").append($('' +
        '<div class="layout-px-spacing">\n' +
        '    <div class="row layout-top-spacing" id="cancel-row">\n' +
        '        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">\n' +
        '            <div class="widget-content widget-content-area br-6">\n' +
        '                <div class="table-responsive mb-4 mt-4">\n' +
        '                    <table id="data_pagination" class="table table-hover" style="width:100%">\n' +
        '                        <thead>\n' +
        '                            <tr>\n' +
        '                                <th>Юридична адреса</th>\n' +
        '                                <th>Ліміт</th>\n' +
        '                                <th>Статус</th>\n' +
        '                                <th class="text-center">Дії</th>\n' +
        '                            </tr>\n' +
        '                        </thead>\n' +
        '                        <tbody id="address-list-elements">\n' +
        '                        </tbody>\n' +
        '                        <tfoot>\n' +
        '                            <tr>\n' +
        '                                <th>Юридична адреса</th>\n' +
        '                                <th>Ліміт</th>\n' +
        '                                <th>Статус</th>\n' +
        '                                <th class="text-center">Дії</th>\n' +
        '                            </tr>\n' +
        '                        </tfoot>\n' +
        '                    </table>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>' +
        '').fadeIn('slow'));
}

function addressJuridicalBuildCreateContent() {
    $("#content").empty();
    $("#content").append($('' +
        '<div class="layout-px-spacing">\n' +
        '    <div class="row layout-top-spacing">\n' +
        '        <div id="basic" class="col-lg-12 layout-spacing">\n' +
        '            <div class="statbox widget box box-shadow">\n' +
        '                <div class="widget-header">\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">\n' +
        '                            <h4>Створення юридичної адреси</h4>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <div class="widget-content widget-content-area">\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="title">Юридична адреса</label>\n' +
        '                                <input id="title" type="text" placeholder="Юридична адреса" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-5 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <label for="limitation">Ліміт компаній на адресі</label>\n' +
        '                                <input id="limitation" type="text" placeholder="Ліміт компаній на адресі" class="form-control" required>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                    <div class="row">\n' +
        '                        <div class="col-lg-4 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <button id="cancelBtn" onclick="getAddressJuridicalItem([AddressStatusUuidACT, AddressStatusUuidINACT], null, null, null, addressJuridicalBuildInfo, actionError);" class="btn btn-block btn-primary mb-2">Відмінити</button>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="col-lg-4 col-12 mx-auto">\n' +
        '                            <div class="form-group">\n' +
        '                                <button id="createBtn" onclick="createAddressJuridical();" class="btn btn-block btn-success mb-2">Створити</button>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>' +
        '</div>' +
        '').fadeIn('slow'));
}

function addressJuridicalBuildInfo(responseData) {

    addressJuridicalBuildListContent();

    for (i = 0; i < responseData.meta.size; i++) {
        let element = responseData.data[i];

        let addressJuridical = ((element.title !== null) ? element.title : '');

        let limitation = ((element.limitation !== null) ? element.limitation : '');

        let status = ((element.status !== null) ? ((element.status.uuid === AddressStatusUuidACT) ? '<span class="badge outline-badge-success"> ' + element.status.title + ' </span>' : ((element.status.uuid === AddressStatusUuidINACT) ? '<span class="badge outline-badge-warning"> ' + element.status.title + ' </span>' : ((element.status.uuid === AddressStatusUuidDEL) ? '<span class="badge outline-badge-danger"> ' + element.status.title + ' </span>' : '<span class="badge outline-badge-dark"> Невизначений </span>'))) : '');

        $("#address-list-elements").append($('' +
            '<tr id="address-' + element.uuid + '">\n' +
            '    <td>' + addressJuridical + '</td>\n' +
            '    <td>' + limitation + '</td>\n' +
            '    <td>' + status + '</td>\n' +
            '    <td id="address-' + element.uuid + '-actions" class="text-center"></td>\n' +
            '</tr>\n' +
            '').fadeIn('slow'));

        if (element.status !== null) {
            if (element.status.uuid === AddressStatusUuidACT) {
                $("#address-" + element.uuid + "-actions").append($('' +
                    '<a href="javascript:setStatusAddressJuridical(&quot;' + element.uuid + '&quot;, &quot;' + AddressStatusUuidINACT + '&quot;);" target="_blank" class="btn btn-outline-warning mb-2 mr-2">\n' +
                    '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                    '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock">\n' +
                    '       <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
                    '       <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>\n' +
                    '   </svg>' +
                    '</a>\n' +
                    '').fadeIn('slow'));
            } else if (element.status.uuid === AddressStatusUuidINACT) {
                $("#address-" + element.uuid + "-actions").append($('' +
                    '<a href="javascript:setStatusAddressJuridical(&quot;' + element.uuid + '&quot;, &quot;' + AddressStatusUuidACT + '&quot;);" target="_blank" class="btn btn-outline-success mb-2 mr-2">\n' +
                    '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                    '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-unlock">\n' +
                    '       <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
                    '       <path d="M7 11V7a5 5 0 0 1 9.9-1"></path>\n' +
                    '   </svg>' +
                    '</a>\n' +
                    '').fadeIn('slow'));
            }
        }
    }

    $("#data_pagination").DataTable({
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Показано сторінок _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Пошук...",
            "sLengthMenu": "Результати :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [10, 20, 50],
        "pageLength": 10
    });

    $("#data_pagination_length").append($('' +
        '<label>' +
        '   <button onclick="addressJuridicalBuildCreateContent();" class="btn btn-outline-primary mb-2">Створити</button>' +
        '</label>' +
        '').fadeIn('slow'));

}

function createAddressJuridical(){
    let title = '' + document.getElementById("title").value;
    let limitation = Number.parseInt('' + document.getElementById("limitation").value);

    postAddressJuridical(
        ((title !== '') ? title : undefined),
        ((limitation !== null) ? limitation : undefined),
        saveEditedData,
        actionError);

    $("#cancelBtn").attr("disabled", true);
    $("#createBtn").attr("disabled", true);
}

function saveEditedData(){
    getAddressJuridicalList(
        [AddressStatusUuidACT, AddressStatusUuidINACT],
        null,
        null,
        null,
        addressJuridicalBuildInfo,
        actionError);
}

function setStatusAddressJuridical(uuid, statusUuid){
    if (typeof uuid === 'string' && typeof statusUuid === 'string') {
        if (statusUuid === AddressStatusUuidACT || statusUuid === AddressStatusUuidINACT) {
            putAddressJuridical(
                uuid,
                statusUuid,
                undefined,
                undefined,
                setStatusAddressJuridicalElement,
                actionError);
        }
    }
}

function setStatusAddressJuridicalElement(responseData){

    let element = responseData.data;

    let addressJuridical = ((element.title !== null) ? element.title : '');

    let limitation = ((element.limitation !== null) ? element.limitation : '');

    let status = ((element.status !== null) ? ((element.status.uuid === AddressStatusUuidACT) ? '<span class="badge outline-badge-success"> ' + element.status.title + ' </span>' : ((element.status.uuid === AddressStatusUuidINACT) ? '<span class="badge outline-badge-warning"> ' + element.status.title + ' </span>' : ((element.status.uuid === AddressStatusUuidDEL) ? '<span class="badge outline-badge-danger"> ' + element.status.title + ' </span>' : '<span class="badge outline-badge-dark"> Невизначений </span>'))) : '');

    let actions = '';

    if (element.status !== null) {
        if (element.status.uuid === AddressStatusUuidACT) {
            actions = actions + '' +
                '<a href="javascript:setStatusAddressJuridical(&quot;' + element.uuid + '&quot;, &quot;' + AddressStatusUuidINACT + '&quot;);" target="_blank" class="btn btn-outline-warning mb-2 mr-2">\n' +
                '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock">\n' +
                '       <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
                '       <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>\n' +
                '   </svg>' +
                '</a>\n' +
                '';
        } else if (element.status.uuid === AddressStatusUuidINACT) {
            actions = actions + '' +
                '<a href="javascript:setStatusAddressJuridical(&quot;' + element.uuid + '&quot;, &quot;' + AddressStatusUuidACT + '&quot;);" target="_blank" class="btn btn-outline-success mb-2 mr-2">\n' +
                '   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"\n' +
                '        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-unlock">\n' +
                '       <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>\n' +
                '       <path d="M7 11V7a5 5 0 0 1 9.9-1"></path>\n' +
                '   </svg>' +
                '</a>\n' +
                '';
        }
    }

    let oTable = $('#data_pagination').dataTable();
    oTable.fnUpdate([addressJuridical, limitation, status, actions], $("#address-" + element.uuid + "-actions"));
}

function actionErrorCreation(responseData) {
    alert('Error code : ' + responseData.state.code + '\n' + 'Error message : ' + responseData.state.message);
    $("#cancelBtn").attr("disabled", false);
    $("#createBtn").attr("disabled", false);
}

function actionError(responseData) {
    alert('Error code : ' + responseData.state.code + '\n' + 'Error message : ' + responseData.state.message);
}